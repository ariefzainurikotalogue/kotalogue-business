package com.kotalogue.android_business.classHelper;

import android.animation.LayoutTransition;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.kotalogue.android_business.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PublicMethod {

    // slide the view from below itself to the current position
    public static void slideUp(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);         // toYDelta
        animate.setStartTime(200);
        animate.setDuration(400);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public static File saveBitmapToFile(File file){
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=100;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.mkdir();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public static void focusToView(View view){
        new Handler().postDelayed(() -> view.getParent().requestChildFocus(view, view), 1000);
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static long dateStringToLong(String strDate, String pattern){
        long timeInLong;
        Date date = null;

        if (strDate.contains(":") || strDate.contains("/") || strDate.contains(",")){
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);
                date = sdf.parse(strDate);
            } catch (ParseException e){
                e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            timeInLong = calendar.getTimeInMillis();
        } else {
            timeInLong = 0;
        }

        return timeInLong;
    }

    public static void changeToWhiteStatusBar(Activity activity, Context context){
        //below code will make the status bar color to white, and change the item inside the status bar color to grey
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);

            View decor = activity.getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            //code below is to set the status bar color to white / default color
            //decor.setSystemUiVisibility(0);
        }

        //this code will make the status bar color to color primary dark
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    public static void sharedDynamicsLink(Context context, Activity activity, String id, String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_wait, null);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://kotalogue.com/"+ type + "," + id))
                .setDomainUriPrefix("https://ktlg.app.goo.gl")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder()
                        .setFallbackUrl(Uri.parse("https://kotalogue.com/"))
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle("https://kotalogue.com/")
                        .setDescription("Welcome to Kotalogue").build())
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        // Short link created
                        alertDialog.dismiss();
                        Uri shortLink = task.getResult().getShortLink();

                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shortLink.toString());
                        context.startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
                    } else {
                        Snackbar.make(view, task.getException().getMessage(), Snackbar.LENGTH_LONG)
                                .setAction(R.string.retry, v -> sharedDynamicsLink(context, activity, id, type)).show();
                    }
                });
    }

    public static String dateLongToString(long longDate, String pattern, Context context){
        String dateReturn;

        if (longDate == 0){
            dateReturn = context.getResources().getString(R.string.dayNotSet);
        } else {
            Date date = new Date(longDate);
            Format format = new SimpleDateFormat(pattern, Locale.US);
            dateReturn = format.format(date);
        }

        return dateReturn;
    }

    public static void enableDefaultAnimation(ViewGroup viewGroup){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            viewGroup.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        }
    }

    public static int dp(int id, Context context){
        return Integer.valueOf(String.valueOf(context.getResources().getDimensionPixelSize(id)));
    }

    public static int getViewHeight(View view){
        WindowManager wm = (WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int deviceWidth;

        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthMeasureSpec, heightMeasureSpec);
        return view.getMeasuredHeight();
    }

    public static int getViewWidth(View view){
        WindowManager wm = (WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int deviceWidth;

        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthMeasureSpec, heightMeasureSpec);
        return view.getMeasuredWidth();
    }

    // slide the view from its current position to below itself
    public static void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setStartTime(200);
        animate.setDuration(400);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean hasNavBar(Context context, Activity activity) {
        Point realSize = new Point();
        Point screenSize = new Point();
        boolean hasNavBar = false;
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        realSize.x = metrics.widthPixels;
        realSize.y = metrics.heightPixels;
        activity.getWindowManager().getDefaultDisplay().getSize(screenSize);
        if (realSize.y != screenSize.y) {
            int difference = realSize.y - screenSize.y;
            int navBarHeight = 0;
            Resources resources = context.getResources();
            int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navBarHeight = resources.getDimensionPixelSize(resourceId);
            }
            if (navBarHeight != 0) {
                if (difference == navBarHeight) {
                    hasNavBar = true;
                }
            }

        }
        return hasNavBar;

    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getNavigationBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    public static boolean isDeviceOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static void checkSocMedia(String inputFacebook, String inputTwitter, String inputInstagram, String inputWhatsapp, String inputWebsite,
                                     String inputEmail, TextInputEditText facebook, TextInputEditText twitter, TextInputEditText instagram,
                                     TextInputEditText whatsapp, TextInputEditText website, TextInputEditText email) {

        if (inputFacebook.equals("") || inputFacebook.equals("https://www.facebook.com/")){
            facebook.setText(null);
            facebook.setHint(R.string.facebookNotSet);
            facebook.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus){
                    if (facebook.getText().toString().equals("")){
                        facebook.setText(R.string.facebookDefault);
                    }
                } else {
                    if (facebook.getText().toString().equals("https://www.facebook.com/")){
                        facebook.setText(null);
                    }
                }
            });
        } else {
            facebook.setText(inputFacebook);
        }

        if (inputTwitter.equals("") || inputTwitter.equals("https://www.twitter.com/")){
            twitter.setText(null);
            twitter.setHint(R.string.twitterNotSet);
            twitter.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus){
                    if (twitter.getText().toString().equals("")){
                        twitter.setText(R.string.twitterDefault);
                    }
                } else {
                    if (twitter.getText().toString().equals("https://www.twitter.com/")){
                        twitter.setText(null);
                    }
                }
            });
        } else {
            twitter.setText(inputTwitter);
        }

        if (inputInstagram.equals("") || inputInstagram.equals("https://www.instagram.com/")){
            instagram.setText(null);
            instagram.setHint(R.string.instagramNotSet);
            instagram.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus){
                    if (instagram.getText().toString().equals("")){
                        instagram.setText(R.string.instagramDefault);
                    }
                } else {
                    if (instagram.getText().toString().equals("https://www.instagram.com/")){
                        instagram.setText(null);
                    }
                }
            });
        } else {
            instagram.setText(inputInstagram);
        }

        if (inputWhatsapp.equals("")){
            whatsapp.setHint(R.string.whatsAppNotSet);
        } else {
            whatsapp.setText(inputWhatsapp);
        }

        if (inputWebsite.equals("")){
            website.setHint(R.string.websiteNotSet);
        } else {
            website.setText(inputWebsite);
        }

        if (inputEmail.equals("")){
            email.setHint(R.string.emailNotSet);
        } else {
            email.setText(inputEmail);
        }
    }
}
