package com.kotalogue.android_business.classHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.kotalogue.android_business.activity.StandartActivity;
import com.kotalogue.android_business.globalInterface.OnBackPressedListener;

public class SimpleBrandBackListener implements OnBackPressedListener {
    private Activity activity;
    private Context context;

    public SimpleBrandBackListener(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;
    }

    @Override
    public void doBack() {
        Intent intent = new Intent(context, StandartActivity.class);
        intent.putExtra("open", "OpenFragmentHomeBrands");
        context.startActivity(intent);
        activity.finish();
    }

}
