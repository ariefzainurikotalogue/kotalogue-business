package com.kotalogue.android_business.globalAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.databinding.LayoutItemBrandBinding;
import com.kotalogue.android_business.model.ModelBrand;

import java.util.ArrayList;

public class AdapterRecyclerBrand extends RecyclerView.Adapter<AdapterRecyclerBrand.ViewHolder>{
    private ArrayList<ModelBrand> modelBrands;
    private Context context;
    private AdapterRecyclerBrandInterface adapterRecyclerBrandInterface;

    public AdapterRecyclerBrand(ArrayList<ModelBrand> modelBrands, Context context, AdapterRecyclerBrandInterface adapterRecyclerBrandInterface) {
        this.modelBrands = modelBrands;
        this.context = context;
        this.adapterRecyclerBrandInterface = adapterRecyclerBrandInterface;
    }

    @NonNull
    @Override
    public AdapterRecyclerBrand.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemBrandBinding binding = LayoutItemBrandBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerBrand.ViewHolder holder, int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.cardBrand);
        holder.binding.cardBrandDescription.setSelected(true);
        holder.binding.brandCategories.setSelected(true);
        holder.binding.cardBrandName.setSelected(true);

        boolean isMaster = context.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        setCustomMargin(holder, position);

        //check if master
        if (isMaster){
            holder.binding.iconMenu.setVisibility(View.GONE);
        }

        holder.binding.cardBrandDescription.setText(modelBrands.get(position).getBrandLongDesc());
        holder.binding.cardBrandName.setText(modelBrands.get(position).getBrandName());
        holder.binding.brandCategories.setText(modelBrands.get(position).getBrandCategories());

        Glide.with(context.getApplicationContext()).load(modelBrands.get(position).getBrandHeaderPhoto()).placeholder(R.drawable.thumbnail).into(holder.binding.imageHeader);

        clickListener(holder, position);
    }

    private void setCustomMargin(ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();

        if (position == 0){
            PublicMethod.setMargins(holder.binding.cardBrand, PublicMethod.dp(R.dimen.standart_20dp, context) ,PublicMethod.dp(R.dimen.fab_margin, context), PublicMethod.dp(R.dimen.standart_20dp, context), PublicMethod.dp(R.dimen.dp10, context));
        } else if (position == modelBrands.size() - 1){
            PublicMethod.setMargins(holder.binding.cardBrand, PublicMethod.dp(R.dimen.standart_20dp, context) ,PublicMethod.dp(R.dimen.dp10, context), PublicMethod.dp(R.dimen.standart_20dp, context), PublicMethod.dp(R.dimen.fab_margin, context));
            holder.binding.divider.setVisibility(View.GONE);
        }
    }

    public ArrayList<ModelBrand> removeData(int position) {
        notifyItemRangeChanged(position, modelBrands.size());
        modelBrands.remove(position);
        notifyItemRemoved(position);

        return modelBrands;
    }

    public ArrayList<ModelBrand> addData(ModelBrand modelBrand, int position) {
        if (modelBrands.size() == 0){
            modelBrands.add(modelBrand);
            notifyDataSetChanged();
        } else {
            modelBrands.add(modelBrands.size()-1, modelBrand);
            notifyItemInserted(position);
        }

        return modelBrands;
    }

    private void clickListener(ViewHolder holder, int position) {
        holder.binding.cardBrand.setOnClickListener(v -> adapterRecyclerBrandInterface.itemClick(modelBrands, position));

        holder.binding.iconMenu.setOnClickListener(v -> openMenu(holder, position));
    }

    private void openMenu(ViewHolder holder, int position) {
        PopupMenu popupMenu = new PopupMenu(context, holder.binding.iconMenu);

        popupMenu.getMenuInflater().inflate(R.menu.menu_brand, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.delete:
                    adapterRecyclerBrandInterface.deleteBrand(modelBrands, position);
                    break;

                case R.id.transfer:
                    adapterRecyclerBrandInterface.transferClick(modelBrands, position);
                    break;
            }
            return false;
        });

        popupMenu.show();
    }

    @Override
    public int getItemCount() {
        if (modelBrands != null){
            return modelBrands.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelBrand> modelBrands) {
        this.modelBrands = modelBrands;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutItemBrandBinding binding;
        public ViewHolder(LayoutItemBrandBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterRecyclerBrandInterface{
        void itemClick(ArrayList<ModelBrand> modelBrands, int position);
        void transferClick(ArrayList<ModelBrand> modelBrands, int position);
        void deleteBrand(ArrayList<ModelBrand> modelBrands, int position);
    }
}
