package com.kotalogue.android_business.globalAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.databinding.LayoutDaftarUserBinding;
import com.kotalogue.android_business.model.ModelUser;

import java.util.ArrayList;

public class AdapterSingleLineText extends RecyclerView.Adapter<AdapterSingleLineText.ViewHolder>{
    private ArrayList<ModelUser> modelUsers;
    private AdapterDaftarUserInterface adapterDaftarUserInterface;

    public AdapterSingleLineText(ArrayList<ModelUser> modelUsers, AdapterDaftarUserInterface adapterDaftarUserInterface) {
        this.modelUsers = modelUsers;
        this.adapterDaftarUserInterface = adapterDaftarUserInterface;
    }

    @NonNull
    @Override
    public AdapterSingleLineText.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutDaftarUserBinding binding = LayoutDaftarUserBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSingleLineText.ViewHolder holder, int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.layoutContentMain);

        holder.binding.user.setSelected(true);
        holder.binding.user.setText(modelUsers.get(position).getEmail());

        //click listener
        holder.binding.cardUser.setOnClickListener(v -> clickTransfer(position));
    }

    private void clickTransfer(int position) {
        adapterDaftarUserInterface.transferClick(modelUsers, position);
    }

    @Override
    public int getItemCount() {
        if (modelUsers != null){
            return modelUsers.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelUser> modelUsers) {
        this.modelUsers = modelUsers;
        notifyDataSetChanged();
    }

    public ArrayList<ModelUser> removeData(int position) {
        notifyItemRangeChanged(position, modelUsers.size());
        modelUsers.remove(position);
        notifyItemRemoved(position);

        return modelUsers;
    }

    public ArrayList<ModelUser> addData(ModelUser modelUser, int position) {
        if (modelUsers.size() == 0){
            modelUsers.add(modelUser);
            notifyDataSetChanged();
        } else {
            modelUsers.add(modelUsers.size()-1, modelUser);
            notifyItemInserted(position);
        }

        return modelUsers;
    }

    public void setFilter(ArrayList<ModelUser> filterList) {
        modelUsers = new ArrayList<>();
        modelUsers.addAll(filterList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutDaftarUserBinding binding;
        public ViewHolder(LayoutDaftarUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterDaftarUserInterface{
        void transferClick(ArrayList<ModelUser> modelUsers, int position);
    }
}
