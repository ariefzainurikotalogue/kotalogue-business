package com.kotalogue.android_business.globalAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.model.ModelVenue;
import com.kotalogue.android_business.databinding.LayoutItemPickVenueBinding;

import java.util.ArrayList;

import static android.view.View.GONE;

/**
 * Created by ariefzainuri on 21/04/18.
 */

public class AdapterPickVenue extends RecyclerView.Adapter<AdapterPickVenue.ViewHolder> {
    private ArrayList<ModelVenue> modelVenues;
    private String openFor;
    private AdapterPickVenueInterface adapterPickVenueInterface;

    //local adapter var
    private ArrayList<String> listOfVenue = new ArrayList<>();

    public AdapterPickVenue(ArrayList<ModelVenue> modelVenues, String openFor, AdapterPickVenueInterface adapterPickVenueInterface) {
        this.modelVenues = modelVenues;
        this.openFor = openFor;
        this.adapterPickVenueInterface = adapterPickVenueInterface;
    }

    @NonNull
    @Override
    public AdapterPickVenue.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemPickVenueBinding binding = LayoutItemPickVenueBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPickVenue.ViewHolder holder, final int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.layoutContentMain);

        if (listOfVenue.contains(modelVenues.get(position).getVenueID())) {
            holder.binding.iconCheck.setVisibility(View.VISIBLE);
        } else {
            holder.binding.iconCheck.setVisibility(View.GONE);
        }

        holder.binding.venueName.setSelected(true);

        holder.binding.venueName.setText(modelVenues.get(position).getVenueName());
        holder.binding.venueAddress.setText(modelVenues.get(position).getVenueAddress());

        holder.binding.layoutVenue.setOnClickListener(v -> {
            switch (openFor){
                case "Venue":
                    adapterPickVenueInterface.openVenue(modelVenues, position);
                    break;

                case "Event":
                    adapterPickVenueInterface.openEvent(modelVenues, position);
                    break;

                case "Group":
                    adapterPickVenueInterface.insertToGroup(modelVenues, position, holder.binding.iconCheck);
                    break;
            }
        });
    }

    @Override
    public int getItemCount() {
        if (modelVenues != null){
            return modelVenues.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelVenue> modelVenues) {
        this.modelVenues = modelVenues;
        notifyDataSetChanged();
    }

    public void setFilter(ArrayList<ModelVenue> filterList) {
        modelVenues = new ArrayList<>();
        modelVenues.addAll(filterList);
        notifyDataSetChanged();
    }

    public void updatePickedVenue(ArrayList<String> listOfVenue) {
        this.listOfVenue = listOfVenue;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutItemPickVenueBinding binding;
        public ViewHolder(LayoutItemPickVenueBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterPickVenueInterface{
        void openVenue(ArrayList<ModelVenue> modelVenues, int position);
        void openEvent(ArrayList<ModelVenue> modelVenues, int position);
        void insertToGroup(ArrayList<ModelVenue> modelVenues, int position, ImageView iconCheck);
    }
}
