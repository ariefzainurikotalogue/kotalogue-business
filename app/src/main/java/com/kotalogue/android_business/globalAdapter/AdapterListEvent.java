package com.kotalogue.android_business.globalAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.model.ModelEvent;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.ItemListEventBinding;

import java.util.ArrayList;

public class AdapterListEvent extends RecyclerView.Adapter<AdapterListEvent.ViewHolder>{
    private ArrayList<ModelEvent> modelEvents;
    private Context context;
    private AdapterListEventInterface adapterListEventInterface;
    private Activity activity;

    public AdapterListEvent(ArrayList<ModelEvent> modelEvents, Context context, AdapterListEventInterface adapterListEventInterface, Activity activity) {
        this.modelEvents = modelEvents;
        this.context = context;
        this.adapterListEventInterface = adapterListEventInterface;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AdapterListEvent.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemListEventBinding binding = ItemListEventBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterListEvent.ViewHolder holder, int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.layoutContentMain);

        boolean isMaster = context.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);
        holder.binding.eventName.setSelected(true);
        holder.binding.venueName.setSelected(true);
        holder.binding.date.setSelected(true);

        setCustomMargin(holder, position);

        if (modelEvents.get(position).getUserID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            holder.binding.iconDelete.setVisibility(View.VISIBLE);
            holder.binding.layoutEvent.setOnClickListener(v -> toDetailEvent(position));
            holder.binding.iconDelete.setOnClickListener(v -> adapterListEventInterface.deleteItem(modelEvents, position));
        } else if (isMaster) {
            holder.binding.layoutEvent.setOnClickListener(v -> toDetailEvent(position));
            holder.binding.iconDelete.setVisibility(View.GONE);
        } else {
            holder.binding.layoutEvent.setOnClickListener(v -> openDialog());
            holder.binding.iconDelete.setVisibility(View.GONE);
        }

        holder.binding.eventName.setText(modelEvents.get(position).getEventName());
        holder.binding.date.setText(PublicMethod.dateLongToString(modelEvents.get(position).getEventStartDate(), "MMMM dd, yyyy", holder.itemView.getContext()));

        //get venue name
        FirebaseFirestore.getInstance().collection("Venue").document(modelEvents.get(position).getVenueID())
                .get().addOnSuccessListener(documentSnapshot -> holder.binding.venueName.setText(documentSnapshot.getString("venueName")));

        Glide.with(context).load(modelEvents.get(position).getEventHeaderPhoto())
                .placeholder(R.drawable.new_logo_v2)
                .error(R.drawable.new_logo_v2)
                .into(holder.binding.imageHeader);

        //click listener
        holder.binding.iconDelete.setOnClickListener(view -> adapterListEventInterface.deleteItem(modelEvents, position));
        holder.binding.iconShare.setOnClickListener(view -> PublicMethod.sharedDynamicsLink(context, activity, modelEvents.get(position).getEventID(),"OpenFragmentDetailEvent"));
    }

    private void setCustomMargin(ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();

        if (position == 0){
            PublicMethod.setMargins(holder.binding.layoutContentMain, PublicMethod.dp(R.dimen.standart_20dp, context),
                    PublicMethod.dp(R.dimen.fab_margin, context), PublicMethod.dp(R.dimen.standart_20dp, context), PublicMethod.dp(R.dimen.dp10, context));
        } else if (position == modelEvents.size() - 1){
            PublicMethod.setMargins(holder.binding.layoutContentMain, PublicMethod.dp(R.dimen.standart_20dp, context),
                    PublicMethod.dp(R.dimen.dp10, context), PublicMethod.dp(R.dimen.standart_20dp, context), PublicMethod.dp(R.dimen.fab_margin, context));
            holder.binding.divider.setVisibility(View.GONE);
        }
    }

    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.cantEditEvent));
        builder.setMessage(context.getResources().getString(R.string.cantEditMessage));
        builder.setPositiveButton(context.getResources().getString(R.string.cancel), (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void toDetailEvent(int position){
        adapterListEventInterface.itemClick(modelEvents, position);
    }

    @Override
    public int getItemCount() {
        if (modelEvents != null){
            return modelEvents.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelEvent> modelEvents) {
        this.modelEvents = modelEvents;
        notifyDataSetChanged();
    }

    public ArrayList<ModelEvent> removeData(int position) {
        notifyItemRangeChanged(position, modelEvents.size());
        modelEvents.remove(position);
        notifyItemRemoved(position);

        return modelEvents;
    }

    public ArrayList<ModelEvent> addData(ModelEvent modelEvent, int position) {
        if (modelEvents.size() == 0){
            modelEvents.add(modelEvent);
            notifyDataSetChanged();
        } else {
            modelEvents.add(modelEvents.size()-1, modelEvent);
            notifyItemInserted(position);
        }

        return modelEvents;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemListEventBinding binding;
        public ViewHolder(ItemListEventBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterListEventInterface{
        void itemClick(ArrayList<ModelEvent> modelEvents, int position);
        void deleteItem(ArrayList<ModelEvent> modelEvents, int position);
    }
}
