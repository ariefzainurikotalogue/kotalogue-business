package com.kotalogue.android_business.globalAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.ViewHolder.ViewHolderAdd;
import com.kotalogue.android_business.ViewHolder.ViewHolderImage;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.databinding.LayoutAddImageBinding;
import com.kotalogue.android_business.model.ModelGallery;
import com.kotalogue.android_business.databinding.LayoutGalleryBinding;

import java.util.ArrayList;

/**
 * Created by ariefzainuri on 15/04/18.
 */

public class AdapterGallery extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<ModelGallery> modelGalleries;
    Context context;
    AdapterGalleryInterface adapterGalleryInterface;

    private final int VIEW_IMAGE = 0, VIEW_ADD = 1;

    public AdapterGallery(ArrayList<ModelGallery> modelGalleries, Context context, AdapterGalleryInterface adapterGalleryInterface) {
        this.modelGalleries = modelGalleries;
        this.context = context;
        this.adapterGalleryInterface = adapterGalleryInterface;
    }

    @Override
    public int getItemCount() {
        if (modelGalleries != null){
            return modelGalleries.size();
        } else {
            return 0;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int itemViewType) {
        if (itemViewType == VIEW_ADD) {
            LayoutAddImageBinding layoutAddImageBinding = LayoutAddImageBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderAdd(layoutAddImageBinding);
        } else {
            LayoutGalleryBinding layoutGalleryBinding = LayoutGalleryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderImage(layoutGalleryBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case VIEW_IMAGE:
                ViewHolderImage viewHolderImage = (ViewHolderImage) viewHolder;
                RelativeLayout layout = viewHolderImage.itemView.findViewById(R.id.layoutContentMain);
                ImageView image = viewHolderImage.itemView.findViewById(R.id.imageGallery);
                CardView cardImage = viewHolderImage.itemView.findViewById(R.id.layoutImage);
                TextView textView = viewHolderImage.itemView.findViewById(R.id.textSaved);

                PublicMethod.enableDefaultAnimation(layout);

                if (modelGalleries.get(position).isSavedToServer()){
                    textView.setVisibility(View.VISIBLE);

                    Glide.with(context.getApplicationContext()).load(modelGalleries.get(position).getPhoto())
                            .placeholder(R.drawable.new_logo_v2)
                            .error(R.drawable.new_logo_v2)
                            .crossFade()
                            .into(image);
                } else {
                    textView.setVisibility(View.GONE);

                    Glide.with(context.getApplicationContext()).load(modelGalleries.get(position).getPhotoUri())
                            .placeholder(R.drawable.new_logo_v2)
                            .error(R.drawable.new_logo_v2)
                            .crossFade()
                            .into(image);
                }

                cardImage.setOnClickListener(v -> adapterGalleryInterface.galleryClick(modelGalleries, position, cardImage));
                break;
            case VIEW_ADD:
                ViewHolderAdd viewHolderAdd = (ViewHolderAdd) viewHolder;
                CardView cardAdd = viewHolderAdd.itemView.findViewById(R.id.layoutImage);
                cardAdd.setOnClickListener(view -> {
                    adapterGalleryInterface.addGallery();
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelGalleries.get(position).getStatus().equals("Image")) {
            return VIEW_IMAGE;
        } else {
            return VIEW_ADD;
        }
    }

    public ArrayList<ModelGallery> removeData(int position) {
        notifyItemRangeChanged(position, modelGalleries.size());
        modelGalleries.remove(position);
        notifyItemRemoved(position);
        return modelGalleries;
    }

    public ArrayList<ModelGallery> addData(ModelGallery modelGallery, int position) {
        if (modelGalleries.size() == 0){
            modelGalleries.add(modelGallery);
            notifyDataSetChanged();
        } else {
            modelGalleries.add(modelGalleries.size()-1, modelGallery);
            notifyItemInserted(position);
        }

        return modelGalleries;
    }

    public void updateData(ArrayList<ModelGallery> modelGalleries) {
        this.modelGalleries = modelGalleries;
        notifyDataSetChanged();
    }

    public interface AdapterGalleryInterface{
        void galleryClick(ArrayList<ModelGallery> modelGalleries, int position, CardView cardView);
        void addGallery();
    }
}