package com.kotalogue.android_business.globalAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kotalogue.android_business.databinding.LayoutDaftarUserBinding;
import com.kotalogue.android_business.model.ModelGroup;

import java.util.ArrayList;

public class AdapterFragmentPickGroup extends RecyclerView.Adapter<AdapterFragmentPickGroup.ViewHolder> {
    private ArrayList<ModelGroup> modelGroups;
    private AdapterFragmentPickGroupInterface adapterFragmentPickGroupInterface;

    public AdapterFragmentPickGroup(ArrayList<ModelGroup> modelGroups, AdapterFragmentPickGroupInterface adapterFragmentPickGroupInterface) {
        this.modelGroups = modelGroups;
        this.adapterFragmentPickGroupInterface = adapterFragmentPickGroupInterface;
    }

    @NonNull
    @Override
    public AdapterFragmentPickGroup.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutDaftarUserBinding binding = LayoutDaftarUserBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFragmentPickGroup.ViewHolder viewHolder, int i) {
        viewHolder.binding.user.setText(modelGroups.get(i).getGroupName());
        viewHolder.binding.cardUser.setOnClickListener(view -> adapterFragmentPickGroupInterface.itemClick(modelGroups, i));
    }

    @Override
    public int getItemCount() {
        if (modelGroups != null) {
            return modelGroups.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelGroup> modelGroups) {
        this.modelGroups = modelGroups;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutDaftarUserBinding binding;
        public ViewHolder(LayoutDaftarUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterFragmentPickGroupInterface{
        void itemClick(ArrayList<ModelGroup> modelGroups, int i);
    }
}
