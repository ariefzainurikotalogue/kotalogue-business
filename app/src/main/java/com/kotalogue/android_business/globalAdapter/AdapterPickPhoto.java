package com.kotalogue.android_business.globalAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.databinding.LayoutImageBinding;

import java.util.ArrayList;

/**
 * Created by ariefzainuri on 15/04/18.
 */

public class AdapterPickPhoto extends RecyclerView.Adapter<AdapterPickPhoto.ViewHolder>{
    private ArrayList<String> imagePaths;
    private Context context;
    private AddVenueInterface addVenueInterface;

    public AdapterPickPhoto(ArrayList<String> imagePaths, Context context, AddVenueInterface addVenueInterface) {
        this.imagePaths = imagePaths;
        this.context = context;
        this.addVenueInterface = addVenueInterface;
    }

    @NonNull
    @Override
    public AdapterPickPhoto.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutImageBinding binding = LayoutImageBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterPickPhoto.ViewHolder holder, int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.layoutImage);
        clickListener(holder, position);

        /*//remove the card add image if the list is equal to totalphoto
        if (imagePaths.size() == totalPhoto){
            holder.binding.cardAddImage.setVisibility(View.GONE);
        }

        //remove the button add image if the position of item not the last position of the list
        if (position != imagePaths.size()-1){
            holder.binding.cardAddImage.setVisibility(View.GONE);
        }

        //remove the initial imageview if the position is 0
        if (position == 0){
            holder.binding.layoutImageHeader.setVisibility(View.GONE);
            holder.binding.cardAddImage.setVisibility(View.VISIBLE);
        }*/

        Glide.with(context.getApplicationContext()).load(imagePaths.get(position)).into(holder.binding.imageHeader);
        //Glide.with(context).load(imagePaths.get(position)).override(500, 500).into(holder.binding.imageHeader);
    }

    private void clickListener(final ViewHolder holder, final int position) {
        holder.binding.layoutImage.setOnClickListener(v -> addVenueInterface.layoutImageClick(imagePaths, position, holder.binding.layoutImage));
        //holder.binding.iconDelete.setOnClickListener(view -> addVenueInterface.deleteImage(imagePaths, position));

        //holder.binding.cardAddImage.setOnClickListener(view -> addVenueInterface.addImage(imagePaths, position, holder.binding.cardAddImage));
    }

    @Override
    public int getItemCount() {
        if (imagePaths != null){
            return imagePaths.size();
        } else {
            return 0;
        }
    }

    public ArrayList<String> removeData(int position) {
        notifyItemRangeChanged(position, imagePaths.size());
        imagePaths.remove(position);
        notifyItemRemoved(position);

        if (imagePaths.size() % 3 == 0){
            new android.os.Handler().postDelayed(() -> notifyDataSetChanged(), 500);
        }

        return imagePaths;
    }

    public ArrayList<String> addData(String path, int position) {
        if (imagePaths.size() == 0){
            imagePaths.add(path);
            notifyDataSetChanged();
        } else {
            imagePaths.add(imagePaths.size()-1, path);
            notifyItemInserted(position);
        }

        return imagePaths;
    }

    public void updateData(ArrayList<String> imagePaths) {
        this.imagePaths = imagePaths;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutImageBinding binding;
        public ViewHolder(LayoutImageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AddVenueInterface{
        void layoutImageClick(ArrayList<String> imagePaths, int position, RelativeLayout layout);
        void addImage(ArrayList<String> imagePaths, int position, CardView cardView);
        void deleteImage(ArrayList<String> imagePaths, int position);
    }
}
