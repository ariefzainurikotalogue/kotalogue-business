package com.kotalogue.android_business.globalAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.databinding.LayoutItemVenueBinding;
import com.kotalogue.android_business.model.ModelVenue;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.ItemListEventBinding;

import java.util.ArrayList;

public class AdapterListVenue extends RecyclerView.Adapter<AdapterListVenue.ViewHolder> {
    private ArrayList<ModelVenue> modelVenues;
    private Context context;
    private AdapterListVenueInterface adapterListVenueInterface;
    private Activity activity;

    //variabel sementara
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    public AdapterListVenue(ArrayList<ModelVenue> modelVenues, Context context,
                            AdapterListVenueInterface adapterListVenueInterface, Activity activity) {
        this.modelVenues = modelVenues;
        this.context = context;
        this.adapterListVenueInterface = adapterListVenueInterface;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AdapterListVenue.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemVenueBinding binding = LayoutItemVenueBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterListVenue.ViewHolder holder, int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.layoutContentMain);

        holder.binding.venueName.setSelected(true);
        holder.binding.venueStatus.setSelected(true);

        dialogInit();

        boolean isMaster = context.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        setCustomMargin(holder, position);

        if (modelVenues.get(position).getUserID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            holder.binding.venueStatus.setText(context.getResources().getString(R.string.thisVenueIsInYourManagement));
            holder.binding.iconDelete.setVisibility(View.VISIBLE);
            holder.binding.iconDelete.setOnClickListener(v -> adapterListVenueInterface.deleteItem(modelVenues, position));
            holder.binding.layoutContentMain.setOnClickListener(v -> adapterListVenueInterface.itemClick(modelVenues, position));
        } else if (isMaster) {
            holder.binding.venueStatus.setText(context.getResources().getString(R.string.thisVenueIsNotInYourManagement));
            holder.binding.layoutContentMain.setOnClickListener(v -> adapterListVenueInterface.itemClick(modelVenues, position));
            holder.binding.iconDelete.setVisibility(View.GONE);
        } else {
            holder.binding.venueStatus.setText(context.getResources().getString(R.string.thisVenueIsNotInYourManagement));
            holder.binding.iconDelete.setVisibility(View.GONE);
            holder.binding.layoutContentMain.setOnClickListener(v -> openDialog());
        }

        holder.binding.venueName.setText(modelVenues.get(position).getVenueName());

        Glide.with(context.getApplicationContext()).load(modelVenues.get(position).getVenueHeaderPhoto())
                .placeholder(R.drawable.new_logo_v2)
                .into(holder.binding.imageHeader);

        //click listener
        holder.binding.iconDelete.setOnClickListener(view -> adapterListVenueInterface.deleteItem(modelVenues, position));
        holder.binding.iconShare.setOnClickListener(view -> PublicMethod.sharedDynamicsLink(context, activity,
                modelVenues.get(position).getVenueID(), "OpenFragmentDetailVenue"));
    }

    private void setCustomMargin(ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();

        if (position == 0){
            PublicMethod.setMargins(holder.binding.layoutContentMain, PublicMethod.dp(R.dimen.standart_20dp, context) ,PublicMethod.dp(R.dimen.fab_margin, context), PublicMethod.dp(R.dimen.standart_20dp, context), PublicMethod.dp(R.dimen.dp10, context));
        } else if (position == modelVenues.size() - 1){
            PublicMethod.setMargins(holder.binding.layoutContentMain, PublicMethod.dp(R.dimen.standart_20dp, context) ,PublicMethod.dp(R.dimen.dp10, context), PublicMethod.dp(R.dimen.standart_20dp, context), PublicMethod.dp(R.dimen.fab_margin, context));
            holder.binding.divider.setVisibility(View.GONE);
        }
    }

    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.cantEditVenue));
        builder.setMessage(context.getResources().getString(R.string.cantEdiVenuetMessage));
        builder.setPositiveButton(context.getResources().getString(R.string.cancel), (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        alertDialog.show();
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_loading, null);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        builder.setView(view);
        builder.setCancelable(false);
        dialogLoading = builder.create();
        dialogLoading.setCancelable(false);
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
    }

    public ArrayList<ModelVenue> removeData(int position) {
        notifyItemRangeChanged(position, modelVenues.size());
        modelVenues.remove(position);
        notifyItemRemoved(position);

        return modelVenues;
    }

    public ArrayList<ModelVenue> addData(ModelVenue modelVenue, int position) {
        if (modelVenues.size() == 0){
            modelVenues.add(modelVenue);
            notifyDataSetChanged();
        } else {
            modelVenues.add(modelVenues.size()-1, modelVenue);
            notifyItemInserted(position);
        }

        return modelVenues;
    }

    @Override
    public int getItemCount() {
        if (modelVenues != null){
            return modelVenues.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelVenue> modelVenues) {
        this.modelVenues = modelVenues;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutItemVenueBinding binding;
        public ViewHolder(LayoutItemVenueBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterListVenueInterface{
        void itemClick(ArrayList<ModelVenue> modelVenues, int position);
        void deleteItem(ArrayList<ModelVenue> modelVenues, int position);
    }
}
