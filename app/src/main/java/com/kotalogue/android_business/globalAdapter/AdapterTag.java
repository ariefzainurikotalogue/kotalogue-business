package com.kotalogue.android_business.globalAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kotalogue.android_business.databinding.LayoutItemTagBinding;

import java.util.ArrayList;

/**
 * Created by ariefzainuri on 21/04/18.
 */

public class AdapterTag extends RecyclerView.Adapter<AdapterTag.ViewHolder>{
    private ArrayList<String> listOfTag;
    private InterfaceTag interfaceTag;

    public AdapterTag(ArrayList<String> listOfTag, InterfaceTag interfaceTag) {
        this.listOfTag = listOfTag;
        this.interfaceTag = interfaceTag;
    }

    @NonNull
    @Override
    public AdapterTag.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemTagBinding binding  = LayoutItemTagBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterTag.ViewHolder holder, int position) {
        holder.binding.tag.setText(listOfTag.get(position));
        holder.binding.iconDelete.setOnClickListener(v -> interfaceTag.tagClick(listOfTag.get(position)));
    }

    @Override
    public int getItemCount() {
        if (listOfTag != null){
            return listOfTag.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<String> listOfTag) {
        this.listOfTag = listOfTag;
        notifyDataSetChanged();
    }

    public ArrayList<String> removeTag(int position) {
        notifyItemRangeChanged(position, listOfTag.size());
        listOfTag.remove(position);
        notifyItemRemoved(position);

        return listOfTag;
    }

    public ArrayList<String> addData(int position, String data) {
        if (listOfTag.size() == 0){
            listOfTag.add(data);
            notifyDataSetChanged();
        } else {
            listOfTag.add(listOfTag.size()-1, data);
            notifyItemInserted(position);
        }

        return listOfTag;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutItemTagBinding binding;
        public ViewHolder(LayoutItemTagBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface InterfaceTag{
        void tagClick(String tag);
    }
}
