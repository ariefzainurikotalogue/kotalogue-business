package com.kotalogue.android_business.fragmentDetailVenueFolder;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.globalAdapter.AdapterGallery;
import com.kotalogue.android_business.globalAdapter.AdapterPickPhoto;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelGallery;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentVenueGalleryBinding;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

public class FragmentVenueGallery extends Fragment implements AdapterGallery.AdapterGalleryInterface, BackPressObserver {
    private static final int SELECT_PICTURES = 1;
    private FragmentVenueGalleryBinding binding;

    //recyclerview
    private AdapterGallery adapterGallery;
    private ArrayList<ModelGallery> modelGalleries = new ArrayList<>();
    private Context mContext;
    private AlertDialog alertDialog;
    private boolean isMaster;
    private RelativeLayout layoutLoading;
    private boolean needBackPressedChecked = true;
    private String venueID, venueLatLon, venueAddress, venueName, brandID;
    private boolean noNewImage = true;
    private int addPosition;
    private ListenerRegistration galleryListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            venueID = bundle.getString("venueID");
            venueLatLon = bundle.getString("venueLatLon");
            venueAddress = bundle.getString("venueAddress");
            venueName = bundle.getString("venueName");
            brandID = bundle.getString("brandID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentVenueGalleryBinding.inflate(LayoutInflater.from(mContext), null, false);

        binding.venueGallery.setSelected(true);

        binding.venueInformation.setSelected(true);

        PublicMethod.enableDefaultAnimation(binding.layoutMenu);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);

            PublicMethod.setMargins(binding.layoutMenu, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogInit();

        recyclerInit();

        checkMaster();

        loadGallery();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void checkMaster() {
        boolean isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);
        if (isMaster){
            binding.btnSaveImage.setVisibility(View.GONE);
        }
    }

    private void clickListener() {
        binding.layoutBottomInformation.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomGallery.setOnClickListener(view -> { });

        binding.layoutBottomEvent.setOnClickListener(view -> {

            FragmentVenueEvent fragmentVenueEvent = new FragmentVenueEvent();
            Bundle bundle = new Bundle();
            bundle.putString("venueID", venueID);
            bundle.putString("brandID", brandID);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueName", venueName);
            fragmentVenueEvent.setArguments(bundle);

            getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentVenueEvent).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomSubVenue.setOnClickListener(view -> {

            FragmentVenueSubvenue fragmentVenueSubvenue = new FragmentVenueSubvenue();
            Bundle bundle = new Bundle();
            bundle.putString("venueID", venueID);
            bundle.putString("brandID", brandID);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueName", venueName);
            fragmentVenueSubvenue.setArguments(bundle);

            getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentVenueSubvenue).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.iconBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.btnSaveImage.setOnClickListener(view -> {
            for (int i=0; i< modelGalleries.size(); i++){
                if (!modelGalleries.get(i).getStatus().equals("Add")){
                    if (modelGalleries.get(i).isSavedToServer()){
                        noNewImage = true;
                    } else {
                        noNewImage = false;
                        break;
                    }
                }
            }

            if (noNewImage) {
                Toast.makeText(mContext, R.string.noNewPhotoAdded, Toast.LENGTH_SHORT).show();
            } else {
                saveNewPhoto();
            }
        });
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        alertDialog = builder.create();
        alertDialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        alertDialog.setCancelable(false);
    }

    private void recyclerInit(){
        adapterGallery = new AdapterGallery(modelGalleries, mContext, this);

        binding.recyclerGallery.setHasFixedSize(true);
        binding.recyclerGallery.setLayoutManager(new GridLayoutManager(mContext, 3));
        binding.recyclerGallery.setAdapter(adapterGallery);

        binding.recyclerGallery.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0){
                    hideBottom();
                } else if (dy < 0) {
                    showBottom();
                }
            }
        });
    }

    private void showBottom() {
        binding.layoutBottomInformation.setVisibility(View.VISIBLE);
        binding.layoutBottomGallery.setVisibility(View.VISIBLE);
        binding.layoutBottomEvent.setVisibility(View.VISIBLE);
        binding.layoutBottomSubVenue.setVisibility(View.VISIBLE);
    }

    private void hideBottom() {
        binding.layoutBottomInformation.setVisibility(View.GONE);
        binding.layoutBottomGallery.setVisibility(View.GONE);
        binding.layoutBottomEvent.setVisibility(View.GONE);
        binding.layoutBottomSubVenue.setVisibility(View.GONE);
    }

    private void loadGallery(){
        galleryListener =  FirebaseFirestore.getInstance().collection("Venue").document(venueID).collection("VenuePhotoGallery")
                .addSnapshotListener((documentSnapshots, e) -> {

                    if (documentSnapshots != null && documentSnapshots.size() > 0){
                        modelGalleries.clear();

                        modelGalleries = adapterGallery.addData(new ModelGallery("", "", "Add", false, null), modelGalleries.size()-1);

                        for (DocumentSnapshot document: documentSnapshots){
                            ModelGallery modelGallery = document.toObject(ModelGallery.class);
                            modelGallery.setSavedToServer(true);
                            modelGallery.setStatus("Image");
                            modelGallery.setPhotoUri(null);
                            modelGalleries.add(modelGallery);
                            hideLoading();
                        }

                        Collections.swap(modelGalleries, 0, modelGalleries.size()-1);
                        addPosition = modelGalleries.size()-1;
                        adapterGallery.updateData(modelGalleries);
                    } else {
                        if (PublicMethod.isDeviceOnline(mContext)){
                            hideLoading();
                            modelGalleries = adapterGallery.addData(new ModelGallery("", "", "Add", false, null), modelGalleries.size()-1);
                            Toast.makeText(mContext, R.string.emptyData, Toast.LENGTH_SHORT).show();
                        } else {
                            hideLoading();
                            modelGalleries = adapterGallery.addData(new ModelGallery("", "", "Add", false, null), modelGalleries.size()-1);
                            Toast.makeText(mContext, R.string.checkInternetConnection, Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void saveNewPhoto() {
        for (int i = 0 ; i < modelGalleries.size(); i++) {
            if (!modelGalleries.get(i).isSavedToServer() && modelGalleries.get(i).getStatus().equals("Image")){
                final String imageID = UUID.randomUUID().toString();
                Uri uri = modelGalleries.get(i).getPhotoUri();

                StorageReference storageRef = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(imageID);
                storageRef.putFile(uri).continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return storageRef.getDownloadUrl();
                }).addOnSuccessListener(imageUrl -> {
                    ModelGallery modelPhoto = new ModelGallery(imageID, imageUrl.toString(), "Image", true, null);
                    FirebaseFirestore.getInstance().collection("Venue").document(venueID)
                            .collection("VenuePhotoGallery").document(imageID).set(modelPhoto);
                }).addOnFailureListener(e -> {
                    if (PublicMethod.isDeviceOnline(mContext)) {
                        Toast.makeText(mContext, R.string.noNewPhotoAdded, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

        recreateFragment();
        noNewImage = true;
    }

    private void recreateFragment() {
        FragmentVenueGallery fragmentVenueGallery = new FragmentVenueGallery();
        Bundle bundle = new Bundle();
        bundle.putString("venueID", venueID);
        bundle.putString("venueLatLon", venueLatLon);
        bundle.putString("venueAddress", venueAddress);
        bundle.putString("venueName", venueName);
        bundle.putString("brandID", brandID);
        fragmentVenueGallery.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentVenueGallery).commit();
    }

    private void showDialogLoading() {
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    @Override
    public void galleryClick(ArrayList<ModelGallery> modelGalleries, int position, CardView cardView) {
        galleryListener.remove();

        ModelGallery modelGallerySementara = modelGalleries.get(position);

        PopupMenu popupMenu = new PopupMenu(mContext, cardView);
        popupMenu.getMenuInflater().inflate(R.menu.menu_image, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.delete:
                    if (modelGalleries.get(position).isSavedToServer()) {
                        this.modelGalleries = adapterGallery.removeData(position);

                        addPosition = this.modelGalleries.size()-1;

                        FirebaseFirestore.getInstance().collection("Venue").document(venueID)
                                .collection("VenuePhotoGallery").document(modelGallerySementara.getPhotoID())
                                .delete();

                        FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(modelGallerySementara.getPhotoID())
                                .delete();

                    } else {
                        this.modelGalleries = adapterGallery.removeData(position);
                        addPosition = this.modelGalleries.size()-1;
                    }
                    break;
            }
            return false;
        });
        popupMenu.show();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void addGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*"); //allows any image file type. Change * to specific extension to limit it
        //**These following line is the important one!
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURES);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case SELECT_PICTURES:
                if (resultCode == RESULT_OK){
                    if (data.getClipData() != null) {
                        int count = data.getClipData().getItemCount();
                        for (int i=0; i<count; i++){
                            Uri imageUri = data.getClipData().getItemAt(i).getUri();

                            String id = UUID.randomUUID().toString();
                            ModelGallery modelGallery = new ModelGallery(id, "", "Image", false, imageUri);
                            modelGalleries.add(modelGallery);
                            adapterGallery.updateData(modelGalleries);

                            /*BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            try {
                                BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(Uri.fromFile(new File(imageUri.getPath()))), null, options);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }

                            double imageHeight = options.outHeight;
                            double imageWidth = options.outWidth;

                            if (imageWidth > 1920 || imageHeight > 1920){
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.maxWidthHeight), Toast.LENGTH_SHORT).show();
                            } else if (imageWidth < 300 || imageHeight < 300){
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.minWidthHeight), Toast.LENGTH_SHORT).show();
                            } else {

                            }*/
                        }

                        Collections.swap(modelGalleries, addPosition, modelGalleries.size()-1);
                        addPosition = modelGalleries.size()-1;
                        noNewImage = false;
                    } else if (data.getData() != null) {
                        Uri imageUri = data.getData();
                        String id = UUID.randomUUID().toString();
                        ModelGallery modelGallery = new ModelGallery(id, "", "Image", false, imageUri);
                        modelGalleries.add(modelGallery);
                        adapterGallery.updateData(modelGalleries);

                        Collections.swap(modelGalleries, addPosition, modelGalleries.size()-1);
                        addPosition = modelGalleries.size()-1;
                        noNewImage = false;
                        /*BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        try {
                            BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(Uri.fromFile(new File(imagePath))), null, options);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        double imageHeight = options.outHeight;
                        double imageWidth = options.outWidth;

                        if (imageWidth > 1920 || imageHeight > 1920){
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.maxWidthHeight), Toast.LENGTH_SHORT).show();
                        } else if (imageWidth < 300 || imageHeight < 300){
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.minWidthHeight), Toast.LENGTH_SHORT).show();
                        } else {

                        }*/
                    }
                }
                break;
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needBackPressedChecked;
    }

    @Override
    public void onBackPress() {
        if (!noNewImage){
            needBackPressedChecked = false;
            openUnsavedDialog();
        } else {
            needBackPressedChecked = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        }
    }

    private void openUnsavedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.eventGallery);
        builder.setMessage(R.string.galleryHasImageNotSaved);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.save, (dialogInterface, i) -> {
            dialogInterface.dismiss();
            saveNewPhoto();
        });
        builder.setNegativeButton(R.string.dismiss, (dialogInterface, i) -> {
            dialogInterface.dismiss();
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
