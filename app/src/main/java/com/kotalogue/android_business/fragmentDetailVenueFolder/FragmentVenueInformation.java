package com.kotalogue.android_business.fragmentDetailVenueFolder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.databinding.FragmentVenueInformationBinding;
import com.kotalogue.android_business.fragment.FragmentAddEvent;
import com.kotalogue.android_business.fragment.FragmentAddVenue;
import com.kotalogue.android_business.fragment.TimePickerFragment;
import com.kotalogue.android_business.globalAdapter.AdapterTag;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelVenue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static com.kotalogue.android_business.classHelper.PublicMethod.checkSocMedia;

public class FragmentVenueInformation extends Fragment implements AdapterTag.InterfaceTag, BackPressObserver, TimePickerFragment.TimePickerInterface {
    private final int REQUEST_IMAGE_CAPTURE = 1;
    private final int SELECT_PICTURE = 2;
    private final int PLACE_PICKER_REQUEST = 3;

    private FragmentVenueInformationBinding binding;

    //intent
    private String venueID;

    //recycler view
    private AdapterTag adapterTag;
    private ArrayList<String> listOfTag = new ArrayList<>();
    private Context mContext;
    private String venueLatLon;
    private String venueHeaderPhoto;
    private String brandID;
    private String venueParentID;
    private Bitmap photoBitmap;

    private boolean isUpdateImageCanceled = true;
    private boolean isImageUpdate = false;

    private ModelVenue modelVenueSementara;
    private boolean venueIsManagedByBusiness;
    private AlertDialog alertDialog;
    private String imageHeaderFrom = "a";
    private Uri selectedImageUri;
    private String mCurrentPhotoPath;
    private boolean isMaster;
    private boolean isDailyScheduleOpen = false;
    private boolean isSocialMediaOpen = false;
    private boolean isTagChange = false;
    private RelativeLayout layoutLoading;
    private boolean needBackPressedChecked = true;
    private String venueAddress;
    private String venueName;
    private LinearLayout rootLayout;
    private String destination = "";

    private String venueUsername;
    private boolean needUsernameChecked = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            venueID = bundle.getString("venueID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentVenueInformationBinding.inflate(LayoutInflater.from(mContext), null, false);

        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutTextSocialMedia);
        PublicMethod.enableDefaultAnimation(binding.textLayoutDailySchedule);
        PublicMethod.enableDefaultAnimation(binding.nestedScrollView);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));

        binding.recyclerTag.setNestedScrollingEnabled(false);

        binding.venueInformation.setSelected(true);
        binding.textTapTheDay.setSelected(true);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);

            PublicMethod.setMargins(binding.layoutMenu, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogInit();

        checkMaster();

        recyclerInit();

        loadDetailVenue();
    }

    private void doCheckUsername() {
        FirebaseFirestore.getInstance().collection("VenueUsername").get().addOnCompleteListener(task -> {
            ArrayList<String> arrayList = new ArrayList<>();
            for (DocumentSnapshot document : task.getResult()){
                arrayList.add(document.getString("venueUsername"));
            }

            if (arrayList.contains(binding.inputVenueUsername.getText().toString().trim())){
                Toast.makeText(getContext(), "This username is already used", Toast.LENGTH_SHORT).show();
                needUsernameChecked = true;
            } else {
                Toast.makeText(getContext(), "This username is available to use", Toast.LENGTH_SHORT).show();
                needUsernameChecked = false;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
        nestedScrollListener();
    }

    private void nestedScrollListener() {
        binding.nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (view, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY == (view.getChildAt(0).getMeasuredHeight() - view.getMeasuredHeight())){
                Log.d(TAG, "BOTTOM SCROLL");
            }

            if (scrollY > oldScrollY){
                Log.d(TAG, "SCROLL DOWN");
                binding.layoutBottomInformation.setVisibility(View.GONE);
                binding.layoutBottomGallery.setVisibility(View.GONE);
                binding.layoutBottomEvent.setVisibility(View.GONE);
                binding.layoutBottomSubVenue.setVisibility(View.GONE);

                //custom margin
                PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                        PublicMethod.dp(R.dimen.standart_20dp, mContext));
            }

            if (scrollY < oldScrollY){
                Log.d(TAG, "SCROLL UP");
                binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                binding.layoutBottomGallery.setVisibility(View.VISIBLE);
                binding.layoutBottomEvent.setVisibility(View.VISIBLE);
                binding.layoutBottomSubVenue.setVisibility(View.VISIBLE);

                //custom margin
                PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                        PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
            }

            if (scrollY == 0){
                Log.d(TAG, "TOP SCROLL");
            }
        });
    }

    private void checkMaster() {
        boolean isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        if (isMaster){
            binding.btnSaveChange.setVisibility(View.GONE);
        }
    }

    private void createEvent(){
        FragmentAddEvent fragmentAddEvent = new FragmentAddEvent();
        Bundle bundle = new Bundle();
        bundle.putString("venueLatLon", venueLatLon);
        bundle.putString("venueAddress", venueAddress);
        bundle.putString("venueID", venueID);
        bundle.putString("venueName", venueName);
        bundle.putString("brandID", brandID);
        fragmentAddEvent.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddEvent).addToBackStack("").commit();
    }

    private void createSubVenue(){
        FragmentAddVenue fragmentAddVenue = new FragmentAddVenue();
        Bundle bundle = new Bundle();
        bundle.putString("brandID", brandID);
        bundle.putString("venueID", venueID);
        bundle.putString("specificVenue", "specificVenue");
        bundle.putString("venueLatLon", venueLatLon);
        bundle.putString("venueAddress", venueAddress);
        bundle.putString("venueName", venueName);
        bundle.putString("createSubvenue", "createSubvenue");
        fragmentAddVenue.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddVenue).addToBackStack("").commit();
    }

    private void clickListener() {
        binding.txtCheckUsername.setOnClickListener(v -> doCheckUsername());

        binding.iconAdd.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            View dialogVenueView = LayoutInflater.from(mContext).inflate(R.layout.dialog_venue_view, null);
            builder.setView(dialogVenueView);

            TextView createSubvenue = dialogVenueView.findViewById(R.id.createSubVenue);
            TextView createEvent = dialogVenueView.findViewById(R.id.createEvent);
            rootLayout = dialogVenueView.findViewById(R.id.rootLayoutDialog);
            createSubvenue.setSelected(true);
            createEvent.setSelected(true);

            AlertDialog dialogMenu = builder.create();
            dialogMenu.show();
            dialogMenu.getWindow().setLayout(PublicMethod.getViewWidth(rootLayout) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

            createSubvenue.setOnClickListener(view1 -> {
                dialogMenu.dismiss();
                createSubVenue();
            });

            createEvent.setOnClickListener(view1 -> {
                dialogMenu.dismiss();
                createEvent();
            });
        });

        binding.layoutBottomInformation.setOnClickListener(view -> { });

        binding.layoutBottomGallery.setOnClickListener(view -> {
            destination = "gallery";

            if (isImageUpdate){
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateVenueName.getText().toString().equals(modelVenueSementara.getVenueName())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.venueCategories.getText().toString().equals(modelVenueSementara.getVenueCategories())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateDescription.getText().toString().equals(modelVenueSementara.getVenueLongDesc())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.venueAddress.getText().toString().equals(modelVenueSementara.getVenueAddress())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updatePhone.getText().toString().equals(modelVenueSementara.getVenuePhoneNumber())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelVenueSementara.getVenueFacebook())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelVenueSementara.getVenueTwitter())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelVenueSementara.getVenueInstagram())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelVenueSementara.getVenueWhatsapp())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelVenueSementara.getVenueWebsite())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateEmail.getText().toString().equals(modelVenueSementara.getVenueEmail())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.inputPeriodScheduleTime.getText().toString().equals(modelVenueSementara.getOverWriteTime())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (isTagChange) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            }

            bottomGallery();
        });

        binding.layoutBottomEvent.setOnClickListener(view -> {
            destination = "event";

            if (isImageUpdate){
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateVenueName.getText().toString().equals(modelVenueSementara.getVenueName())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.venueCategories.getText().toString().equals(modelVenueSementara.getVenueCategories())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateDescription.getText().toString().equals(modelVenueSementara.getVenueLongDesc())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.venueAddress.getText().toString().equals(modelVenueSementara.getVenueAddress())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updatePhone.getText().toString().equals(modelVenueSementara.getVenuePhoneNumber())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelVenueSementara.getVenueFacebook())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelVenueSementara.getVenueTwitter())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelVenueSementara.getVenueInstagram())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelVenueSementara.getVenueWhatsapp())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelVenueSementara.getVenueWebsite())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateEmail.getText().toString().equals(modelVenueSementara.getVenueEmail())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.inputPeriodScheduleTime.getText().toString().equals(modelVenueSementara.getOverWriteTime())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (isTagChange) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            }

            bottomEvent();
        });

        binding.layoutBottomSubVenue.setOnClickListener(view -> {
            destination = "subVenue";

            if (isImageUpdate){
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateVenueName.getText().toString().equals(modelVenueSementara.getVenueName())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.venueCategories.getText().toString().equals(modelVenueSementara.getVenueCategories())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateDescription.getText().toString().equals(modelVenueSementara.getVenueLongDesc())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.venueAddress.getText().toString().equals(modelVenueSementara.getVenueAddress())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updatePhone.getText().toString().equals(modelVenueSementara.getVenuePhoneNumber())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelVenueSementara.getVenueFacebook())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelVenueSementara.getVenueTwitter())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelVenueSementara.getVenueInstagram())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelVenueSementara.getVenueWhatsapp())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelVenueSementara.getVenueWebsite())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.updateEmail.getText().toString().equals(modelVenueSementara.getVenueEmail())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (!binding.inputPeriodScheduleTime.getText().toString().equals(modelVenueSementara.getOverWriteTime())) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            } else if (isTagChange) {
                needBackPressedChecked = false;
            openUnsavedDialog();
            return;
            }

            bottomSubVenue();
        });

        binding.toogleDailySchedule.setOnClickListener(v -> {
            if (isDailyScheduleOpen){
                binding.toogleDailySchedule.setText(R.string.show);
                binding.layoutDailySchedule.setVisibility(View.GONE);
                isDailyScheduleOpen = false;
            } else {
                binding.toogleDailySchedule.setText(R.string.hide);
                binding.layoutDailySchedule.setVisibility(View.VISIBLE);
                isDailyScheduleOpen = true;
            }
        });

        binding.toogleSocialMedia.setOnClickListener(v -> {
            if (isSocialMediaOpen){
                binding.toogleSocialMedia.setText(R.string.show);
                binding.layoutSocialMedia.setVisibility(View.GONE);
                isSocialMediaOpen = false;
            } else {
                binding.toogleSocialMedia.setText(R.string.hide);
                binding.layoutSocialMedia.setVisibility(View.VISIBLE);
                isSocialMediaOpen = true;
            }
        });

        binding.inputVenueTag.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addTag();
            }
            return true;
        });

        binding.venueCategories.setOnClickListener(v -> chooseCategories());
        binding.iconEditHeaderPhoto.setOnClickListener(v -> openPopUpPhoto());
        binding.iconEditAddress.setOnClickListener(v -> openPlacePicker());
        binding.btnSaveChange.setOnClickListener(v -> {
            if (isImageUpdate){
                if (needUsernameChecked) {
                    if (!binding.inputVenueUsername.getText().toString().trim().equals(venueUsername)){
                        Toast.makeText(mContext, "Please check your venue username first", Toast.LENGTH_SHORT).show();
                        PublicMethod.focusToView(binding.txtCheckUsername);
                        return;
                    }
                }

                updateImage();
            } else {
                saveUpdateVenue(venueHeaderPhoto);
            }
        });

        binding.iconBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
        });

        binding.openSunday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openSunday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeSunday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeSunday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.openMonday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openMonday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeMonday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeMonday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.openTuesday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openTuesday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeTuesday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeTuesday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.openWednesday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openWednesday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeWednesday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeWednesday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.openThursday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openThursday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeThursday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeThursday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.openFriday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openFriday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeFriday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeFriday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.openSaturday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.openSaturday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.closeSaturday.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.closeSaturday, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });
        binding.sunday.setOnClickListener(v -> {
            binding.openSunday.setText(mContext.getString(R.string.closed));
            binding.closeSunday.setText(mContext.getString(R.string.closed));
        });

        binding.monday.setOnClickListener(v -> {
            binding.openMonday.setText(mContext.getString(R.string.closed));
            binding.closeMonday.setText(mContext.getString(R.string.closed));
        });

        binding.tuesday.setOnClickListener(v -> {
            binding.openTuesday.setText(mContext.getString(R.string.closed));
            binding.closeTuesday.setText(mContext.getString(R.string.closed));
        });

        binding.wednesday.setOnClickListener(v -> {
            binding.openWednesday.setText(mContext.getString(R.string.closed));
            binding.closeWednesday.setText(mContext.getString(R.string.closed));
        });

        binding.thursday.setOnClickListener(v -> {
            binding.openThursday.setText(mContext.getString(R.string.closed));
            binding.closeThursday.setText(mContext.getString(R.string.closed));
        });

        binding.friday.setOnClickListener(v -> {
            binding.openFriday.setText(mContext.getString(R.string.closed));
            binding.closeFriday.setText(mContext.getString(R.string.closed));
        });

        binding.saturday.setOnClickListener(v -> {
            binding.openSaturday.setText(mContext.getString(R.string.closed));
            binding.closeSaturday.setText(mContext.getString(R.string.closed));
        });
}

    private void bottomSubVenue() {
        FragmentVenueSubvenue fragmentVenueSubvenue = new FragmentVenueSubvenue();
        Bundle bundle = new Bundle();
        bundle.putString("venueID", venueID);
        bundle.putString("venueLatLon", venueLatLon);
        bundle.putString("venueAddress", venueAddress);
        bundle.putString("venueName", venueName);
        bundle.putString("brandID", brandID);
        fragmentVenueSubvenue.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentVenueSubvenue).addToBackStack("").commit();
    }

    private void bottomEvent() {
        FragmentVenueEvent fragmentVenueEvent = new FragmentVenueEvent();
        Bundle bundle = new Bundle();
        bundle.putString("venueID", venueID);
        bundle.putString("venueLatLon", venueLatLon);
        bundle.putString("venueAddress", venueAddress);
        bundle.putString("venueName", venueName);
        bundle.putString("brandID", brandID);
        fragmentVenueEvent.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentVenueEvent).addToBackStack("").commit();
    }

    private void bottomGallery() {
        FragmentVenueGallery fragmentVenueGallery = new FragmentVenueGallery();
        Bundle bundle = new Bundle();
        bundle.putString("venueID", venueID);
        bundle.putString("venueLatLon", venueLatLon);
        bundle.putString("venueAddress", venueAddress);
        bundle.putString("venueName", venueName);
        bundle.putString("brandID", brandID);
        fragmentVenueGallery.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentVenueGallery).addToBackStack("").commit();
    }

    private void addTag() {
        if (binding.inputVenueTag.getText().toString().trim().equals("")){
            Toast.makeText(mContext, R.string.masukanTag, Toast.LENGTH_SHORT).show();
            return;
        }

        isTagChange = true;

        listOfTag = adapterTag.addData(listOfTag.size() - 1, binding.inputVenueTag.getText().toString().trim());

        binding.inputVenueTag.setText(null);

    }

    private void chooseCategories() {
        PopupMenu popupMenu = new PopupMenu(mContext, binding.venueCategories);
        popupMenu.getMenuInflater().inflate(R.menu.menu_categories, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.food:
                    binding.venueCategories.setText(R.string.foodBeverages);
                    break;

                case R.id.shopping:
                    binding.venueCategories.setText(R.string.shoppingCenters);
                    break;

                case R.id.automotive:
                    binding.venueCategories.setText(R.string.automotive);
                    break;

                case R.id.fashion:
                    binding.venueCategories.setText(R.string.fashion);
                    break;

                case R.id.health:
                    binding.venueCategories.setText(R.string.health);
                    break;

                case R.id.electronics:
                    binding.venueCategories.setText(R.string.electronics);
                    break;

                case R.id.houseHolds:
                    binding.venueCategories.setText(R.string.houseHold);
                    break;

                case R.id.souvenirs:
                    binding.venueCategories.setText(R.string.souvenir);
                    break;

                case R.id.tourist:
                    binding.venueCategories.setText(R.string.tourist);
                    break;

                case R.id.emergencies:
                    binding.venueCategories.setText(R.string.emergencies);
                    break;
            }
            return false;
        });

        popupMenu.show();
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        alertDialog = builder.create();
        alertDialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        alertDialog.setCancelable(false);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mContext,
                        "com.kotalogue.android_business",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void updateImage() {
        //memasukan gambar ke firebase storage
        showDialogLoading();

        switch (imageHeaderFrom){
            case "storage":
                StorageReference storageReferenceVenue = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(venueID);
                UploadTask uploadGambarBarangVenue = storageReferenceVenue.putFile(selectedImageUri);

                uploadGambarBarangVenue.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReferenceVenue.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        saveUpdateVenue(downloadUri.toString());
                    } else {
                        Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

            case "camera":
                StorageReference storageReference = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(venueID);
                UploadTask uploadGambarBarang = storageReference.putFile(Uri.fromFile(new File(mCurrentPhotoPath)));

                uploadGambarBarang.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        saveUpdateVenue(downloadUri.toString());

                    } else {
                        Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

                default:
                    binding.imageHeader.setDrawingCacheEnabled(true);
                    binding.imageHeader.buildDrawingCache();
                    Bitmap bitmap2 = binding.imageHeader.getDrawingCache();
                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
                    byte[] data2 = baos2.toByteArray();

                    StorageReference storageReference2 = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(venueID);
                    UploadTask uploadGambarBarang2 = storageReference2.putBytes(data2);
                    uploadGambarBarang2.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return storageReference2.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();

                            saveUpdateVenue(downloadUri.toString());
                        } else {
                            Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
                    break;
        }
    }

    private void showDialogLoading() {
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void saveUpdateVenue(String venueHeaderPhoto) {
        if (needUsernameChecked){
            if (!binding.inputVenueUsername.getText().toString().trim().equals(venueUsername)){
                Toast.makeText(mContext, "Please check your venue username first", Toast.LENGTH_SHORT).show();
                PublicMethod.focusToView(binding.txtCheckUsername);
                return;
            }
        }

        showDialogLoading();

        String dSunday = String.valueOf(PublicMethod.dateStringToLong(binding.openSunday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeSunday.getText().toString().trim(), "kk:mm"));
        String dMonday = String.valueOf(PublicMethod.dateStringToLong(binding.openMonday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeMonday.getText().toString().trim(), "kk:mm"));
        String dTuesday = String.valueOf(PublicMethod.dateStringToLong(binding.openTuesday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeTuesday.getText().toString().trim(), "kk:mm"));
        String dWednesday = String.valueOf(PublicMethod.dateStringToLong(binding.openWednesday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeWednesday.getText().toString().trim(), "kk:mm"));
        String dThursday = String.valueOf(PublicMethod.dateStringToLong(binding.openThursday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeThursday.getText().toString().trim(), "kk:mm"));
        String dFriday = String.valueOf(PublicMethod.dateStringToLong(binding.openFriday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeFriday.getText().toString().trim(), "kk:mm"));
        String dSaturday = String.valueOf(PublicMethod.dateStringToLong(binding.openSaturday.getText().toString().trim(), "kk:mm")) + "," + String.valueOf(PublicMethod.dateStringToLong(binding.closeSaturday.getText().toString().trim(), "kk:mm"));

        ModelVenue modelVenue = new ModelVenue(listOfTag, binding.inputPeriodScheduleTime.getText().toString(), brandID, FirebaseAuth.getInstance().getCurrentUser().getUid(), venueID, venueParentID,
                binding.venueCategories.getText().toString().trim(), binding.inputVenueUsername.getText().toString().trim(), binding.updateVenueName.getText().toString().trim(), binding.venueAddress.getText().toString(),
                binding.updatePhone.getText().toString().trim(), venueHeaderPhoto, binding.updateDescription.getText().toString().trim(), venueLatLon,
                binding.updateFacebook.getText().toString().trim(), binding.updateTwitter.getText().toString().trim(), binding.updateInstagram.getText().toString().trim(),
                binding.updateWhatsApp.getText().toString().trim(), binding.updateWebsite.getText().toString().trim(), binding.updateEmail.getText().toString().trim(),
                dSunday, dMonday, dTuesday, dWednesday, dThursday, dFriday, dSaturday, venueIsManagedByBusiness);

        updateVenue(modelVenue);
    }

    private void updateVenue(ModelVenue modelVenue) {
        FirebaseFirestore.getInstance().collection("Venue").document(venueID).set(modelVenue)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        alertDialog.dismiss();

                        //change the flag of tag change so the system will not check tag again if user
                        //directly back pressed after save the data
                        isTagChange = false;

                        //change the flag of back pressed checker
                        needBackPressedChecked = true;

                        Snackbar.make(getView(), mContext.getResources().getString(R.string.succesUpdateVenue), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (PublicMethod.isDeviceOnline(mContext)){
                            Toast.makeText(mContext, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void openPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void recyclerInit(){
        adapterTag = new AdapterTag(null, this);

        binding.recyclerTag.setHasFixedSize(true);
        binding.recyclerTag.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL));
        binding.recyclerTag.setAdapter(adapterTag);
    }

    private void loadDetailVenue() {
        FirebaseFirestore.getInstance().collection("Venue").document(venueID).addSnapshotListener((documentSnapshot, e) -> {

            listOfTag = new ArrayList<>();

            if (documentSnapshot != null && documentSnapshot.exists()){
                modelVenueSementara = documentSnapshot.toObject(ModelVenue.class);

                binding.setVenue(modelVenueSementara);

                new Handler().postDelayed(() -> {
                    hideLoading();
                    binding.layoutContentMain.setVisibility(View.VISIBLE);
                }, 500);

                venueUsername = modelVenueSementara.getVenueUniqueName();
                venueLatLon = modelVenueSementara.getVenueLatLon();
                venueHeaderPhoto = modelVenueSementara.getVenueHeaderPhoto();
                venueIsManagedByBusiness = modelVenueSementara.isVenueIsManagedByBusiness();
                brandID = modelVenueSementara.getBrandID();
                venueParentID = modelVenueSementara.getVenueParentID();
                venueAddress = modelVenueSementara.getVenueAddress();
                venueName = modelVenueSementara.getVenueName();
                String facebook = modelVenueSementara.getVenueFacebook();
                String twitter = modelVenueSementara.getVenueTwitter();
                String instagram = modelVenueSementara.getVenueInstagram();
                String whatsapp = modelVenueSementara.getVenueWhatsapp();
                String website = modelVenueSementara.getVenueWebsite();
                String email = modelVenueSementara.getVenueEmail();
                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + modelVenueSementara.getVenueLatLon() + "&zoom=16&size=400x200";

                //load image header
                Glide.with(mContext.getApplicationContext()).load(modelVenueSementara.getVenueHeaderPhoto()).into(binding.imageHeader);

                //load static map
                Glide.with(mContext.getApplicationContext()).load(mapURL).into(binding.venueStaticMap);

                loadPeriodTime(modelVenueSementara.getOverWriteTime());

                loadDailyTime(modelVenueSementara);

                if (modelVenueSementara.getTag() != null){
                    adapterTag.updateData(modelVenueSementara.getTag());

                    if (modelVenueSementara.getTag().size() > 0){
                        listOfTag = modelVenueSementara.getTag();
                    }
                }

                checkSocMedia(facebook, twitter, instagram, whatsapp, website, email, binding.updateFacebook, binding.updateTwitter,
                        binding.updateInstagram, binding.updateWhatsApp, binding.updateWebsite, binding.updateEmail);

            } else {
                Toast.makeText(mContext.getApplicationContext(), R.string.makeSureConnected, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void loadPeriodTime(String overWriteTime) {
        if (overWriteTime != null && !overWriteTime.equals("")){
            binding.inputPeriodScheduleTime.setText(overWriteTime);
        }
    }

    private void loadDailyTime(ModelVenue modelVenueSementara) {
        if (modelVenueSementara.getSunday() != null && modelVenueSementara.getMonday() != null
                && modelVenueSementara.getTuesday() != null && modelVenueSementara.getWednesday() != null
                && modelVenueSementara.getThursday() != null && modelVenueSementara.getFriday() != null
                && modelVenueSementara.getSaturday() != null){

            String[] arrSunday = modelVenueSementara.getSunday().split(",");
            String[] arrMonday = modelVenueSementara.getMonday().split(",");
            String[] arrTuesday = modelVenueSementara.getTuesday().split(",");
            String[] arrWednesday = modelVenueSementara.getWednesday().split(",");
            String[] arrThursday = modelVenueSementara.getThursday().split(",");
            String[] arrFriday = modelVenueSementara.getFriday().split(",");
            String[] arrSaturday = modelVenueSementara.getSaturday().split(",");
            String openSunday = PublicMethod.dateLongToString(Long.valueOf(arrSunday[0]), "kk:mm", mContext);
            String closeSunday = PublicMethod.dateLongToString(Long.valueOf(arrSunday[1]), "kk:mm", mContext);
            String openMonday = PublicMethod.dateLongToString(Long.valueOf(arrMonday[0]), "kk:mm", mContext);
            String closeMonday = PublicMethod.dateLongToString(Long.valueOf(arrMonday[1]), "kk:mm", mContext);
            String openTuesday = PublicMethod.dateLongToString(Long.valueOf(arrTuesday[0]), "kk:mm", mContext);
            String closeTuesday = PublicMethod.dateLongToString(Long.valueOf(arrTuesday[1]), "kk:mm", mContext);
            String openWednesday = PublicMethod.dateLongToString(Long.valueOf(arrWednesday[0]), "kk:mm", mContext);
            String closeWednesday = PublicMethod.dateLongToString(Long.valueOf(arrWednesday[1]), "kk:mm", mContext);
            String openThursday = PublicMethod.dateLongToString(Long.valueOf(arrThursday[0]), "kk:mm", mContext);
            String closeThursday = PublicMethod.dateLongToString(Long.valueOf(arrThursday[1]), "kk:mm", mContext);
            String openFriday = PublicMethod.dateLongToString(Long.valueOf(arrFriday[0]), "kk:mm", mContext);
            String closeFriday = PublicMethod.dateLongToString(Long.valueOf(arrFriday[1]), "kk:mm", mContext);
            String openSaturday = PublicMethod.dateLongToString(Long.valueOf(arrSaturday[0]), "kk:mm", mContext);
            String closeSaturday = PublicMethod.dateLongToString(Long.valueOf(arrSaturday[1]), "kk:mm", mContext);

            binding.openSunday.setText(openSunday);
            binding.openMonday.setText(openMonday);
            binding.openTuesday.setText(openTuesday);
            binding.openWednesday.setText(openWednesday);
            binding.openThursday.setText(openThursday);
            binding.openFriday.setText(openFriday);
            binding.openSaturday.setText(openSaturday);

            binding.closeSunday.setText(closeSunday);
            binding.closeMonday.setText(closeMonday);
            binding.closeTuesday.setText(closeTuesday);
            binding.closeWednesday.setText(closeWednesday);
            binding.closeThursday.setText(closeThursday);
            binding.closeFriday.setText(closeFriday);
            binding.closeSaturday.setText(closeSaturday);
        } else {
            binding.openSunday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.openMonday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.openTuesday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.openWednesday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.openThursday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.openFriday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.openSaturday.setText(mContext.getResources().getString(R.string.dayNotSet));

            binding.closeSunday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.closeMonday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.closeTuesday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.closeWednesday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.closeThursday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.closeFriday.setText(mContext.getResources().getString(R.string.dayNotSet));
            binding.closeSaturday.setText(mContext.getResources().getString(R.string.dayNotSet));
        }
    }

    private void openPopUpPhoto() {
        PopupMenu popupMenu = new PopupMenu(mContext, binding.iconEditHeaderPhoto);
        popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.camera:
                    dispatchTakePictureIntent();
                    break;

                case R.id.storage:
                    Intent intentStorage = new Intent();
                    intentStorage.setType("image/*");
                    intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE);
                    break;
            }
            return false;
        });

        popupMenu.show();
    }

    private void getDropboxIMGSize(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(
                    getContext().getContentResolver().openInputStream(uri),
                    null,
                    options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double imageHeight = options.outHeight;
        double imageWidth = options.outWidth;
        double finalValue = imageWidth / imageHeight;

        if (imageHeight < 300 || imageWidth < 300){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
        } else {
            //change the flag that user is change the image
            isImageUpdate = true;

            //change the flag to true so we can update the image after user change the photo
            isUpdateImageCanceled = true;

            binding.iconEdit.setImageResource(R.drawable.icon_cancel);
            binding.textPhotoCover.setText(R.string.undo);
            binding.imageHeader.setImageURI(uri);
        }

        /*if (finalValue >= 0.5 && finalValue <= 1.8){
            if (imageHeight < 300 || imageWidth < 300){
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
            } else {
                //change the flag that user is change the image
                isImageUpdate = true;

                //change the flag to true so we can update the image after user change the photo
                isUpdateImageCanceled = true;

                binding.iconEdit.setImageResource(R.drawable.icon_cancel);
                binding.textPhotoCover.setText(R.string.undo);
                binding.imageHeader.setImageURI(uri);
            }
        } else if (finalValue < 0.5){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooHeight),
                    Toast.LENGTH_SHORT).show();
        } else if (finalValue > 1.8){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooWidth),
                    Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case PLACE_PICKER_REQUEST:
                    Place place = PlacePicker.getPlace(mContext, data);
                    venueLatLon = place.getLatLng().toString().substring(10).replace(")", "");
                    String mapURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + venueLatLon + "&zoom=16&size=400x200";
                    binding.venueAddress.setText(place.getAddress().toString());
                    Glide.with(mContext.getApplicationContext()).load(mapURL).into(binding.venueStaticMap);
                    break;

                case REQUEST_IMAGE_CAPTURE:
                    imageHeaderFrom = "camera";
                    Bundle extras = data.getExtras();
                    photoBitmap = (Bitmap) extras.get("data");
                    binding.imageHeader.setImageBitmap(photoBitmap);

                    //change the flag that user is change the image
                    isImageUpdate = true;

                    //change the flag to true so we can update the image after user change the photo
                    isUpdateImageCanceled = true;

                    //overide onclick update image
                    binding.iconEdit.setImageResource(R.drawable.icon_cancel);
                    binding.textPhotoCover.setText(R.string.undo);

                    binding.iconEditHeaderPhoto.setOnClickListener(v -> {
                        if (isUpdateImageCanceled){
                            isImageUpdate = false;
                            binding.iconEdit.setImageResource(R.drawable.icon_edit);
                            binding.textPhotoCover.setText(R.string.changePhotoCover);
                            Glide.with(mContext.getApplicationContext()).load(venueHeaderPhoto).into(binding.imageHeader);
                            isUpdateImageCanceled = false;
                        } else {
                            openPopUpPhoto();
                        }
                    });
                    break;

                case SELECT_PICTURE:
                    imageHeaderFrom = "storage";
                    selectedImageUri = data.getData();
                    if (null != selectedImageUri) {

                        getDropboxIMGSize(selectedImageUri);

                        binding.iconEditHeaderPhoto.setOnClickListener(v -> {
                            if (isUpdateImageCanceled){
                                isImageUpdate = false;
                                isUpdateImageCanceled = false;
                                binding.textPhotoCover.setText(R.string.changePhotoCover);
                                binding.iconEdit.setImageResource(R.drawable.icon_edit);
                                Glide.with(mContext.getApplicationContext()).load(venueHeaderPhoto).into(binding.imageHeader);
                            } else {
                                openPopUpPhoto();
                            }
                        });
                    }
                    break;
            }
        }
    }

    @Override
    public void tagClick(String tag) {
        isTagChange = true;
        int index = listOfTag.indexOf(tag);

        if (listOfTag.size() == 1){
            listOfTag.remove(index);
            adapterTag.updateData(listOfTag);
        } else {
            listOfTag = adapterTag.removeTag(index);
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needBackPressedChecked;
    }

    @Override
    public void onBackPress() {
        checkBackPressed();
    }

    private void openUnsavedDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_unsaved_change, null);
        builder.setView(view);
        builder.setCancelable(false);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView save = view.findViewById(R.id.save);
        TextView dismiss = view.findViewById(R.id.dismiss);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        save.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            if (isImageUpdate){
                if (needUsernameChecked) {
                    if (!binding.inputVenueUsername.getText().toString().trim().equals(venueUsername)){
                        Toast.makeText(mContext, "Please check your venue username first", Toast.LENGTH_SHORT).show();
                        PublicMethod.focusToView(binding.txtCheckUsername);
                        return;
                    }
                }

                updateImage();
            } else {
                saveUpdateVenue(venueHeaderPhoto);
            }
        });

        dismiss.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            switch (destination){
                case "gallery":
                    bottomGallery();
                    break;

                case "event":
                    bottomEvent();
                    break;

                case "subVenue":
                    bottomSubVenue();
                    break;

                    default:
                        getActivity().onBackPressed();
                        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
                        break;
            }
        });
    }

    private void checkBackPressed() {
        if (isImageUpdate){
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateVenueName.getText().toString().equals(modelVenueSementara.getVenueName())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.venueCategories.getText().toString().equals(modelVenueSementara.getVenueCategories())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateDescription.getText().toString().equals(modelVenueSementara.getVenueLongDesc())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.venueAddress.getText().toString().equals(modelVenueSementara.getVenueAddress())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updatePhone.getText().toString().equals(modelVenueSementara.getVenuePhoneNumber())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateFacebook.getText().toString().equals(modelVenueSementara.getVenueFacebook())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateTwitter.getText().toString().equals(modelVenueSementara.getVenueTwitter())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateInstagram.getText().toString().equals(modelVenueSementara.getVenueInstagram())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateWhatsApp.getText().toString().equals(modelVenueSementara.getVenueWhatsapp())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateWebsite.getText().toString().equals(modelVenueSementara.getVenueWebsite())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateEmail.getText().toString().equals(modelVenueSementara.getVenueEmail())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.inputPeriodScheduleTime.getText().toString().equals(modelVenueSementara.getOverWriteTime())) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (isTagChange) {
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else if (!binding.inputVenueUsername.getText().toString().trim().equals(venueUsername)){
            needBackPressedChecked = false;
            openUnsavedDialog();
            return;
        } else {
            needBackPressedChecked = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
            return;
        }
    }

    @Override
    public void onTimeSet() {
        //do nothing
    }
}
