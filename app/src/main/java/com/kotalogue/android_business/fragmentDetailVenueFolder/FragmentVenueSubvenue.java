package com.kotalogue.android_business.fragmentDetailVenueFolder;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.fragment.FragmentAddVenue;
import com.kotalogue.android_business.fragmentDetailBrandFolder.FragmentBrandVenue;
import com.kotalogue.android_business.globalAdapter.AdapterListVenue;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelVenue;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentVenueSubVenueBinding;

import java.util.ArrayList;

public class FragmentVenueSubvenue extends Fragment implements AdapterListVenue.AdapterListVenueInterface, BackPressObserver {
    private FragmentVenueSubVenueBinding binding;

    //recyclerview
    private AdapterListVenue adapterListVenue;
    private ArrayList<ModelVenue> modelVenues = new ArrayList<>();
    private Context mContext;
    private boolean isMaster;
    private String venueID, venueLatLon, venueAddress, venueName, brandID;
    private boolean needBackPressChecked = true;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            venueID = bundle.getString("venueID");
            venueLatLon = bundle.getString("venueLatLon");
            venueAddress = bundle.getString("venueAddress");
            venueName = bundle.getString("venueName");
            brandID = bundle.getString("brandID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentVenueSubVenueBinding.inflate(LayoutInflater.from(mContext), null, false);

        binding.venueInformation.setSelected(true);

        PublicMethod.enableDefaultAnimation(binding.layoutMenu);
        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.recyclerVenue, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
            PublicMethod.setMargins(binding.layoutMenu, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogLoadingInit();

        recyclerInit();

        loadVenue();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void clickListener() {
        binding.iconAdd.setOnClickListener(v -> {
            FragmentAddVenue fragmentAddVenue = new FragmentAddVenue();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            bundle.putString("venueID", venueID);
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueName", venueName);
            bundle.putString("createSubvenue", "createSubvenue");
            bundle.putString("specificVenue", "specificVenue");
            fragmentAddVenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddVenue).addToBackStack("").commit();
        });

        binding.layoutBottomInformation.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomGallery.setOnClickListener(view -> {

            FragmentVenueGallery fragmentVenueGallery = new FragmentVenueGallery();
            Bundle bundle = new Bundle();
            bundle.putString("venueID", venueID);
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueName", venueName);
            bundle.putString("brandID", brandID);
            fragmentVenueGallery.setArguments(bundle);

            getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentVenueGallery).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomEvent.setOnClickListener(view -> {

            FragmentVenueEvent fragmentVenueEvent = new FragmentVenueEvent();
            Bundle bundle = new Bundle();
            bundle.putString("venueID", venueID);
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueName", venueName);
            bundle.putString("brandID", brandID);
            fragmentVenueEvent.setArguments(bundle);

            getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentVenueEvent).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomSubVenue.setOnClickListener(view -> { });

        binding.iconBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });
    }

    private void recyclerInit(){
        adapterListVenue = new AdapterListVenue(modelVenues, mContext, this, getActivity());

        binding.recyclerVenue.setHasFixedSize(true);
        binding.recyclerVenue.setLayoutManager(new LinearLayoutManager(mContext));
        binding.recyclerVenue.setAdapter(adapterListVenue);

        binding.recyclerVenue.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //dy > 0 means it will scrolled and when its get the last item will be notified
                //dy < 0 means Recycle view scrolling up...

                if (dy > 0) {
                    // Recycle view scrolling down...
                    /*if(recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN) == false){

                    }*/
                    binding.layoutBottomInformation.setVisibility(View.GONE);
                    binding.layoutBottomEvent.setVisibility(View.GONE);
                    binding.layoutBottomSubVenue.setVisibility(View.GONE);
                    binding.layoutBottomGallery.setVisibility(View.GONE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.dp(R.dimen.standart_20dp, mContext));
                } else if (dy < 0){
                    binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                    binding.layoutBottomEvent.setVisibility(View.VISIBLE);
                    binding.layoutBottomSubVenue.setVisibility(View.VISIBLE);
                    binding.layoutBottomGallery.setVisibility(View.VISIBLE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
                }
            }
        });
    }

    private void loadVenue(){
        FirebaseFirestore.getInstance().collection("Venue").document(venueID)
                .collection("CollectionSubVenue").addSnapshotListener((documentSnapshots, e) -> {

            modelVenues.clear();

            if (documentSnapshots != null && documentSnapshots.getDocuments().size() > 0){

                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Venue").document(document.getString("venueID"))
                            .get().addOnSuccessListener(documentSnapshot -> {

                        if (documentSnapshot != null && documentSnapshot.exists()){
                            hideLoading();
                            ModelVenue modelVenue = documentSnapshot.toObject(ModelVenue.class);
                            modelVenues.add(modelVenue);
                            adapterListVenue.updateData(modelVenues);
                        }
                    });
                }
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                } else {
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.noConnection), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void itemClick(ArrayList<ModelVenue> modelVenues, int position) {
        FragmentVenueInformation fragmentVenueInformation = new FragmentVenueInformation();
        Bundle bundle = new Bundle();
        bundle.putString("venueID", modelVenues.get(position).getVenueID());
        fragmentVenueInformation.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.containerStandartActivity, fragmentVenueInformation).addToBackStack("").commit();
    }

    @Override
    public void deleteItem(ArrayList<ModelVenue> modelVenues, int position) {
        showDeleteDialog(modelVenues, position);
    }

    private void deleteVenue(ArrayList<ModelVenue> modelVenues, int position) {
        //check if this venue is subvenue or no
        if (!modelVenues.get(position).getVenueParentID().equals("")){
            //delete this venue from collection subvenue from its parent
            FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueParentID()).collection("CollectionSubVenue")
                    .document(modelVenues.get(position).getVenueID()).delete();
        }

        //remove venue photo gallery
        FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                .collection("VenuePhotoGallery").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    //remove data inside the firestore collection
                    FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                            .collection("VenuePhotoGallery").document(document.getString("photoID")).delete();

                    //remove the data inside the cloud storage child
                    FirebaseStorage.getInstance().getReference("Venue").child(modelVenues.get(position).getVenueID())
                            .child(document.getString("photoID")).delete();
                }
            }
        });

        //remove the subvenue
        FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                .collection("CollectionSubVenue").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null){
                for (DocumentSnapshot document: documentSnapshots){
                    //menghapus id dari collection subvenue dari document venue
                    FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                            .collection("CollectionSubVenue").document(document.getString("venueID")).delete();

                    //menghapus data subvenue dari venue ini
                    FirebaseFirestore.getInstance().collection("Venue").document(document.getString("venueID")).delete();
                }
            }
        });

        FirebaseFirestore.getInstance().collection("Brands").document(modelVenues.get(position).getBrandID())
                .collection("CollectionVenue"+modelVenues.get(position).getBrandID()).document(modelVenues.get(position).getVenueID()).delete();

        //remove the entire data in venue
        FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                .delete().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                dialogLoading.dismiss();

                Toast.makeText(mContext, R.string.successDeleteVenue, Toast.LENGTH_SHORT).show();

                FragmentVenueSubvenue fragmentVenueSubvenue = new FragmentVenueSubvenue();
                Bundle bundle = new Bundle();
                bundle.putString("venueID", venueID);
                bundle.putString("venueLatLon", venueLatLon);
                bundle.putString("venueAddress", venueAddress);
                bundle.putString("venueName", venueName);
                bundle.putString("brandID", brandID);
                fragmentVenueSubvenue.setArguments(bundle);

                getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(R.id.containerStandartActivity, fragmentVenueSubvenue).commit();
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    Snackbar.make(getView(), task.getException().getMessage(), Snackbar.LENGTH_LONG).setAction(mContext.getResources().getString(R.string.retry), v -> deleteVenue(modelVenues, position)).show();
                }
            }
        });

    }

    private void dialogLoadingInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        builder.setView(view);
        builder.setCancelable(false);
        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void showDeleteDialog(ArrayList<ModelVenue> modelVenues, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_delete_dialog, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView delete = view.findViewById(R.id.delete);
        TextView cancel = view.findViewById(R.id.cancel);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //hancle click
        delete.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            showDialogLoading();

            deleteVenue(modelVenues, position);

            //this.modelVenues = adapterListVenue.removeData(modelVenues.indexOf(modelVenues.get(position)));
        });

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needBackPressChecked;
    }

    @Override
    public void onBackPress() {
        needBackPressChecked = false;
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
