package com.kotalogue.android_business.fragmentDetailVenueFolder;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.fragment.FragmentAddEvent;
import com.kotalogue.android_business.fragmentDetailEventFolder.FragmentEventInformation;
import com.kotalogue.android_business.globalAdapter.AdapterListEvent;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelEvent;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentVenueEventBinding;

import java.util.ArrayList;

public class FragmentVenueEvent extends Fragment implements AdapterListEvent.AdapterListEventInterface, BackPressObserver {
    private FragmentVenueEventBinding binding;

    //variabel for getting bundle data
    private String venueLatLon, venueAddress, venueName, brandID, venueID;

    //recyclerview
    private AdapterListEvent adapterListEvent;
    private ArrayList<ModelEvent> modelEvents = new ArrayList<>();
    private Context mContext;
    private boolean isMaster;
    private boolean needBackPressChecked = true;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            venueID = bundle.getString("venueID");
            venueLatLon = bundle.getString("venueLatLon");
            venueAddress = bundle.getString("venueAddress");
            venueName = bundle.getString("venueName");
            brandID = bundle.getString("brandID");
        }

        binding = FragmentVenueEventBinding.inflate(LayoutInflater.from(mContext), null, false);

        binding.venueInformation.setSelected(true);

        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutMenu);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.recyclerEvent, 0, 0,0,PublicMethod.getNavigationBarHeight(mContext));
            PublicMethod.setMargins(binding.layoutMenu, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogLoadingInit();

        recyclerInit();

        loadEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void clickListener() {
        binding.iconAdd.setOnClickListener(view -> {
            FragmentAddEvent fragmentAddEvent = new FragmentAddEvent();
            Bundle bundle = new Bundle();
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueID", venueID);
            bundle.putString("venueName", venueName);
            bundle.putString("brandID", brandID);
            bundle.putString("specificVenue", "specificVenue");
            fragmentAddEvent.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddEvent).addToBackStack("").commit();
        });

        binding.layoutBottomInformation.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomGallery.setOnClickListener(view -> {
            FragmentVenueGallery fragmentVenueGallery = new FragmentVenueGallery();
            Bundle bundle = new Bundle();
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueID", venueID);
            bundle.putString("venueName", venueName);
            bundle.putString("brandID", brandID);
            fragmentVenueGallery.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentVenueGallery).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomEvent.setOnClickListener(view -> { });

        binding.layoutBottomSubVenue.setOnClickListener(view -> {
            FragmentVenueSubvenue fragmentVenueSubvenue = new FragmentVenueSubvenue();
            Bundle bundle = new Bundle();
            bundle.putString("venueLatLon", venueLatLon);
            bundle.putString("venueAddress", venueAddress);
            bundle.putString("venueID", venueID);
            bundle.putString("venueName", venueName);
            bundle.putString("brandID", brandID);
            fragmentVenueSubvenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentVenueSubvenue).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.iconBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });
    }

    private void loadEvent(){
        FirebaseFirestore.getInstance().collection("Venue").document(venueID).collection("CollectionEvent")
                .orderBy("eventStartDate", Query.Direction.DESCENDING).addSnapshotListener((documentSnapshots, e) -> {

            if (documentSnapshots != null && documentSnapshots.size() > 0){
                modelEvents.clear();

                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .get().addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot != null && documentSnapshot.exists()){
                            ModelEvent modelEvent = documentSnapshot.toObject(ModelEvent.class);
                            modelEvents.add(modelEvent);
                            adapterListEvent.updateData(modelEvents);
                        }
                    });
                }

                if (modelEvents.size() > 0) {
                    hideLoading();
                    binding.emptyData.setVisibility(View.GONE);
                } else {
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                }
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                } else {
                    hideLoading();
                    binding.emptyData.setVisibility(View.GONE);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void recyclerInit(){
        //adapterListEvent = new AdapterListEvent(null, mContext, getView(), getActivity(), "", "");
        adapterListEvent = new AdapterListEvent(modelEvents, mContext, this, getActivity());

        binding.recyclerEvent.setHasFixedSize(true);
        binding.recyclerEvent.setLayoutManager(new LinearLayoutManager(mContext));
        binding.recyclerEvent.setAdapter(adapterListEvent);

        binding.recyclerEvent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //dy > 0 means it will scrolled and when its get the last item will be notified
                //dy < 0 means Recycle view scrolling up...

                if (dy > 0) {
                    // Recycle view scrolling down...
                    /*if(recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN) == false){

                    }*/
                    binding.layoutBottomInformation.setVisibility(View.GONE);
                    binding.layoutBottomEvent.setVisibility(View.GONE);
                    binding.layoutBottomSubVenue.setVisibility(View.GONE);
                    binding.layoutBottomGallery.setVisibility(View.GONE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.dp(R.dimen.standart_20dp, mContext));
                } else if (dy < 0){
                    binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                    binding.layoutBottomEvent.setVisibility(View.VISIBLE);
                    binding.layoutBottomSubVenue.setVisibility(View.VISIBLE);
                    binding.layoutBottomGallery.setVisibility(View.VISIBLE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
                }
            }
        });
    }

    @Override
    public void itemClick(ArrayList<ModelEvent> modelEvents, int position) {
        FragmentEventInformation fragmentEventInformation = new FragmentEventInformation();
        Bundle bundle = new Bundle();
        bundle.putString("eventID", modelEvents.get(position).getEventID());
        fragmentEventInformation.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.containerStandartActivity, fragmentEventInformation).addToBackStack("").commit();
    }

    @Override
    public void deleteItem(ArrayList<ModelEvent> modelEvents, int position) {
        showDeleteDialog(modelEvents, position);
    }

    private void showDeleteDialog(ArrayList<ModelEvent> modelEvents, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_delete_dialog, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView delete = view.findViewById(R.id.delete);
        TextView cancel = view.findViewById(R.id.cancel);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //hancle click
        delete.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            showDialogLoading();

            deleteEvent(modelEvents, position);

            //this.modelEvents = adapterListEvent.removeData(modelEvents.indexOf(modelEvent));
        });

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
    }

    private void dialogLoadingInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        builder.setView(view);
        builder.setCancelable(false);
        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void deleteEvent(ArrayList<ModelEvent> modelEvents, int position) {
        //check if this event is subevent or no
        if (!modelEvents.get(position).getEventParentID().equals("")){
            //delete this event from collection sub event from its event parent
            FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventParentID())
                    .collection("CollectionSubEvent").document(modelEvents.get(position).getEventID()).delete();
        }

        //remove event photo gallery
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("EventPhotoGallery").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("EventPhotoGallery").document(document.getString("photoID"))
                            .delete();

                    FirebaseStorage.getInstance().getReference("Event").child(modelEvents.get(position).getEventID())
                            .child(document.getString("photoID")).delete();
                }
            }
        });

        //remove event subevent
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("CollectionSubEvent").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("CollectionSubEvent").document(document.getString("eventID")).delete();

                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID")).delete();
                }
            }
        });

        //remove eventlike
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("CollectionLike").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("CollectionLike").document(document.getString("userID")).delete();
                }
            }
        });

        //remove eventdislike
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("CollectionDislike").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("CollectionDislike").document(document.getString("userID")).delete();
                }
            }
        });

        FirebaseFirestore.getInstance().collection("Brands").document(modelEvents.get(position).getBrandID())
                .collection("CollectionEvent"+modelEvents.get(position).getBrandID()).document(modelEvents.get(position).getEventID())
                .delete();

        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .delete().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                dialogLoading.dismiss();

                Toast.makeText(mContext, R.string.successDeleteEvent, Toast.LENGTH_SHORT).show();

                FragmentVenueEvent fragmentVenueEvent = new FragmentVenueEvent();
                Bundle bundle = new Bundle();
                bundle.putString("venueID", venueID);
                bundle.putString("venueLatLon", venueLatLon);
                bundle.putString("venueAddress", venueAddress);
                bundle.putString("venueName", venueName);
                bundle.putString("brandID", brandID);
                fragmentVenueEvent.setArguments(bundle);

                getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(R.id.containerStandartActivity, fragmentVenueEvent).addToBackStack("").commit();
            }
        });
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needBackPressChecked;
    }

    @Override
    public void onBackPress() {
        needBackPressChecked = false;
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
