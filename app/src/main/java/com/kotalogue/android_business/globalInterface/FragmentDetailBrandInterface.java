package com.kotalogue.android_business.globalInterface;

/**
 * Created by ariefzainuri on 14/04/18.
 */

public interface FragmentDetailBrandInterface {
    void doUpdateFacebook();
    void doUpdateTwitter();
    void doUpdateInstagram();
    void doUpdateWhatsApp();
    void doUpdateWebsite();
    void doUpdateEmail();
    void addEvent();
    void showSocialMedia();
    void iconBack();
    void allertClick();
    void addVenue();
    void updateBrandName();
    void updateBrandDescription();
    void editHeaderPhoto();
    void updateHeaderPhoto();
    void updateTag();
    void openCloseBrandTag();
    void addNewTag();
    void updatePhoneNumber();
    void saveMultipleSocialMedia();
    void saveMultipleBrandInformationField();
    void publishBrand();
    void sendCode();
}
