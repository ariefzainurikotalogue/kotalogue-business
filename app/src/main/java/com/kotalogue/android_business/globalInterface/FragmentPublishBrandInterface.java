package com.kotalogue.android_business.globalInterface;

public interface FragmentPublishBrandInterface {
    void back();
    void sendEmail();
}
