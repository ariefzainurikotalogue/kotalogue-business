package com.kotalogue.android_business.globalInterface;

/**
 * Created by ariefzainuri on 15/04/18.
 */

public interface FragmentAddEventInterface {
    void back();
    void startDate();
    void finishDate();
    void timeStart();
    void timeFinish();
    void saveEvent();
    void addGallery();
    void addHeaderPhoto();
    void showHideSocialMedia();
    void categories();
    void addTag();
}
