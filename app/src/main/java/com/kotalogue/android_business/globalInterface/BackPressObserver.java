package com.kotalogue.android_business.globalInterface;

public interface BackPressObserver {

    boolean isReadyToInterceptBackPress();
    void onBackPress();

}
