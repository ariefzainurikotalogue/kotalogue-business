package com.kotalogue.android_business.globalInterface;

/**
 * Created by ariefzainuri on 13/04/18.
 */

public interface LoginInterface {
    void login();
    void register();
    void loginRegister();
    void googleSignIn();
    void loginClick();
    void forgetPassword();
}
