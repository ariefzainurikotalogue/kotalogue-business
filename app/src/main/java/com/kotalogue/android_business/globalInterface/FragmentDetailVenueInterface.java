package com.kotalogue.android_business.globalInterface;

/**
 * Created by ariefzainuri on 14/04/18.
 */

public interface FragmentDetailVenueInterface {
    void backClick();
    void updateDescription();
    void openPlacePicker();
    void updateAddress();
    void updateContact();
    void updateFacebook();
    void updateEmail();
    void updateTwitter();
    void updateWhatsapp();
    void updateInstagram();
    void updateWebsite();
    void addEvent();
    void editImageHeader();
    void updateImageHeader();
    void updateVenueName();
    void editVenueCategories();
    void updateVenueCategories();
    void editVenueGallery();
    void updateVenueGallery();
    void addSubvenue();
    void openCloseTag();
    void addTag();
    void updateTag();
    void updateMultipleField();
    void updateMultipleSocialMedia();
}
