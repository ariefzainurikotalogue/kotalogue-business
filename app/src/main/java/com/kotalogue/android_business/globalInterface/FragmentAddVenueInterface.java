package com.kotalogue.android_business.globalInterface;

/**
 * Created by ariefzainuri on 14/04/18.
 */

public interface FragmentAddVenueInterface {
    void iconBack();
    void simpanVenue();
    void openPlacePicker();
    void addImage();
    void checkUsername();
    void chooseCategories();
    void addSocialMedia();
    void addGallery();
    void addTag();
}
