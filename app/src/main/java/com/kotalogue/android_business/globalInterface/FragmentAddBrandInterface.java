package com.kotalogue.android_business.globalInterface;

/**
 * Created by ariefzainuri on 13/04/18.
 */

public interface FragmentAddBrandInterface {
    void back();
    void saveBrand();
    void openCloseSocialMedia();
    void addTag();
    void addImage();
    void chooseCategories();
}
