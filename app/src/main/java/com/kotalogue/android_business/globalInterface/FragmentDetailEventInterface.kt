package com.kotalogue.android_business.globalInterface

/**
 * Created by ariefzainuri on 16/04/18.
 */
interface FragmentDetailEventInterface {
    fun backClick()
    fun updateDescription()
    fun updateName()
    fun updatePrice()
    fun updateTime()
    fun updateStartDate()
    fun updateFinishDate()
    fun editGallery()
    fun updateGallery()
    fun updateFacebook()
    fun updateTwitter()
    fun updateInstagram()
    fun updateWhatsapp()
    fun updateWebsite()
    fun updateEmail()
    fun timeStart()
    fun timeFinish()
    fun startDate()
    fun finishDate()
    fun editHeaderPhoto()
    fun updateHeaderPhoto()
    fun updateEventStatus()
    fun addSubevent()
    fun addTag()
    fun updateTag()
    fun openCloseTag()
    fun updateMultipleField()
    fun updateMultipleSocMedia()
}