package com.kotalogue.android_business.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.globalInterface.LoginInterface;
import com.kotalogue.android_business.model.ModelUser;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.ActivityLoginBinding;

import static android.content.ContentValues.TAG;

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;
    private ActivityLoginBinding binding;
    private boolean isAlreadyLoginClicked = false;
    private boolean isSignInClick = false;

    //view
    RelativeLayout layoutLogin;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        askForPermission(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, 3, getApplicationContext(), this);

        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_login);
        binding.setClick(loginInterface);

        layoutLogin = binding.layoutLogin;

        dialogLoadingInit();

        setUpGoogleSignIn();

        checkUserState();
    }

    private void dialogLoadingInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.setCancelable(false);
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
    }

    private void showLoadingDialog(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void cancelLoadingDialog(){
        dialogLoading.cancel();
    }

    private void dialogLoginOrRegister() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_login_register, null);
        builder.setView(view);
        builder.setCancelable(false);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView signIn = view.findViewById(R.id.signIn);
        TextView signUp = view.findViewById(R.id.signUp);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, this), ViewGroup.LayoutParams.WRAP_CONTENT);
        alertDialog.setCancelable(false);

        signIn.setOnClickListener(view1 -> {
            isAlreadyLoginClicked = true;
            isSignInClick = true;
            binding.textSignIn.setText(R.string.signIn);
            binding.textSignUpTop.setText(R.string.signInToContinue);
            binding.btnRegister.setVisibility(View.GONE);
            binding.btnLogin.setVisibility(View.VISIBLE);
            binding.txtLoginRegister.setText(getString(R.string.dontHaveAccount));

            alertDialog.cancel();
        });

        signUp.setOnClickListener(view1 -> alertDialog.cancel());
    }

    public static void askForPermission(String permission, String permission2, String permission3, String permission4, Integer requestCode, Context context, Activity activity) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                ActivityCompat.requestPermissions(activity, new String[]{permission, permission2, permission3, permission4}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission, permission2, permission3, permission4}, requestCode);
            }
        } else {
            Log.d("Permissions", permission + " is granted");
        }
    }

    private void checkUserState() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            boolean isMaster = getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

            if (isMaster){
                Intent intent = new Intent(LoginActivity.this, StandartActivity.class);
                intent.putExtra("open", "OpenFragmentDaftarUser");
                intent.putExtra("userID", FirebaseAuth.getInstance().getCurrentUser().getUid());
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(LoginActivity.this, StandartActivity.class);
                intent.putExtra("open", "OpenFragmentHome");
                intent.putExtra("userID", FirebaseAuth.getInstance().getCurrentUser().getUid());
                startActivity(intent);
                finish();
            }
        } else {
            dialogLoginOrRegister();
        }
    }

    private void setUpGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(LoginActivity.this, gso);
    }

    private LoginInterface loginInterface = new LoginInterface() {
        @Override
        public void login() {
            doLogin();
        }

        @Override
        public void register() {
            doRegister();
        }

        @Override
        public void loginRegister() {
            if (isAlreadyLoginClicked){
                binding.textSignIn.setText(R.string.signUp);
                binding.textSignUpTop.setText(R.string.signUpToContinue);
                binding.txtLoginRegister.setText(getResources().getString(R.string.alreadyHaveAccount));
                binding.btnRegister.setVisibility(View.VISIBLE);
                binding.btnLogin.setVisibility(View.GONE);
                isAlreadyLoginClicked = false;
            } else {
                binding.textSignIn.setText(R.string.signIn);
                binding.textSignUpTop.setText(R.string.signInToContinue);
                binding.txtLoginRegister.setText(getResources().getString(R.string.dontHaveAccount));
                binding.btnRegister.setVisibility(View.GONE);
                binding.btnLogin.setVisibility(View.VISIBLE);
                isAlreadyLoginClicked = true;
            }
        }

        @Override
        public void googleSignIn() {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }

        @Override
        public void loginClick() {
            if (isSignInClick) {
                isSignInClick = false;
                binding.textSignIn.setText(R.string.signUp);
                binding.textSignUpTop.setText(R.string.signUpToContinue);
                binding.btnRegister.setVisibility(View.VISIBLE);
                binding.btnLogin.setVisibility(View.GONE);
                binding.txtLoginRegister.setText(getString(R.string.alreadyHaveAccount));
            } else {
                isSignInClick = true;
                binding.textSignIn.setText(R.string.signIn);
                binding.textSignUpTop.setText(R.string.signInToContinue);
                binding.btnRegister.setVisibility(View.GONE);
                binding.btnLogin.setVisibility(View.VISIBLE);
                binding.txtLoginRegister.setText(getString(R.string.dontHaveAccount));
            }
        }

        @Override
        public void forgetPassword() {
            openDialogForgetPassword();
        }
    };

    private void openDialogForgetPassword() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.layout_forget_password, null);
        builder.setView(view);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        //init view
        TextView btnResetPassword = view.findViewById(R.id.btnResetPassword);
        RelativeLayout btnCancel = view.findViewById(R.id.btnCancel);
        TextInputEditText inputEmail = view.findViewById(R.id.inputEmail);

        //click listener
        btnResetPassword.setOnClickListener(v -> resetPassword(inputEmail));
        btnCancel.setOnClickListener(v -> alertDialog.dismiss());
    }

    private void resetPassword(TextInputEditText inputEmail) {
        FirebaseAuth.getInstance().sendPasswordResetEmail(inputEmail.getText().toString())
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()){
                    Toast.makeText(this, getString(R.string.succesSendResetLink), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
    }

    private void doRegister() {
        String email = binding.inputEmail.getText().toString();
        String password = binding.inputPassword.getText().toString();

        if (email.isEmpty() && password.isEmpty()){
            Toast.makeText(LoginActivity.this, "anda belum mengisi email & password", Toast.LENGTH_SHORT).show();
            return;
        } else if (email.isEmpty()){
            Toast.makeText(LoginActivity.this, "anda belum mengisi email", Toast.LENGTH_SHORT).show();
            return;
        } else if (password.isEmpty()){
            Toast.makeText(LoginActivity.this, "anda belum mengisi password", Toast.LENGTH_SHORT).show();
            return;
        }

        showLoadingDialog();

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){

                cancelLoadingDialog();

                Toast.makeText(LoginActivity.this, "Succesfully create an account", Toast.LENGTH_SHORT).show();

                FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification().addOnCompleteListener(task1 -> {
                    if (task1.isComplete()){
                        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        ModelUser modelUser = new ModelUser(userID, "Business", FirebaseAuth.getInstance().getCurrentUser().getEmail());
                        FirebaseFirestore.getInstance().collection("User").document(userID)
                                .set(modelUser).addOnCompleteListener(task11 -> {
                                    if (task11.isComplete()){
                                        Log.d("SUCCES_CREATE", "SUCCESS");
                                    }
                                });

                        Toast.makeText(LoginActivity.this, "We have sent you a verification email, please check your email", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                cancelLoadingDialog();

                if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                    Toast.makeText(LoginActivity.this, "User with this email already exist.", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(LoginActivity.this, "Make sure you have connected to internet", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void doLogin(){
        String email = binding.inputEmail.getText().toString();
        String password = binding.inputPassword.getText().toString();

        if (email.isEmpty() && password.isEmpty()){
            Toast.makeText(LoginActivity.this, "anda belum mengisi email & password", Toast.LENGTH_SHORT).show();
            return;
        } else if (email.isEmpty()){
            Toast.makeText(LoginActivity.this, "anda belum mengisi email", Toast.LENGTH_SHORT).show();
            return;
        } else if (password.isEmpty()){
            Toast.makeText(LoginActivity.this, "anda belum mengisi password", Toast.LENGTH_SHORT).show();
            return;
        }

        showLoadingDialog();

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){

                cancelLoadingDialog();

                if (FirebaseAuth.getInstance().getCurrentUser().getEmail().equals("ariefzainuri@kotalogue.com")
                        || FirebaseAuth.getInstance().getCurrentUser().getEmail().equals("prayudi@kotalogue.com")){

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Kotalogue Master");
                    builder.setMessage("Anda terdaftar sebagai Kotalogue Master, tekan lanjut untuk melihat data member.");
                    builder.setPositiveButton("Lanjut", (dialogInterface, i) -> {
                        SharedPreferences masterPref = getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE);
                        masterPref.edit().putBoolean(StaticVariable.IS_MASTER, true).apply();
                        Intent intent = new Intent(LoginActivity.this, StandartActivity.class);
                        intent.putExtra("open", "OpenFragmentDaftarUser");
                        startActivity(intent);
                        finish();

                        dialogInterface.dismiss();
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                } else {
                    SharedPreferences masterPref = getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE);
                    masterPref.edit().putBoolean(StaticVariable.IS_MASTER, false).apply();

                    Intent intent = new Intent(LoginActivity.this, StandartActivity.class);
                    intent.putExtra("open", "OpenFragmentHome");
                    startActivity(intent);
                    finish();
                }
            } else {
                cancelLoadingDialog();

                Toast.makeText(LoginActivity.this, "Failed to login, Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        showLoadingDialog();

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(LoginActivity.this, task -> {
                    if (task.isSuccessful()) {
                        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        ModelUser modelUser = new ModelUser(userID, "Business", FirebaseAuth.getInstance().getCurrentUser().getEmail());

                        if (FirebaseAuth.getInstance().getCurrentUser().getEmail().equals("ariefzainuri@kotalogue.com")
                                || FirebaseAuth.getInstance().getCurrentUser().getEmail().equals("prayudi@kotalogue.com")){

                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle("Kotalogue Master");
                            builder.setMessage("Anda terdaftar sebagai Kotalogue Master, tekan lanjut untuk melihat data member.");

                            builder.setPositiveButton("Lanjut", (dialogInterface, i) -> {
                                SharedPreferences masterPref = getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE);
                                masterPref.edit().putBoolean(StaticVariable.IS_MASTER, true).apply();
                                Intent intent = new Intent(LoginActivity.this, StandartActivity.class);
                                intent.putExtra("open", "OpenFragmentDaftarUser");
                                startActivity(intent);
                                finish();

                                dialogInterface.dismiss();
                            });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();

                        } else {
                            FirebaseFirestore.getInstance().collection("User").document(userID)
                                    .set(modelUser).addOnCompleteListener(task1 -> {
                                if (task1.isComplete()){
                                    SharedPreferences masterPref = getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE);
                                    masterPref.edit().putBoolean(StaticVariable.IS_MASTER, false).apply();
                                    cancelLoadingDialog();
                                    Toast.makeText(LoginActivity.this, "Welcome to Kotalogue", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(LoginActivity.this, StandartActivity.class);
                                    intent.putExtra("open", "OpenFragmentHome");
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        }
                    } else {
                        cancelLoadingDialog();

                        Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                });
    }
}
