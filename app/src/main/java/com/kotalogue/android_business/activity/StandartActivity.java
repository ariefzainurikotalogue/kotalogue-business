package com.kotalogue.android_business.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kotalogue.android_business.classHelper.Constants;
import com.kotalogue.android_business.fragment.FragmentHome;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.R;

import java.util.Locale;

public class StandartActivity extends AppCompatActivity {
    private SharedPreferences localizationPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);

        localizationPreference = getSharedPreferences(Constants.USER_LOCALIZATION, Context.MODE_PRIVATE);

        setLanguange();

        //default fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, new FragmentHome()).commit();
    }

    private void setLanguange() {
        switch (localizationPreference.getString(Constants.LANGUANGE, "")){
            case "in":
                Locale locale = new Locale("in");
                Locale.setDefault(locale);
                Configuration config = getBaseContext().getResources().getConfiguration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                break;

            case "en":
                Locale localeEn = new Locale("en");
                Locale.setDefault(localeEn);
                Configuration configEn = getBaseContext().getResources().getConfiguration();
                configEn.locale = localeEn;
                getBaseContext().getResources().updateConfiguration(configEn, getBaseContext().getResources().getDisplayMetrics());
                break;

            default:
                Locale localeDef = new Locale("en");
                Locale.setDefault(localeDef);
                Configuration configDef = getBaseContext().getResources().getConfiguration();
                configDef.locale = localeDef;
                getBaseContext().getResources().updateConfiguration(configDef, getBaseContext().getResources().getDisplayMetrics());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof BackPressObserver) {
                BackPressObserver observer = (BackPressObserver) fragment;
                if (observer.isReadyToInterceptBackPress()) {
                    observer.onBackPress();
                    return;
                }
            }
        }

        super.onBackPressed();
    }
}
