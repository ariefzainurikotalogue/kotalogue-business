package com.kotalogue.android_business.fragmentDetailBrandFolder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.databinding.FragmentBrandInformationBinding;
import com.kotalogue.android_business.fragment.FragmentAddVenue;
import com.kotalogue.android_business.fragment.FragmentPickEvent;
import com.kotalogue.android_business.fragment.FragmentPickVenue;
import com.kotalogue.android_business.fragment.FragmentPublishBrand;
import com.kotalogue.android_business.globalAdapter.AdapterTag;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelBrand;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static com.kotalogue.android_business.classHelper.PublicMethod.checkSocMedia;

@SuppressLint("ValidFragment")
public class FragmentBrandInformation extends Fragment implements AdapterTag.InterfaceTag, BackPressObserver {
    private FragmentBrandInformationBinding binding;

    //code
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int SELECT_PICTURE = 2;

    //recyclerview
    private AdapterTag adapterTag;
    private Context mContext;
    private ArrayList<String> listOfTag = new ArrayList<>();

    //String
    private String brandID;
    private String brandUserName;
    private String brandCategories;
    private String brandHeaderPhoto;
    private String brandFacebook;
    private String brandTwitter;
    private String brandInstagram;
    private String brandWhatsapp;
    private String brandWebsite;
    private String brandEmail;
    private String imageHeaderFrom = "";
    private String mCurrentPhotoPath;

    //boolean
    private boolean publicBrand, isBrandManagedByBusiness;
    private boolean tagChanged = false;
    private boolean isImageUpdate = false;
    private boolean isSocialMediaOpen = false;
    private boolean isUpdateImageCanceled = true;
    private boolean needToBackPressedHandled = true;
    private boolean isMaster;

    private ModelBrand modelBeforeUpdate;
    private AlertDialog alertDialog;
    private RelativeLayout layoutLoading;
    private Bitmap photoBitmap;
    private Uri selectedImageUri;
    private String destination = "";

    public FragmentBrandInformation() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentBrandInformationBinding.inflate(LayoutInflater.from(mContext), null, false);

        preloadView();

        loadDetailBrand();

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return binding.getRoot();
    }

    private void preloadView() {
        //set animation for viewgroup
        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutMenu);
        PublicMethod.enableDefaultAnimation(binding.rootLayoutSocialMedia);
        PublicMethod.enableDefaultAnimation(binding.nestedScrollView);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        binding.recyclerTag.setNestedScrollingEnabled(false);

        //make textview focus so it will be a moving text
        binding.brandEvent.setSelected(true);
        binding.brandInformation.setSelected(true);
        binding.brandVenue.setSelected(true);

        //set the custom height of toolbar
        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        //check if device has active navbar or no
        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);

            PublicMethod.setMargins(binding.layoutMenu, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        //preload view
        dialogInit();

        recyclerInit();

        checkMaster();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void openUnsavedDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_unsaved_change, null);
        builder.setView(view);
        builder.setCancelable(false);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView save = view.findViewById(R.id.save);
        TextView dismiss = view.findViewById(R.id.dismiss);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        save.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            if (isImageUpdate){
                updateImage();
            } else {
                showLoadingDialog();

                saveChange(brandHeaderPhoto);
            }
        });

        dismiss.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            switch (destination){
                case "venue":
                    bottomVenue();
                    break;

                case "event":
                    bottomEvent();
                    break;

                    default:
                        getActivity().onBackPressed();
                        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
                        break;
            }
        });
    }

    private void checkBeforeBackPressed() {
        if (isImageUpdate){
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateBrandName.getText().toString().equals(modelBeforeUpdate.getBrandName())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateBrandDescription.getText().toString().equals(modelBeforeUpdate.getBrandLongDesc())){
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.editBrandPhoneNumber.getText().toString().equals(modelBeforeUpdate.getBrandPhoneNumber())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateFacebook.getText().toString().equals(modelBeforeUpdate.getBrandFacebook())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateTwitter.getText().toString().equals(modelBeforeUpdate.getBrandTwitter())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateInstagram.getText().toString().equals(modelBeforeUpdate.getBrandInstagram())){
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateWhatsApp.getText().toString().equals(modelBeforeUpdate.getBrandWhatsapp())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateWebsite.getText().toString().equals(modelBeforeUpdate.getBrandWebsite())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateEmail.getText().toString().equals(modelBeforeUpdate.getBrandEmail())) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else if (tagChanged) {
            needToBackPressedHandled = false;
            openUnsavedDialog();
            return;
        } else {
            needToBackPressedHandled = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
        nestedScrollListener();
    }

    private void nestedScrollListener() {
        binding.nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (view, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY == (view.getChildAt(0).getMeasuredHeight() - view.getMeasuredHeight())){
                Log.d(TAG, "BOTTOM SCROLL");
            }

            if (scrollY > oldScrollY){
                Log.d(TAG, "SCROLL DOWN");
                binding.appBarLayout.setExpanded(false, true);
                binding.layoutBottomInformation.setVisibility(View.GONE);
                binding.layoutBottomEvent.setVisibility(View.GONE);
                binding.layoutBottomVenue.setVisibility(View.GONE);

                PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                        PublicMethod.dp(R.dimen.standart_20dp, mContext));
            }

            if (scrollY < oldScrollY){
                Log.d(TAG, "SCROLL UP");
                binding.appBarLayout.setExpanded(true, true);
                binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                binding.layoutBottomEvent.setVisibility(View.VISIBLE);
                binding.layoutBottomVenue.setVisibility(View.VISIBLE);

                PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                        PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
            }

            if (scrollY == 0){
                Log.d(TAG, "TOP SCROLL");
            }
        });
    }

    private void checkMaster() {
        boolean isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        if (isMaster){
            binding.btnSaveChange.setVisibility(View.GONE);
            binding.btnSendCode.setVisibility(View.GONE);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mContext, "com.kotalogue.android_business", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void updateImage() {
        //memasukan gambar ke firebase storage
        showLoadingDialog();

        switch (imageHeaderFrom){
            case "camera":
                StorageReference storageReference = FirebaseStorage.getInstance().getReference("Brand").child(brandID);
                UploadTask uploadGambarBarang = storageReference.putFile(Uri.fromFile(new File(mCurrentPhotoPath)));
                uploadGambarBarang.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        saveChange(downloadUri.toString());
                    } else {
                        Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

            case "storage":
                StorageReference storageReferenceBrand = FirebaseStorage.getInstance().getReference("Brand").child(brandID);
                UploadTask uploadTask = storageReferenceBrand.putFile(selectedImageUri);

                uploadTask.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReferenceBrand.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        saveChange(downloadUri.toString());

                    } else {
                        Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

                default:
                    binding.imageHeader.setDrawingCacheEnabled(true);
                    binding.imageHeader.buildDrawingCache();
                    Bitmap bitmap2 = binding.imageHeader.getDrawingCache();
                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
                    byte[] data2 = baos2.toByteArray();

                    StorageReference storageReference2 = FirebaseStorage.getInstance().getReference("Brand").child(brandID);
                    UploadTask uploadGambarBarang2 = storageReference2.putBytes(data2);
                    uploadGambarBarang2.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return storageReference2.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();

                            saveChange(downloadUri.toString());

                        } else {
                            Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
                    break;
        }
    }

    private void openPopUpPhoto() {
        PopupMenu popupMenu = new PopupMenu(mContext, binding.iconEditHeaderPhoto);
        popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.camera:
                    dispatchTakePictureIntent();
                    break;

                case R.id.storage:
                    Intent intentStorage = new Intent();
                    intentStorage.setType("image/*");
                    intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE);
                    break;
            }
            return false;
        });

        popupMenu.show();
    }

    private void loadDetailBrand() {
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).addSnapshotListener((documentSnapshot, e) -> {
            if (documentSnapshot != null && documentSnapshot.exists()){

                modelBeforeUpdate = documentSnapshot.toObject(ModelBrand.class);

                //set binding data for the view
                binding.setBrand(modelBeforeUpdate);

                new Handler().postDelayed(() -> {
                    hideLoading();
                    binding.layoutContentMain.setVisibility(View.VISIBLE);
                }, 500);

                //set global variabel after load data
                setGlobalVariabel(modelBeforeUpdate);

                //check if brands is published or no
                if (isBrandManagedByBusiness){
                    binding.layoutNotPublishedYet.setVisibility(View.GONE);
                    binding.layoutInputCode.setVisibility(View.GONE);
                } else {
                    binding.layoutNotPublishedYet.setVisibility(View.VISIBLE);
                    binding.layoutInputCode.setVisibility(View.VISIBLE);
                }

                //populate tag
                if (modelBeforeUpdate.getTag() != null){
                    adapterTag.updateData(modelBeforeUpdate.getTag());

                    if (modelBeforeUpdate.getTag().size() > 0){
                        listOfTag.clear();
                        listOfTag = modelBeforeUpdate.getTag();
                    }
                }

                Glide.with(mContext.getApplicationContext()).load(brandHeaderPhoto).placeholder(R.drawable.thumbnail).into(binding.imageHeader);

                checkSocMedia(brandFacebook, brandTwitter, brandInstagram, brandWhatsapp, brandWebsite, brandEmail,
                        binding.updateFacebook, binding.updateTwitter, binding.updateInstagram, binding.updateWhatsApp,
                        binding.updateWebsite, binding.updateEmail);
            } else {
                if (!PublicMethod.isDeviceOnline(mContext)){
                    Toast.makeText(mContext.getApplicationContext(), R.string.makeSureConnected, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void setGlobalVariabel(ModelBrand modelBeforeUpdate) {
        publicBrand = modelBeforeUpdate.isPublicBrand();
        isBrandManagedByBusiness = modelBeforeUpdate.isBrandIsManagedByBusiness();
        brandFacebook = modelBeforeUpdate.getBrandFacebook();
        brandTwitter = modelBeforeUpdate.getBrandTwitter();
        brandInstagram = modelBeforeUpdate.getBrandInstagram();
        brandWhatsapp = modelBeforeUpdate.getBrandWhatsapp();
        brandWebsite = modelBeforeUpdate.getBrandWebsite();
        brandEmail = modelBeforeUpdate.getBrandEmail();
        brandUserName = modelBeforeUpdate.getBrandUsername();
        brandCategories = modelBeforeUpdate.getBrandCategories();
        brandHeaderPhoto = modelBeforeUpdate.getBrandHeaderPhoto();
    }

    private void recyclerInit() {
        adapterTag = new AdapterTag(listOfTag, this);

        binding.recyclerTag.setHasFixedSize(true);
        binding.recyclerTag.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL));
        binding.recyclerTag.setAdapter(adapterTag);
    }

    private void showDialogBrand(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_create_event_venue, null);
        builder.setView(view);

        AlertDialog alertDialog = builder.create();

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView createEvent = view.findViewById(R.id.createEvent);
        TextView createVenue = view.findViewById(R.id.createVenue);

        createEvent.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            createEvent();
        });

        createVenue.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            createVenue();
        });

        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void createVenue() {
        FragmentAddVenue fragmentAddVenue = new FragmentAddVenue();
        Bundle bundle = new Bundle();
        bundle.putString("brandID", brandID);
        bundle.putString("checkFirst", "checkFirst");
        fragmentAddVenue.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddVenue).addToBackStack("").commit();
    }

    private void createEvent() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_pick_event, null);
        builder.setView(view);

        //setclick listener
        TextView createSubEvent = view.findViewById(R.id.createSubEvent);
        TextView title = view.findViewById(R.id.title);
        TextView pickVenue = view.findViewById(R.id.pickVenue);
        LinearLayout rootLayout = view.findViewById(R.id.rootLayoutDialog);
        title.setSelected(true);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(rootLayout) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        createSubEvent.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            FragmentPickEvent fragmentPickEvent = new FragmentPickEvent();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            fragmentPickEvent.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickEvent).addToBackStack("").commit();
        });

        pickVenue.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            FragmentPickVenue fragmentPickVenue = new FragmentPickVenue();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            bundle.putString("openFor", "Event");
            fragmentPickVenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickVenue).addToBackStack("").commit();
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> getActivity().onBackPressed());

        binding.iconAdd.setOnClickListener(v -> showDialogBrand());

        binding.toogleSocialMedia.setOnClickListener(v -> {
            if (isSocialMediaOpen){
                binding.layoutSocialMedia.setVisibility(View.GONE);
                binding.toogleSocialMedia.setText(R.string.show);
                isSocialMediaOpen = false;
            } else {
                binding.layoutSocialMedia.setVisibility(View.VISIBLE);
                binding.toogleSocialMedia.setText(R.string.hide);
                isSocialMediaOpen = true;
            }
        });

        binding.layoutBottomInformation.setOnClickListener(view -> {  });

        binding.layoutBottomEvent.setOnClickListener(view -> {
            destination = "event";

            if (isImageUpdate){
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateBrandName.getText().toString().equals(modelBeforeUpdate.getBrandName())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateBrandDescription.getText().toString().equals(modelBeforeUpdate.getBrandLongDesc())){
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.editBrandPhoneNumber.getText().toString().equals(modelBeforeUpdate.getBrandPhoneNumber())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelBeforeUpdate.getBrandFacebook())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelBeforeUpdate.getBrandTwitter())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelBeforeUpdate.getBrandInstagram())){
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelBeforeUpdate.getBrandWhatsapp())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelBeforeUpdate.getBrandWebsite())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateEmail.getText().toString().equals(modelBeforeUpdate.getBrandEmail())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (tagChanged) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            }

            bottomEvent();
        });

        binding.layoutBottomVenue.setOnClickListener(view -> {
            destination = "venue";

            if (isImageUpdate){
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateBrandName.getText().toString().equals(modelBeforeUpdate.getBrandName())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateBrandDescription.getText().toString().equals(modelBeforeUpdate.getBrandLongDesc())){
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.editBrandPhoneNumber.getText().toString().equals(modelBeforeUpdate.getBrandPhoneNumber())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelBeforeUpdate.getBrandFacebook())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelBeforeUpdate.getBrandTwitter())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelBeforeUpdate.getBrandInstagram())){
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelBeforeUpdate.getBrandWhatsapp())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelBeforeUpdate.getBrandWebsite())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateEmail.getText().toString().equals(modelBeforeUpdate.getBrandEmail())) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            } else if (tagChanged) {
                needToBackPressedHandled = false;
                openUnsavedDialog();
                return;
            }

            bottomVenue();
        });

        binding.iconEditHeaderPhoto.setOnClickListener(v -> openPopUpPhoto());

        binding.inputBrandTag.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addNewTag();
            }
            return true;
        });

        binding.btnSaveChange.setOnClickListener(v -> {
            if (isImageUpdate){
                updateImage();
            } else {
                showLoadingDialog();
                saveChange(brandHeaderPhoto);
            }
        });

        binding.btnSendCode.setOnClickListener(v -> publishThisBrand());

        binding.tapHere.setOnClickListener(v -> {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                    .replace(R.id.containerStandartActivity, new FragmentPublishBrand()).addToBackStack("").commit();
        });
    }

    private void bottomVenue() {
        FragmentBrandVenue fragmentBrandVenue = new FragmentBrandVenue();
        Bundle bundle = new Bundle();
        bundle.putString("brandID", brandID);
        fragmentBrandVenue.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentBrandVenue).addToBackStack("").commit();
    }

    private void bottomEvent() {
        FragmentBrandEvent fragmentBrandEvent = new FragmentBrandEvent();
        Bundle bundle = new Bundle();
        bundle.putString("brandID", brandID);
        fragmentBrandEvent.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentBrandEvent).addToBackStack("").commit();
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        builder.setView(view);
        builder.setCancelable(false);
        alertDialog = builder.create();
        alertDialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        alertDialog.setCancelable(false);
    }

    private void publishThisBrand() {
        FirebaseFirestore.getInstance().collection("PublishedCode").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null){
                ArrayList<String> listCode = new ArrayList<>();
                for (DocumentSnapshot document: documentSnapshots){
                    listCode.add(document.getString("code"));
                }

                if (listCode.contains(binding.inputCode.getText().toString())){
                    showLoadingDialog();

                    new Thread(this::deleteCurrentCode).start();
                    new Thread(this::makeBrandPublish).start();
                    new Thread(this::makeVenuePublish).start();
                    new Thread(this::makeEventPublish).start();
                } else {
                    Toast.makeText(mContext.getApplicationContext(), mContext.getResources().getString(R.string.wrongCode), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showLoadingDialog() {
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void makeEventPublish(){
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).collection("CollectionEvent"+brandID)
                .get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .update("eventManagedBy", true);
                }

                closeLoadingDialog();
            } else {
                closeLoadingDialog();
                Snackbar.make(getView(), mContext.getResources().getString(R.string.failedToPublish), Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> publishThisBrand()).show();
            }
        });
    }

    private void makeVenuePublish(){
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).collection("CollectionVenue"+brandID)
                .get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Venue").document(document.getString("venueID"))
                            .update("venueIsManagedByBusiness", true);
                }
            } else {
                closeLoadingDialog();
                Snackbar.make(getView(), mContext.getResources().getString(R.string.failedToPublish), Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> publishThisBrand()).show();
            }
        });
    }

    private void makeBrandPublish() {
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).update("brandIsManagedByBusiness", true)
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()){
                        Snackbar.make(getView(), mContext.getResources().getString(R.string.failedToPublish), Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> publishThisBrand()).show();
                    }
                });
    }

    private void deleteCurrentCode() {
        FirebaseFirestore.getInstance().collection("PublishedCode").document(binding.inputCode.getText().toString()).delete()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()){
                        Snackbar.make(getView(), mContext.getResources().getString(R.string.failedToPublish), Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> publishThisBrand()).show();
                    }
                });
    }

    private void saveChange(String headerPhoto) {
        ModelBrand modelBrand = new ModelBrand(listOfTag, brandID, brandUserName, brandCategories, binding.updateBrandName.getText().toString(),
                headerPhoto, binding.editBrandPhoneNumber.getText().toString(), binding.updateBrandDescription.getText().toString(),
                binding.updateFacebook.getText().toString(), binding.updateTwitter.getText().toString(), binding.updateInstagram.getText().toString(),
                binding.updateWhatsApp.getText().toString(), binding.updateWebsite.getText().toString(), binding.updateEmail.getText().toString(),
                FirebaseAuth.getInstance().getCurrentUser().getUid(), isBrandManagedByBusiness, publicBrand);

        FirebaseFirestore.getInstance().collection("Brands").document(brandID).set(modelBrand)
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()){
                    closeLoadingDialog();

                    //change the flag of tag change so the system will not check tag again if user
                    //directly back pressed after save the data
                    tagChanged = false;

                    //change the flag of back pressed checker to true
                    needToBackPressedHandled = true;

                    Snackbar.make(getView(), mContext.getResources().getString(R.string.succesUpdateBrand), Snackbar.LENGTH_LONG).show();
                }
            });
    }

    private void closeLoadingDialog() {
        alertDialog.cancel();
    }

    private void addNewTag() {
        if (binding.inputBrandTag.getText().toString().trim().equals("")){
            Toast.makeText(mContext, R.string.masukanTag, Toast.LENGTH_SHORT).show();
            return;
        }

        tagChanged = true;

        listOfTag = adapterTag.addData(listOfTag.size() - 1, binding.inputBrandTag.getText().toString().trim());

        binding.inputBrandTag.setText(null);
    }

    private void getDropboxIMGSize(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        try {
            BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(uri), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double imageHeight = options.outHeight;
        double imageWidth = options.outWidth;
        double finalValue = imageWidth / imageHeight;

        if (imageHeight < 300 || imageWidth < 300){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
        } else {
            //change the flag if user has change the image header
            isImageUpdate = true;
            //change the flag to true so we can update the image after user change the photo
            isUpdateImageCanceled = true;

            Glide.with(mContext.getApplicationContext()).load(uri).into(binding.imageHeader);
        }

        /*if (finalValue >= 0.5 && finalValue <= 1.8){
            if (imageHeight < 300 || imageWidth < 300){
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
            } else {
                //change the flag if user has change the image header
                isImageUpdate = true;
                //change the flag to true so we can update the image after user change the photo
                isUpdateImageCanceled = true;

                Glide.with(mContext.getApplicationContext()).load(uri).into(binding.imageHeader);
            }
        } else if (finalValue < 0.5){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooHeight), Toast.LENGTH_SHORT).show();
        } else if (finalValue > 1.8){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooWidth), Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){

                case REQUEST_IMAGE_CAPTURE:
                    imageHeaderFrom = "camera";
                    Bundle extras = data.getExtras();
                    photoBitmap = (Bitmap) extras.get("data");
                    binding.imageHeader.setImageBitmap(photoBitmap);

                    //change the flag that user is change the image
                    isImageUpdate = true;

                    //change the flag to tru so we can update the image after user change the photo
                    isUpdateImageCanceled = true;

                    //overide onclick update image
                    binding.iconEdit.setImageResource(R.drawable.icon_cancel);
                    binding.textPhotoCover.setText(R.string.undo);

                    binding.iconEditHeaderPhoto.setOnClickListener((View v) -> {
                        if (isUpdateImageCanceled){
                            isImageUpdate = false;
                            binding.iconEdit.setImageResource(R.drawable.icon_edit);
                            binding.textPhotoCover.setText(R.string.changePhotoCover);
                            Glide.with(mContext.getApplicationContext()).load(brandHeaderPhoto).into(binding.imageHeader);
                            isUpdateImageCanceled = false;
                        } else {
                            openPopUpPhoto();
                        }
                    });
                    break;

                case SELECT_PICTURE:
                    imageHeaderFrom = "storage";
                    selectedImageUri = data.getData();
                    if (null != selectedImageUri) {

                        getDropboxIMGSize(selectedImageUri);

                        //change the flag that user is change the image
                        isImageUpdate = true;

                        //change the flag to true so we can update the image after user change the photo
                        isUpdateImageCanceled = true;

                        //overide onclick update image
                        binding.iconEdit.setImageResource(R.drawable.icon_cancel);
                        binding.textPhotoCover.setText(R.string.undo);

                        binding.iconEditHeaderPhoto.setOnClickListener(v -> {
                            if (isUpdateImageCanceled){
                                isImageUpdate = false;
                                binding.textPhotoCover.setText(R.string.changePhotoCover);
                                binding.iconEdit.setImageResource(R.drawable.icon_edit);
                                Glide.with(mContext.getApplicationContext()).load(brandHeaderPhoto).into(binding.imageHeader);
                                isUpdateImageCanceled = false;
                            } else {
                                openPopUpPhoto();
                            }
                        });
                    }
                    break;
            }
        }
    }

    @Override
    public void tagClick(String tag) {
        tagChanged = true;

        int index = listOfTag.indexOf(tag);

        if (listOfTag.size() == 1){
            listOfTag.remove(index);
            adapterTag.updateData(listOfTag);
        } else {
            listOfTag = adapterTag.removeTag(index);
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needToBackPressedHandled;
    }

    @Override
    public void onBackPress() {
        if (isMaster) {
            needToBackPressedHandled = false;
            getActivity().onBackPressed();
        } else {
            checkBeforeBackPressed();
        }
    }
}
