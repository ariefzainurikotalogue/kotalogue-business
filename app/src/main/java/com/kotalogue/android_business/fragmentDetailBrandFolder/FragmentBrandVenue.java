package com.kotalogue.android_business.fragmentDetailBrandFolder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.fragment.FragmentAddVenue;
import com.kotalogue.android_business.fragmentDetailVenueFolder.FragmentVenueInformation;
import com.kotalogue.android_business.globalAdapter.AdapterListVenue;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelVenue;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentBrandVenueBinding;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentBrandVenue extends Fragment implements AdapterListVenue.AdapterListVenueInterface, BackPressObserver {
    private FragmentBrandVenueBinding binding;
    private Context mContext;
    private String brandID;

    //recycler
    private AdapterListVenue adapterListVenue;
    private ArrayList<ModelVenue> modelVenues = new ArrayList<>();
    private boolean isMaster;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;
    private boolean needBackPressedCheck = true;

    public FragmentBrandVenue() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentBrandVenueBinding.inflate(LayoutInflater.from(mContext), null, false);

        binding.brandInformation.setSelected(true);

        PublicMethod.enableDefaultAnimation(binding.layoutMenu);
        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.layoutMenu, 0,0, 0, PublicMethod.getNavigationBarHeight(mContext));
            PublicMethod.setMargins(binding.recyclerVenue, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogInit();

        recyclerInit();

        loadVenue();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void showDeleteDialog(ArrayList<ModelVenue> modelVenues, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_delete_dialog, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView delete = view.findViewById(R.id.delete);
        TextView cancel = view.findViewById(R.id.cancel);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //hancle click
        delete.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            showDialogLoading();

            deleteVenue(position, modelVenues);

            //this.modelVenues = adapterListVenue.removeData(modelVenues.indexOf(modelVenues.get(position)));
        });

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
    }

    private void deleteVenue(int position, ArrayList<ModelVenue> modelVenues) {
        //check if this venue is subvenue or no
        if (!modelVenues.get(position).getVenueParentID().equals("")){
            //delete this venue from collection subvenue from its parent
            FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position)
                    .getVenueParentID()).collection("CollectionSubVenue")
                    .document(modelVenues.get(position).getVenueID()).delete();
        }

        //remove venue photo gallery
        FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                .collection("VenuePhotoGallery").get().addOnSuccessListener(documentSnapshots -> {
                    if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                        for (DocumentSnapshot document: documentSnapshots){
                            //remove data inside the firestore collection
                            FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                                    .collection("VenuePhotoGallery").document(document.getString("photoID")).delete();

                            //remove the data inside the cloud storage child
                            FirebaseStorage.getInstance().getReference("Venue").child(modelVenues.get(position).getVenueID())
                                    .child(document.getString("photoID")).delete();
                        }
                    }
                });

        //remove the subvenue
        FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                .collection("CollectionSubVenue").get().addOnSuccessListener(documentSnapshots -> {
                    if (documentSnapshots != null){
                        for (DocumentSnapshot document: documentSnapshots){
                            //menghapus id dari collection subvenue dari document venue
                            FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                                    .collection("CollectionSubVenue").document(document.getString("venueID")).delete();

                            //menghapus data subvenue dari venue ini
                            FirebaseFirestore.getInstance().collection("Venue").document(document.getString("venueID")).delete();
                        }
                    }
                });

        FirebaseFirestore.getInstance().collection("Brands").document(modelVenues.get(position).getBrandID())
                .collection("CollectionVenue"+modelVenues.get(position).getBrandID()).document(modelVenues.get(position).getVenueID()).delete();

        //remove the entire data in venue
        FirebaseFirestore.getInstance().collection("Venue").document(modelVenues.get(position).getVenueID())
                .delete().addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        dialogLoading.dismiss();

                        Toast.makeText(mContext, R.string.successDeleteVenue, Toast.LENGTH_SHORT).show();

                        FragmentBrandVenue fragmentBrandVenue = new FragmentBrandVenue();
                        Bundle bundle = new Bundle();
                        bundle.putString("brandID", brandID);
                        fragmentBrandVenue.setArguments(bundle);

                        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .replace(R.id.containerStandartActivity, fragmentBrandVenue).commit();
                    } else {
                        if (PublicMethod.isDeviceOnline(mContext)){
                            Snackbar.make(getView(), task.getException().getMessage(), Snackbar.LENGTH_LONG)
                                    .setAction(mContext.getResources().getString(R.string.retry), v -> deleteVenue(position, modelVenues)).show();
                        }
                    }
                });

    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        builder.setView(view);
        builder.setCancelable(false);
        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.iconAdd.setOnClickListener(v -> {
            FragmentAddVenue fragmentAddVenue = new FragmentAddVenue();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            bundle.putString("checkFirst", "checkFirst");
            fragmentAddVenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddVenue).addToBackStack("").commit();
        });

        binding.layoutBottomInformation.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomEvent.setOnClickListener(view -> {

            FragmentBrandEvent fragmentBrandEvent = new FragmentBrandEvent();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            fragmentBrandEvent.setArguments(bundle);

            getFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentBrandEvent).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomVenue.setOnClickListener(view -> {  });
    }

    private void loadVenue(){
        FirebaseFirestore.getInstance().collection("Venue").whereEqualTo("brandID", brandID)
                .addSnapshotListener((documentSnapshots, e) -> {
            modelVenues.clear();

            if (documentSnapshots != null && documentSnapshots.getDocuments().size() > 0){
                for (DocumentSnapshot document: documentSnapshots){
                    ModelVenue modelVenue = document.toObject(ModelVenue.class);
                    modelVenues = adapterListVenue.addData(modelVenue, modelVenues.size()-1);
                    hideLoading();
                    binding.emptyData.setVisibility(View.GONE);
                }
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                } else {
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void hideLoading(){
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void recyclerInit(){
        adapterListVenue = new AdapterListVenue(modelVenues, mContext, this, getActivity());

        binding.recyclerVenue.setHasFixedSize(true);
        binding.recyclerVenue.setLayoutManager(new LinearLayoutManager(mContext));
        binding.recyclerVenue.setAdapter(adapterListVenue);

        binding.recyclerVenue.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //dy > 0 means it will scrolled and when its get the last item will be notified
                //dy < 0 means Recycle view scrolling up...

                if (dy > 0) {
                    binding.layoutBottomInformation.setVisibility(View.GONE);
                    binding.layoutBottomEvent.setVisibility(View.GONE);
                    binding.layoutBottomVenue.setVisibility(View.GONE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.dp(R.dimen.standart_20dp, mContext));
                } else if (dy < 0){
                    binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                    binding.layoutBottomEvent.setVisibility(View.VISIBLE);
                    binding.layoutBottomVenue.setVisibility(View.VISIBLE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
                }
            }
        });
    }

    @Override
    public void itemClick(ArrayList<ModelVenue> modelVenues, int position) {
        FragmentVenueInformation fragmentVenueInformation = new FragmentVenueInformation();
        Bundle bundle = new Bundle();
        bundle.putString("venueID", modelVenues.get(position).getVenueID());
        fragmentVenueInformation.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.containerStandartActivity, fragmentVenueInformation).addToBackStack("").commit();
    }

    @Override
    public void deleteItem(ArrayList<ModelVenue> modelVenues, int position) {
        showDeleteDialog(modelVenues, position);
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needBackPressedCheck;
    }

    @Override
    public void onBackPress() {
        needBackPressedCheck = false;
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
