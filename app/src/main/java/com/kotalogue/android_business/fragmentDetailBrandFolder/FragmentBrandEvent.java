package com.kotalogue.android_business.fragmentDetailBrandFolder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.fragment.FragmentPickEvent;
import com.kotalogue.android_business.fragment.FragmentPickVenue;
import com.kotalogue.android_business.fragmentDetailEventFolder.FragmentEventInformation;
import com.kotalogue.android_business.globalAdapter.AdapterListEvent;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelEvent;
import com.kotalogue.android_business.databinding.FragmentBrandEventBinding;
import com.kotalogue.android_business.R;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentBrandEvent extends Fragment implements AdapterListEvent.AdapterListEventInterface, BackPressObserver {
    private FragmentBrandEventBinding binding;
    private String brandID;

    //recycler
    private AdapterListEvent adapterListEvent;
    private ArrayList<ModelEvent> modelEvents = new ArrayList<>();
    private Context mContext;
    private boolean isMaster;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;
    private boolean needCheckBackPressed = true;

    public FragmentBrandEvent() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentBrandEventBinding.inflate(LayoutInflater.from(mContext), null, false);

        binding.brandInformation.setSelected(true);

        PublicMethod.enableDefaultAnimation(binding.layoutMenu);
        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);

            PublicMethod.setMargins(binding.layoutMenu, 0,0, 0, PublicMethod.getNavigationBarHeight(mContext));
            PublicMethod.setMargins(binding.recyclerEvent, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogInit();

        recyclerInit();

        loadEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void createEvent() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_pick_event, null);
        builder.setView(view);

        //setclick listener
        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView createSubEvent = view.findViewById(R.id.createSubEvent);
        TextView pickVenue = view.findViewById(R.id.pickVenue);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        createSubEvent.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            FragmentPickEvent fragmentPickEvent = new FragmentPickEvent();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            fragmentPickEvent.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickEvent).addToBackStack("").commit();
        });

        pickVenue.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            FragmentPickVenue fragmentPickVenue = new FragmentPickVenue();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            bundle.putString("openFor", "Event");
            fragmentPickVenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickVenue).addToBackStack("").commit();
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.iconAdd.setOnClickListener(v -> createEvent());

        binding.layoutBottomInformation.setOnClickListener(view -> {
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        binding.layoutBottomEvent.setOnClickListener(view -> {  });

        binding.layoutBottomVenue.setOnClickListener(view -> {

            FragmentBrandVenue fragmentBrandVenue = new FragmentBrandVenue();
            Bundle bundle = new Bundle();
            bundle.putString("brandID", brandID);
            fragmentBrandVenue.setArguments(bundle);

            getFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.containerStandartActivity, fragmentBrandVenue).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });
    }

    private void loadEvent() {
        FirebaseFirestore.getInstance().collection("Event").whereEqualTo("brandID", brandID)
                .addSnapshotListener((documentSnapshots, e) -> {
            modelEvents.clear();

            if (documentSnapshots != null && documentSnapshots.getDocuments().size() > 0){
                for (DocumentSnapshot document: documentSnapshots){
                    ModelEvent modelEvent = document.toObject(ModelEvent.class);
                    modelEvents = adapterListEvent.addData(modelEvent, modelEvents.size()-1);
                    hideLoading();
                    binding.emptyData.setVisibility(View.GONE);
                }
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                } else {
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        builder.setView(view);
        builder.setCancelable(false);
        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void recyclerInit() {
        adapterListEvent = new AdapterListEvent(modelEvents, mContext, this, getActivity());

        binding.recyclerEvent.setHasFixedSize(true);
        binding.recyclerEvent.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerEvent.setAdapter(adapterListEvent);

        binding.recyclerEvent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //dy > 0 means it will scrolled and when its get the last item will be notified
                //dy < 0 means Recycle view scrolling up...

                if (dy > 0) {
                    // Recycle view scrolling down...
                    /*if(recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN) == false){

                    }*/
                    binding.layoutBottomInformation.setVisibility(View.GONE);
                    binding.layoutBottomEvent.setVisibility(View.GONE);
                    binding.layoutBottomVenue.setVisibility(View.GONE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.dp(R.dimen.standart_20dp, mContext));
                } else if (dy < 0){
                    binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                    binding.layoutBottomEvent.setVisibility(View.VISIBLE);
                    binding.layoutBottomVenue.setVisibility(View.VISIBLE);

                    //custom margin
                    PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                            PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
                }
            }
        });
    }

    @Override
    public void itemClick(ArrayList<ModelEvent> modelEvents, int position) {
        FragmentEventInformation fragmentEventInformation = new FragmentEventInformation();
        Bundle bundle = new Bundle();
        bundle.putString("eventID", modelEvents.get(position).getEventID());
        fragmentEventInformation.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.containerStandartActivity, fragmentEventInformation).addToBackStack("").commit();
    }

    @Override
    public void deleteItem(ArrayList<ModelEvent> modelEvents, int position) {
        showDeleteDialog(modelEvents, position);
    }

    private void showDeleteDialog(ArrayList<ModelEvent> modelEvent, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_delete_dialog, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView delete = view.findViewById(R.id.delete);
        TextView cancel = view.findViewById(R.id.cancel);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //hancle click
        delete.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            showDialogLoading();

            deleteEvent(modelEvents, position);

            //this.modelEvents = adapterListEvent.removeData(modelEvents.indexOf(modelEvent));
        });

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
    }

    private void deleteEvent(ArrayList<ModelEvent> modelEvents, int position) {
        //check if this event is subevent or no
        if (!modelEvents.get(position).getEventParentID().equals("")){
            //delete this event from collection sub event from its event parent
            FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventParentID())
                    .collection("CollectionSubEvent").document(modelEvents.get(position).getEventID()).delete();
        }

        //remove event photo gallery
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("EventPhotoGallery").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("EventPhotoGallery").document(document.getString("photoID"))
                            .delete();

                    FirebaseStorage.getInstance().getReference("Event").child(modelEvents.get(position).getEventID())
                            .child(document.getString("photoID")).delete();
                }
            }
        });

        //remove event subevent
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("CollectionSubEvent").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("CollectionSubEvent").document(document.getString("eventID")).delete();

                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID")).delete();
                }
            }
        });

        //remove eventlike
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("CollectionLike").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("CollectionLike").document(document.getString("userID")).delete();
                }
            }
        });

        //remove eventdislike
        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .collection("CollectionDislike").get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                            .collection("CollectionDislike").document(document.getString("userID")).delete();
                }
            }
        });

        FirebaseFirestore.getInstance().collection("Brands").document(modelEvents.get(position).getBrandID())
                .collection("CollectionEvent"+modelEvents.get(position).getBrandID()).document(modelEvents.get(position).getEventID())
                .delete();

        FirebaseFirestore.getInstance().collection("Event").document(modelEvents.get(position).getEventID())
                .delete().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                dialogLoading.dismiss();

                Toast.makeText(mContext, R.string.successDeleteEvent, Toast.LENGTH_SHORT).show();

                FragmentBrandEvent fragmentBrandEvent = new FragmentBrandEvent();
                Bundle bundle = new Bundle();
                bundle.putString("brandID", brandID);
                fragmentBrandEvent.setArguments(bundle);

                getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(R.id.containerStandartActivity, fragmentBrandEvent).commit();
            }
        });
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needCheckBackPressed;
    }

    @Override
    public void onBackPress() {
        needCheckBackPressed = false;
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
