package com.kotalogue.android_business.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalAdapter.AdapterSingleLineText;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelUser;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.LayoutDaftarUserBinding;
import com.kotalogue.android_business.databinding.FragmentTransferBrandBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;

public class FragmentTransferBrand extends Fragment implements BackPressObserver, AdapterSingleLineText.AdapterDaftarUserInterface {
    private FragmentTransferBrandBinding binding;
    private String brandID;

    //recyclerview
    private AdapterSingleLineText adapterSingleLineText;
    private ArrayList<ModelUser> modelUsers = new ArrayList<>();
    private Context mContext;
    private ArrayList<ModelUser> filterList;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
        }

        // Inflate the layout for this fragment
        binding = FragmentTransferBrandBinding.inflate(LayoutInflater.from(mContext), null, false);

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.recyclerDaftarUser, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        /*binding.searchView.onActionViewExpanded();
        binding.searchView.setOnQueryTextListener(this);*/

        dialogInit();

        recyclerInit();

        loadDaftarUser();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();

        binding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                try {
                    filterList = new ArrayList<>();
                    for (ModelUser modelUser : modelUsers){
                        String userEmail = modelUser.getEmail().toLowerCase();
                        if (userEmail.contains(s)){
                            filterList.add(modelUser);
                        }
                    }

                    adapterSingleLineText.setFilter(filterList);
                } catch (InputMismatchException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> getActivity().onBackPressed());
    }

    private void loadDaftarUser() {
        FirebaseFirestore.getInstance().collection("User").whereEqualTo("userType", "Business")
                .get().addOnSuccessListener(documentSnapshots -> {

                    modelUsers.clear();

                    if (documentSnapshots != null && documentSnapshots.getDocuments().size() > 0){
                        for (DocumentSnapshot document : documentSnapshots){
                            ModelUser modelUser = document.toObject(ModelUser.class);
                            //modelUsers.add(modelUser);
                            modelUsers = adapterSingleLineText.addData(modelUser, modelUsers.size()-1);

                            binding.progressBar.setVisibility(View.GONE);
                            binding.iconCircle.setVisibility(View.GONE);
                        }
                    } else {
                        binding.progressBar.setVisibility(View.GONE);
                        binding.iconCircle.setVisibility(View.GONE);
                    }
        });
    }

    private void recyclerInit() {
        adapterSingleLineText = new AdapterSingleLineText(modelUsers, this);

        binding.recyclerDaftarUser.setHasFixedSize(true);
        binding.recyclerDaftarUser.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerDaftarUser.setAdapter(adapterSingleLineText);
    }

    /*@Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

    }*/

    @Override
    public void transferClick(ArrayList<ModelUser> modelUsers, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_continue_transfer, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView transfer = view.findViewById(R.id.transfer);
        TextView cancel = view.findViewById(R.id.cancel);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        transfer.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            showDialogLoading();

            //delete current brand from user who transfer the brand
            deleteCurrentBrand();

            //add current brand to user that has choosen
            transferCurrentBrand(modelUsers, position);

            //change the event userID to the new user
            changeEventUserID(modelUsers, position);

            //change the venue userID to the new user
            changeVenueUserID(modelUsers, position);

            //change brand userID
            changeBrandUserID(modelUsers, position);
        });

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
    }

    private void changeBrandUserID(ArrayList<ModelUser> modelUsers, int position) {
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).update("userID", modelUsers.get(position).getUserID())
            .addOnCompleteListener(task -> {
                if (task.isComplete()){
                    if (task.isComplete()){
                        Toast.makeText(mContext, R.string.redirectingToHome, Toast.LENGTH_SHORT).show();
                        dialogLoading.dismiss();

                        new Handler().postDelayed(() -> getActivity().onBackPressed(), 1000);
                    }
                }
            });
    }

    private void changeVenueUserID(ArrayList<ModelUser> modelUsers, int position) {
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).collection("CollectionVenue"+brandID)
                .get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Venue").document(document.getString("venueID"))
                            .update("userID", modelUsers.get(position).getUserID()).addOnSuccessListener(aVoid -> Log.d(document.getString("venueID"), "Change userID to: " + modelUsers.get(position).getUserID()));
                }
            }
        });
    }

    private void changeEventUserID(ArrayList<ModelUser> modelUsers, int position) {
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).collection("CollectionEvent"+brandID)
                .get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .update("userID", modelUsers.get(position).getUserID()).addOnSuccessListener(aVoid -> Log.d(document.getString("eventID"), "Change userID to: " + modelUsers.get(position).getUserID()));
                }
            }
        });
    }

    private void transferCurrentBrand(ArrayList<ModelUser> modelUsers, int position) {
        Map<String, String> mapBrandID = new HashMap<>();
        mapBrandID.put("brandID", brandID);
        FirebaseFirestore.getInstance().collection("User").document(modelUsers.get(position).getUserID())
                .collection("CollectionBrand").document(brandID).set(mapBrandID).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Toast.makeText(mContext, "Succesfully transfered your brand to " + modelUsers.get(position).getEmail(), Toast.LENGTH_SHORT).show();
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    Toast.makeText(mContext, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
                }
                /*Snackbar.make(getView(), "Failed to transfered your brand, with error " + task.getException().getMessage(), Snackbar.LENGTH_SHORT)
                        .setAction(R.string.resend, v -> {
                            clickTransfer(position);
                        }).show();*/
            }
        });
    }

    private void deleteCurrentBrand() {
        FirebaseFirestore.getInstance().collection("User").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("CollectionBrand").document(brandID).delete();
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return false;
    }

    @Override
    public void onBackPress() {
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
