package com.kotalogue.android_business.fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalAdapter.AdapterPickVenue;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelVenue;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentPickVenueBinding;

import java.util.ArrayList;
import java.util.InputMismatchException;

public class FragmentPickVenue extends Fragment implements AdapterPickVenue.AdapterPickVenueInterface, BackPressObserver {
    private FragmentPickVenueBinding binding;
    private AdapterPickVenue adapterPickVenue;
    private ArrayList<ModelVenue> modelVenues = new ArrayList<>();
    private String brandID;
    private String openFor;
    private ArrayList<ModelVenue> filterList;
    private Context mContext;

    private ArrayList<String> listOfVenue = new ArrayList<>();

    public FragmentPickVenue() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
            openFor = bundle.getString("openFor");
        }

        binding = FragmentPickVenueBinding.inflate(LayoutInflater.from(mContext), null, false);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.recyclerVenue, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        PublicMethod.enableDefaultAnimation(binding.layoutPickGroup);

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        checkOpenFor();

        recyclerInit();

        loadListVenue();
    }

    private void checkOpenFor() {
        if (openFor.equals("Group")){
            binding.textPickGroup.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();

        binding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                try {
                    filterList = new ArrayList<>();
                    for (ModelVenue modelVenue : modelVenues){
                        String venueName = modelVenue.getVenueName().toLowerCase();
                        String venueAddress = modelVenue.getVenueAddress().toLowerCase();
                        if (venueName.contains(s) || venueAddress.contains(s)){
                            filterList.add(modelVenue);
                        }
                    }

                    adapterPickVenue.setFilter(filterList);
                } catch (InputMismatchException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> getActivity().onBackPressed());
        binding.layoutPickGroup.setOnClickListener(view -> {
            if (listOfVenue.size() > 0){
                FragmentPickGroup fragmentPickGroup = new FragmentPickGroup();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("listVenue", listOfVenue);
                fragmentPickGroup.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                        .replace(R.id.containerStandartActivity, fragmentPickGroup)
                        .addToBackStack("").commit();

            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.pickVenueFirst), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadListVenue() {
        FirebaseFirestore.getInstance().collection("Venue").get().addOnSuccessListener(queryDocumentSnapshots -> {
            modelVenues.clear();

            for (DocumentSnapshot document: queryDocumentSnapshots){
                ModelVenue modelVenue = document.toObject(ModelVenue.class);
                modelVenues.add(modelVenue);
            }

            if (modelVenues.size() == 0){
                binding.progressBar.setVisibility(View.GONE);
                binding.iconCircle.setVisibility(View.GONE);
            } else {
                binding.progressBar.setVisibility(View.GONE);
                binding.iconCircle.setVisibility(View.GONE);
            }

            adapterPickVenue.updateData(modelVenues);
        });
    }

    private void recyclerInit() {
        adapterPickVenue = new AdapterPickVenue(modelVenues, openFor, this);

        binding.recyclerVenue.setHasFixedSize(true);
        binding.recyclerVenue.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerVenue.setAdapter(adapterPickVenue);

        binding.recyclerVenue.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (openFor.equals("Group")){
                    if (dy > 0){
                        binding.textPickGroup.setVisibility(View.GONE);
                    } else if (dy < 0){
                        binding.textPickGroup.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    /*@Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        return true;
    }*/

    @Override
    public void openVenue(ArrayList<ModelVenue> modelVenues, int position) {
        FragmentAddVenue fragmentAddVenue = new FragmentAddVenue();
        Bundle bundle = new Bundle();
        bundle.putString("createSubvenue", "createSubvenue");
        bundle.putString("venueLatLon", modelVenues.get(position).getVenueLatLon());
        bundle.putString("venueAddress", modelVenues.get(position).getVenueAddress());
        bundle.putString("venueID", modelVenues.get(position).getVenueID());
        bundle.putString("venueName", modelVenues.get(position).getVenueName());
        bundle.putString("brandID", brandID);
        fragmentAddVenue.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddVenue).commit();
    }

    @Override
    public void openEvent(ArrayList<ModelVenue> modelVenues, int position) {
        FragmentAddEvent fragmentAddEvent = new FragmentAddEvent();
        Bundle bundle = new Bundle();
        bundle.putString("venueLatLon", modelVenues.get(position).getVenueLatLon());
        bundle.putString("venueAddress", modelVenues.get(position).getVenueAddress());
        bundle.putString("venueID", modelVenues.get(position).getVenueID());
        bundle.putString("venueName", modelVenues.get(position).getVenueName());
        bundle.putString("brandID", brandID);
        fragmentAddEvent.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddEvent).commit();
    }

    @Override
    public void insertToGroup(ArrayList<ModelVenue> modelVenues, int position, ImageView iconCheck) {
        if (listOfVenue.contains(modelVenues.get(position).getVenueID())){
            listOfVenue.remove(modelVenues.get(position).getVenueID());
            iconCheck.setVisibility(View.GONE);

            adapterPickVenue.updatePickedVenue(listOfVenue);

            Log.d("LIST_PICKED_VENUE", listOfVenue.toString());

            if (listOfVenue.size() > 0){
                binding.textPickGroup.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
            } else {
                binding.textPickGroup.setBackgroundColor(mContext.getResources().getColor(R.color.blackOppacity80));
            }
        } else {
            listOfVenue.add(modelVenues.get(position).getVenueID());
            iconCheck.setVisibility(View.VISIBLE);
            adapterPickVenue.updatePickedVenue(listOfVenue);

            Log.d("LIST_PICKED_VENUE", listOfVenue.toString());

            binding.textPickGroup.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return false;
    }

    @Override
    public void onBackPress() {
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}

