package com.kotalogue.android_business.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.globalInterface.FragmentPublishBrandInterface;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentPublishBrandBinding;

public class FragmentPublishBrand extends Fragment implements BackPressObserver {
    private FragmentPublishBrandBinding binding;
    private Context mContext;
    private boolean isTranslate = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = FragmentPublishBrandBinding.inflate(LayoutInflater.from(mContext), null, false);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);
        }*/

        ViewGroup.LayoutParams toolbarParam = binding.viewToolbar.getLayoutParams();
        toolbarParam.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(toolbarParam);

        binding.setClick(fragmentPublishBrandInterface);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    private FragmentPublishBrandInterface fragmentPublishBrandInterface = new FragmentPublishBrandInterface() {
        @Override
        public void back() { getActivity().onBackPressed(); }

        @Override
        public void sendEmail() {
            String text =  "Brand name: " + binding.inputBrandName.getText().toString() + "\n" +
                           "Brand address: " + binding.inputBrandAddress.getText().toString() + "\n" +
                           "Brand trademark: " + binding.inputBrandTrademarks.getText().toString() + "\n" +
                           "Brand website: " + binding.inputBrandWebsite.getText().toString() + "\n" +
                           "Brand instagram: " + binding.inputBrandInstagram.getText().toString() + "\n" +
                           "Brand facebook: " + binding.inputBrandFacebook.getText().toString() + "\n" +
                           "Brand twitter: " + binding.inputBrandTwitter.getText().toString();

            String[] TO = {"support@kotalogue.com"};
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");

            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Publish My Brand");
            emailIntent.putExtra(Intent.EXTRA_TEXT, text);

            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                getActivity().finish();
                Log.i("Success Send Email", "");
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding.iconBack.setOnClickListener(v -> getActivity().onBackPressed());
        binding.translate.setOnClickListener(v -> {
            if (isTranslate){
                binding.textPublish.setText(mContext.getResources().getString(R.string.textPublish));
                isTranslate = false;
            } else {
                binding.textPublish.setText(mContext.getResources().getString(R.string.textPublishEng));
                isTranslate = true;
            }
        });
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return false;
    }

    @Override
    public void onBackPress() {
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
