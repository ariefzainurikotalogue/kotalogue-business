package com.kotalogue.android_business.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.storage.FirebaseStorage;
import com.kotalogue.android_business.activity.LoginActivity;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.fragmentDetailBrandFolder.FragmentBrandInformation;
import com.kotalogue.android_business.globalAdapter.AdapterRecyclerBrand;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelBrand;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentHomeBinding;
import com.kotalogue.android_business.databinding.LayoutItemBrandBinding;

import java.util.ArrayList;

public class FragmentHome extends Fragment implements AdapterRecyclerBrand.AdapterRecyclerBrandInterface, BackPressObserver {
    private FragmentHomeBinding binding;

    private ArrayList<ModelBrand> modelBrands = new ArrayList<>();
    private AdapterRecyclerBrand adapterRecyclerBrand;

    //shared preference
    SharedPreferences algoliaPreference;
    SharedPreferences.Editor algoliaPreferenceEditor;
    public static String ALGOLIA_PREFERENCE = "ALGOLIA_PREFERENCE";
    public static String IS_FIRST_TIME_GET_KEY = "IS_FIRST_TIME_GET_KEY";
    public static String ADMIN_API_KEY = "ADMIN_API_KEY";
    public static String SEARCH_API_KEY = "SEARCH_API_KEY";
    public static String APP_ID = "APP_ID";
    private String userID;
    private Context mContext;

    //master preference
    private SharedPreferences masterPref;
    private ListenerRegistration loadAllListener;
    private DocumentSnapshot lastDocumentVisible;
    private ListenerRegistration loadMoreAllListener;
    private DocumentSnapshot prevItemVisible;
    private String emailUser;
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        emailUser = getActivity().getIntent().getStringExtra("email");
        userID = getActivity().getIntent().getStringExtra("userID");

        binding = FragmentHomeBinding.inflate(LayoutInflater.from(mContext), null, false);

        preloadViewInit();

        checkFirstTimeGetKey();

        recyclerInit();

        dialogInit();

        checkLoad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    private void preloadViewInit() {
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);

        binding.email.setSelected(true);
        binding.emptyData.setSelected(true);

        //shared preference init
        algoliaPreference = getActivity().getSharedPreferences("ALGOLIA_PREFERENCE", Context.MODE_PRIVATE);
        algoliaPreferenceEditor = algoliaPreference.edit();
        masterPref = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE);

        //set current email
        boolean isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        if (isMaster){
            binding.email.setText(emailUser);
            binding.textLoginAs.setText(R.string.watching);
        } else {
            binding.email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
            binding.textLoginAs.setText(R.string.currentlyLogin);
            binding.iconAddToGroup.setVisibility(View.INVISIBLE);
        }

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.recyclerDaftarBrand, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
            PublicMethod.setMargins(binding.fabAddBrand, PublicMethod.dp(R.dimen.standart_20dp, mContext), 0, PublicMethod.dp(R.dimen.standart_20dp, mContext), PublicMethod.getNavigationBarHeight(mContext) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void checkLoad() {
        if (masterPref.getBoolean(StaticVariable.IS_MASTER, false)){
            binding.fabAddBrand.hide();
            loadAllBrands();
        } else {
            binding.fabAddBrand.show();
            loadDaftarBrand();
        }
    }

    private void loadAllBrands() {
        loadAllListener = FirebaseFirestore.getInstance().collection("Brands")
                .addSnapshotListener((documentSnapshots, e) -> {
            if (documentSnapshots != null && documentSnapshots.size() > 0) {
                modelBrands.clear();

                for (DocumentSnapshot document : documentSnapshots){
                    ModelBrand modelBrand = document.toObject(ModelBrand.class);
                    modelBrands = adapterRecyclerBrand.addData(modelBrand, modelBrands.size()-1);
                }

                binding.emptyData.setVisibility(View.GONE);
                hideLoading();

            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                    Toast.makeText(mContext, R.string.noConnected, Toast.LENGTH_SHORT).show();
                } else {
                    hideLoading();
                    binding.emptyData.setVisibility(View.VISIBLE);
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadMoreAll(){
        loadMoreAllListener = FirebaseFirestore.getInstance().collection("Brands").startAfter(lastDocumentVisible).limit(10).addSnapshotListener((documentSnapshots, e) -> {
            if (documentSnapshots != null){

                for (DocumentSnapshot document: documentSnapshots){
                    ModelBrand modelBrand = document.toObject(ModelBrand.class);
                    modelBrands.add(modelBrand);
                    lastDocumentVisible = documentSnapshots.getDocuments().get(documentSnapshots.size()-1);
                    binding.layoutLoading.setVisibility(View.GONE);
                }

                adapterRecyclerBrand.updateData(modelBrands);

                FirebaseFirestore.getInstance().collection("Brands").addSnapshotListener((documentSnapshots1, e1) -> {
                    if (e1 == null) {
                        prevItemVisible = documentSnapshots1.getDocuments().get(documentSnapshots1.size() - 1);

                        if (prevItemVisible.getId().equals(lastDocumentVisible.getId())){
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.oopsNoMoreItem), Toast.LENGTH_SHORT).show();
                            binding.layoutLoading.setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    Toast.makeText(mContext, R.string.noConnected, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                binding.layoutLoading.setVisibility(View.GONE);
            }
        });
    }

    private void checkFirstTimeGetKey() {
        if (algoliaPreference.getBoolean("IS_FIRST_TIME_GET_KEY", true)){
            FirebaseFirestore.getInstance().collection("Algolia").document("AlgoliaKey").get().addOnSuccessListener(documentSnapshot -> {
                algoliaPreferenceEditor.putString("ADMIN_API_KEY", documentSnapshot.getString("admin_api_key"));
                algoliaPreferenceEditor.putString("SEARCH_API_KEY", documentSnapshot.getString("search_api_key"));
                algoliaPreferenceEditor.putString("APP_ID", documentSnapshot.getString("app_id"));
                algoliaPreferenceEditor.putBoolean("IS_FIRST_TIME_GET_KEY", false);
                algoliaPreferenceEditor.commit();
            });
        }
    }

    private void clickListener() {
        binding.iconMenu.setOnClickListener(v -> openSettingPopup());
        binding.iconAddToGroup.setOnClickListener(view -> {
            FragmentPickVenue fragmentPickVenue = new FragmentPickVenue();
            Bundle bundle = new Bundle();
            bundle.putString("openFor", "Group");
            fragmentPickVenue.setArguments(bundle);

            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                    .replace(R.id.containerStandartActivity, fragmentPickVenue)
                    .addToBackStack("").commit();
        });
        binding.fabAddBrand.setOnClickListener(v -> getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, new FragmentAddBrand()).addToBackStack("").commit());
    }

    private void openSettingPopup() {
        PopupMenu popupMenu = new PopupMenu(getContext(), binding.iconMenu);
        popupMenu.getMenuInflater().inflate(R.menu.setting_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.languange:
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                            .replace(R.id.containerStandartActivity, new FragmentSetting()).addToBackStack("").commit();
                    break;

                case R.id.logout:
                    FirebaseAuth.getInstance().signOut();

                    Toast.makeText(mContext, R.string.redirectToLoginPage, Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(mContext, LoginActivity.class));
                        getActivity().finish();
                    }, 500);
                    break;
            }

            return false;
        });
        popupMenu.show();
    }

    private void loadDaftarBrand() {
        FirebaseFirestore.getInstance().collection("Brands").whereEqualTo("userID", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addSnapshotListener((documentSnapshots, e) -> {
                    if (documentSnapshots != null && documentSnapshots.getDocuments().size() > 0){

                        modelBrands.clear();

                        for (DocumentSnapshot document : documentSnapshots){
                            ModelBrand modelBrand = document.toObject(ModelBrand.class);
                            modelBrands = adapterRecyclerBrand.addData(modelBrand, modelBrands.size()-1);
                            hideLoading();
                        }

                    } else {
                        if (PublicMethod.isDeviceOnline(mContext)){
                            hideLoading();
                            binding.emptyData.setVisibility(View.VISIBLE);
                        } else {
                            hideLoading();
                            binding.emptyData.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    private void hideLoading() {
        binding.progressBar.setVisibility(View.GONE);
        binding.iconCircle.setVisibility(View.GONE);
    }

    private void recyclerInit() {
        adapterRecyclerBrand = new AdapterRecyclerBrand(modelBrands, mContext, this);

        binding.recyclerDaftarBrand.setHasFixedSize(true);
        binding.recyclerDaftarBrand.setLayoutManager(new LinearLayoutManager(mContext));
        binding.recyclerDaftarBrand.setAdapter(adapterRecyclerBrand);

        binding.recyclerDaftarBrand.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                //scroll down
                if (dy > 0){
                    binding.fabAddBrand.hide();

                    //user scrolled to bottom
                    /*if(recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN) == false){
                        if (masterPref.getBoolean(StaticVariable.IS_MASTER, false)){
                            progressBar.setVisibility(View.VISIBLE);
                            imageView.setVisibility(View.VISIBLE);

                            loadMoreAll();
                        }
                    }*/

                }
                //scroll up
                else if (dy < 0){
                    if (masterPref.getBoolean(StaticVariable.IS_MASTER, false)){
                        binding.fabAddBrand.hide();
                    } else {
                        binding.fabAddBrand.show();
                    }
                }
            }
        });
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);
        builder.setView(view);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
    }

    private void showDialogLoading() {
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void removeFollower(int position) {
        FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                .collection("CollectionFollower"+modelBrands.get(position).getBrandID()).get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null && !documentSnapshots.isEmpty()){
                for (DocumentSnapshot document: documentSnapshots){
                    FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                            .collection("CollectionFollower"+modelBrands.get(position).getBrandID())
                            .document(document.getString("userID")).delete();
                }
            }
        });
    }

    private void removeEvent(int position) {
        FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                .collection("CollectionEvent"+modelBrands.get(position).getBrandID())
                .get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null){
                for (DocumentSnapshot document: documentSnapshots){

                    //remove item inside the event photo gallery sub collection
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .collection("EventPhotoGallery").get().addOnSuccessListener(documentSnapshots1 -> {
                        if (documentSnapshots1 != null){
                            for (DocumentSnapshot data1: documentSnapshots1){
                                //delete all image inside the firebase cloud storage reference
                                FirebaseStorage.getInstance().getReference("Event").child(document.getString("eventID"))
                                        .child(data1.getString("photoID")).delete();

                                //delete all document photo inside the EventPhotoGallery sub collection
                                FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                                        .collection("EventPhotoGallery").document(data1.getString("photoID")).delete();

                            }
                        }
                    });

                    //remove item inside the subevent in sub collection
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .collection("CollectionSubEvent").get().addOnSuccessListener(documentSnapshots1 -> {
                        if (documentSnapshots1 != null){
                            for (DocumentSnapshot dataSnapshot: documentSnapshots1){
                                FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                                        .collection("CollectionSubEvent").document(dataSnapshot.getString("eventID"))
                                        .delete();
                            }
                        }
                    });

                    //remove document inside the subcollection CollectionDislike
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .collection("CollectionDislike").get().addOnSuccessListener(documentSnapshots1 -> {
                        if (documentSnapshots1 != null){
                            for (DocumentSnapshot dataDislike : documentSnapshots1){
                                FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                                        .collection("CollectionDislike").document(dataDislike.getString("userID"))
                                        .delete();
                            }
                        }
                    });

                    //remove document inside the subcollection CollectionLike
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                            .collection("CollectionLike").get().addOnSuccessListener(documentSnapshots1 -> {
                        if (documentSnapshots1 != null){
                            for (DocumentSnapshot dataLike: documentSnapshots1){
                                FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID"))
                                        .collection("CollectionLike").document(dataLike.getString("userID"))
                                        .delete();
                            }
                        }
                    });

                    //remove image header in firebase cloud storage
                    FirebaseStorage.getInstance().getReference("Event").child(document.getString("eventID")).child(document.getString("eventID")).delete();

                    //remove data inside the collection event in specific document
                    FirebaseFirestore.getInstance().collection("Event").document(document.getString("eventID")).delete();

                    //remove data inside the collection event in document brand
                    FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                            .collection("CollectionEvent"+modelBrands.get(position).getBrandID())
                            .document(document.getString("eventID")).delete();
                }
            }
        });
    }

    private void removeVenue(int position) {
        FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                .collection("CollectionVenue"+modelBrands.get(position).getBrandID()).get().addOnSuccessListener(documentSnapshots -> {
            if (documentSnapshots != null){
                for (DocumentSnapshot data: documentSnapshots){

                    FirebaseFirestore.getInstance().collection("Venue").document(data.getString("venueID"))
                            .collection("VenuePhotoGallery").get().addOnSuccessListener(documentSnapshots1 -> {
                        if (documentSnapshots1 != null){
                            for (DocumentSnapshot data1 : documentSnapshots1){
                                FirebaseStorage.getInstance().getReference("Venue").child(data.getString("venueID"))
                                        .child(data1.getString("photoID")).delete();

                                FirebaseFirestore.getInstance().collection("Venue").document(data.getString("venueID"))
                                        .collection("VenuePhotoGallery").document(data1.getString("photoID"))
                                        .delete();
                            }
                        }
                    });

                    FirebaseFirestore.getInstance().collection("Venue").document(data.getString("venueID"))
                            .collection("CollectionSubVenue").get().addOnSuccessListener(documentSnapshots1 -> {
                        if (documentSnapshots1 != null){
                            for (DocumentSnapshot document : documentSnapshots1){
                                FirebaseFirestore.getInstance().collection("Venue").document(data.getString("venueID"))
                                        .collection("CollectionSubVenue").document(document.getString("venueID")).delete();
                            }
                        }
                    });

                    //remove the document inside the collection venue
                    FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                            .collection("CollectionVenue"+modelBrands.get(position).getBrandID())
                            .document(data.getString("venueID")).delete();

                    //remove image header in cloud storage
                    FirebaseStorage.getInstance().getReference("Venue").child(data.getString("venueID")).child(data.getString("venueID")).delete();

                    //remove the entire data in document
                    FirebaseFirestore.getInstance().collection("Venue").document(data.getString("venueID")).delete();

                    Log.d("REMOVE_VENUE", data.getString("venueID") + " was removed");
                }
            }
        });
    }

    private void removeBrand(int position) {
        new Thread(() -> FirebaseFirestore.getInstance().collection("User").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("CollectionBrand").document(modelBrands.get(position).getBrandID()).delete()).start();

        new Thread(() -> FirebaseStorage.getInstance().getReference("Brand").child(modelBrands.get(position).getBrandID()).delete()).start();

        new Thread(() -> FirebaseFirestore.getInstance().collection("Brands").document(modelBrands.get(position).getBrandID())
                .delete().addOnSuccessListener(aVoid -> {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.succesDeleteBrands), Toast.LENGTH_SHORT).show();

                    dialogLoading.dismiss();

                    getFragmentManager().beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.containerStandartActivity, new FragmentHome()).commit();
                }))
                .start();
    }

    @Override
    public void itemClick(ArrayList<ModelBrand> modelBrands, int position) {
        FragmentBrandInformation fragmentBrandInformation = new FragmentBrandInformation();
        Bundle bundle = new Bundle();
        bundle.putSerializable("brandID", modelBrands.get(position).getBrandID());
        fragmentBrandInformation.setArguments(bundle);

        getFragmentManager().beginTransaction()
                //reverse animation
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.containerStandartActivity, fragmentBrandInformation).addToBackStack("").commit();
    }

    @Override
    public void transferClick(ArrayList<ModelBrand> modelBrands, int position) {
        FragmentTransferBrand fragmentTransferBrand = new FragmentTransferBrand();
        Bundle bundle = new Bundle();
        bundle.putString("brandID", modelBrands.get(position).getBrandID());
        fragmentTransferBrand.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.containerStandartActivity, fragmentTransferBrand).addToBackStack("").commit();
    }

    @Override
    public void deleteBrand(ArrayList<ModelBrand> modelBrands, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.deletingBrand);
        builder.setMessage(R.string.deleteBrand);
        builder.setPositiveButton(R.string.delete, (dialog, which) -> {
            showDialogLoading();

            //removeData(position);
            removeBrand(position);
            removeVenue(position);
            removeEvent(position);
            removeFollower(position);
        });

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return false;
    }

    @Override
    public void onBackPress() {
        getActivity().finishAffinity();
    }
}
