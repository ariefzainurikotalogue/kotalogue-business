package com.kotalogue.android_business.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalInterface.FragmentAddEventInterface;

import java.util.Calendar;

/**
 * Created by ariefzainuri on 16/04/18.
 */

@SuppressLint("ValidFragment")
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    String openFrom;
    TextView textTime;
    private TimePickerInterface timePickerInterface;

    //local
    private Context mContext;

    public TimePickerFragment(String openFrom, TextView textTime, TimePickerInterface timePickerInterface) {
        this.openFrom = openFrom;
        this.textTime = textTime;
        this.timePickerInterface = timePickerInterface;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        switch (openFrom){
            case "startTime":
                String newHour = String.valueOf(hourOfDay);
                String newMinute = String.valueOf(minute);
                long longTime = PublicMethod.dateStringToLong(newHour + ":" + newMinute, "k:m");
                textTime.setText(PublicMethod.dateLongToString(longTime, "kk:mm", mContext));
                break;

            case "finishTime":
                String newHourFinish = String.valueOf(hourOfDay);
                String newMinuteFinish = String.valueOf(minute);
                long longTimeFinish = PublicMethod.dateStringToLong(newHourFinish + ":" + newMinuteFinish, "k:m");
                textTime.setText(PublicMethod.dateLongToString(longTimeFinish, "kk:mm", mContext));
                timePickerInterface.onTimeSet();
                break;
        }
    }

    public interface TimePickerInterface{
        void onTimeSet();
    }
}
