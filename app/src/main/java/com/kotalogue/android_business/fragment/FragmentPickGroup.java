package com.kotalogue.android_business.fragment;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.databinding.FragmentPickGroupBinding;
import com.kotalogue.android_business.globalAdapter.AdapterFragmentPickGroup;
import com.kotalogue.android_business.model.ModelGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPickGroup extends Fragment implements AdapterFragmentPickGroup.AdapterFragmentPickGroupInterface {
    private static final String TAG = "FragmentPickGroup";

    private FragmentPickGroupBinding binding;
    private Context mContext;
    private ArrayList<String> listVenue;
    private AdapterFragmentPickGroup adapterFragmentPickGroup;
    private ArrayList<ModelGroup> modelGroups = new ArrayList<>();
    private ArrayList<String> listFailureSend = new ArrayList<>();
    private RelativeLayout layoutLoading;
    private AlertDialog dialogLoading;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public FragmentPickGroup() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            listVenue = bundle.getStringArrayList("listVenue");
        }

        binding = FragmentPickGroupBinding.inflate(LayoutInflater.from(mContext), null, false);

        PublicMethod.enableDefaultAnimation(binding.layoutLoading);

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        recyclerInit();

        loadItem();

        dialogInit();
    }

    private void loadItem() {
        FirebaseFirestore.getInstance().collection("Group").addSnapshotListener((documentSnapshots, e) -> {
           if (documentSnapshots != null){
               modelGroups.clear();

               for (DocumentSnapshot documentSnapshot : documentSnapshots){
                   ModelGroup modelGroup = documentSnapshot.toObject(ModelGroup.class);
                   modelGroups.add(modelGroup);
               }

               finishLoading();

               adapterFragmentPickGroup.updateData(modelGroups);
           }
        });
    }

    private void finishLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void recyclerInit() {
        adapterFragmentPickGroup = new AdapterFragmentPickGroup(modelGroups, this);

        binding.recyclerGroup.setHasFixedSize(true);
        binding.recyclerGroup.setLayoutManager(new LinearLayoutManager(mContext));
        binding.recyclerGroup.setAdapter(adapterFragmentPickGroup);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> getActivity().onBackPressed());
    }

    @Override
    public void itemClick(ArrayList<ModelGroup> modelGroups, int i) {
        showAdapterPrompt(modelGroups.get(i).getGroupID());
    }

    private void dialogInit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.setCancelable(false);
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void hideDialogLoading(){
        dialogLoading.dismiss();
    }

    private void showAdapterPrompt(String groupID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_insert_to_group, null);
        builder.setView(view);

        LinearLayout layout = view.findViewById(R.id.rootLayoutDialog);
        TextView yes = view.findViewById(R.id.yes);
        TextView cancel = view.findViewById(R.id.cancel);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(layout) - PublicMethod.dp(R.dimen.dp60, mContext),
                ViewGroup.LayoutParams.WRAP_CONTENT);

        yes.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            showDialogLoading();

            for (String venueID : listVenue){
                Map<String, String> map = new HashMap<>();
                map.put("venueID", venueID);
                FirebaseFirestore.getInstance().collection("Group").document(groupID)
                        .collection("CollectionVenue").document(venueID).set(map)
                        .addOnSuccessListener(aVoid -> Log.d("SUCCES_INSERT", venueID))
                        .addOnFailureListener(e -> listFailureSend.add(venueID));

                Map<String, String> mapGroup = new HashMap<>();
                mapGroup.put("groupID", groupID);

                FirebaseFirestore.getInstance().collection("Venue").document(venueID)
                        .collection("CollectionGroup").document(groupID)
                        .set(mapGroup).addOnSuccessListener(aVoid -> Log.d(TAG,
                        "Success update " + venueID + " with " + groupID))
                        .addOnFailureListener(e -> { if (!listFailureSend.contains(venueID)) listFailureSend.add(venueID); });
            }

            hideDialogLoading();

            checkFailureSend(groupID);

            Toast.makeText(mContext, R.string.succesInsertToGroup, Toast.LENGTH_SHORT).show();
        });

        cancel.setOnClickListener(view1 -> alertDialog.dismiss());
    }

    private void checkFailureSend(String groupID) {
        if (listFailureSend.size() > 0){
            Snackbar.make(getView(), mContext.getResources().getString(R.string.failedToInsert) + " venue", Snackbar.LENGTH_LONG)
                    .setAction(R.string.resend, view2 -> {
                        for (String venueID : listFailureSend){
                            Map<String, String> map = new HashMap<>();
                            map.put("venueID", venueID);
                            FirebaseFirestore.getInstance().collection("Group").document(groupID)
                                    .collection("CollectionVenue").document(venueID).set(map)
                                    .addOnSuccessListener(aVoid -> Log.d("SUCCES_INSERT", venueID))
                                    .addOnFailureListener(e -> listFailureSend.add(venueID));
                        }

                        checkFailureSend(groupID);
                    }).show();
        }
    }
}
