package com.kotalogue.android_business.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.fragmentDetailVenueFolder.FragmentVenueInformation;
import com.kotalogue.android_business.globalAdapter.AdapterPickPhoto;
import com.kotalogue.android_business.globalAdapter.AdapterTag;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.globalInterface.FragmentAddVenueInterface;
import com.kotalogue.android_business.model.ModelGallery;
import com.kotalogue.android_business.model.ModelVenue;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentAddVenueBinding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class FragmentAddVenue extends Fragment implements BackPressObserver {
    //request code
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int SELECT_PICTURE = 2;
    private static final int PLACE_PICKER_REQUEST = 3;

    //view
    private FragmentAddVenueBinding binding;
    private final FirebaseFirestore fs = FirebaseFirestore.getInstance();
    private String venueLatLon = "";
    private Bitmap photoBitmap;
    private String brandID;
    private String checkFirst;
    private String venueName;

    //recyclerview
    private ArrayList<String> listOfTag = new ArrayList<>();

    //intent from super venue
    private String venueIDParent, createSubvenue, intentVenueLatLon, intentVenueAddress;
    private String userID;
    private Context mContext;
    private Uri selectedImageUri;
    private String imageHeaderFrom = "a";
    private String mCurrentPhotoPath;
    private AlertDialog dialogLoading;
    private boolean needToBackPressHandled = true;
    private RelativeLayout layoutLoading;
    private String specificVenue;
    private boolean isFirstTimeDescription = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
            specificVenue = bundle.getString("specificVenue");
            checkFirst = bundle.getString("checkFirst");
            createSubvenue = bundle.getString("createSubvenue");
            intentVenueAddress = bundle.getString("venueAddress");
            intentVenueLatLon = bundle.getString("venueLatLon");
            venueIDParent = bundle.getString("venueID");
            venueName = bundle.getString("venueName");
        }

        userID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        binding = FragmentAddVenueBinding.inflate(LayoutInflater.from(mContext), null, false);

        preloadView();

        dialogInit();

        if (createSubvenue != null){
            setDefaultLayout();
        }

        if (checkFirst != null){
            openDialogVenueOrSubvenue();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //show snackbar
        Snackbar.make(getView(),"You pick " + venueName + " to your venues parent", Snackbar.LENGTH_SHORT).show();

        clickListener();

        listener();
    }

    private void listener() {
        binding.inputVenueDescription.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (!isFirstTimeDescription) {
                ViewGroup.LayoutParams stepperParam = binding.viewStepper4.getLayoutParams();
                stepperParam.height = PublicMethod.getViewHeight(binding.inputVenueDescription) + PublicMethod.dp(R.dimen.dp37, mContext);
                binding.viewStepper4.setLayoutParams(stepperParam);
            }
        });

        binding.inputVenueAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int numberChar = s.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper2.setVisibility(View.GONE);
                    binding.imageStepper2.setVisibility(View.VISIBLE);
                    binding.inputVenueName.setVisibility(View.VISIBLE);
                    setViewStepper(binding.viewStepper3, binding.layoutVenueName);
                } else {
                    binding.stepper2.setVisibility(View.VISIBLE);
                    binding.imageStepper2.setVisibility(View.GONE);
                    binding.inputVenueName.setVisibility(View.GONE);
                    setViewStepper(binding.viewStepper3, binding.layoutVenueName);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.inputVenueName.setVisibility(View.VISIBLE);
                setViewStepper(binding.viewStepper3, binding.layoutVenueName);
            }
        });

        binding.inputVenueName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int numberChar = s.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper3.setVisibility(View.GONE);
                    binding.imageStepper3.setVisibility(View.VISIBLE);
                    binding.inputVenueDescription.setVisibility(View.VISIBLE);

                    isFirstTimeDescription = false;
                    ViewGroup.LayoutParams stepperParam = binding.viewStepper4.getLayoutParams();
                    stepperParam.height = PublicMethod.getViewHeight(binding.inputVenueDescription);
                    binding.viewStepper4.setLayoutParams(stepperParam);
                } else {
                    binding.stepper3.setVisibility(View.VISIBLE);
                    binding.imageStepper3.setVisibility(View.GONE);
                    binding.inputVenueDescription.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.inputVenueDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int numberChar = s.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper4.setVisibility(View.GONE);
                    binding.imageStepper4.setVisibility(View.VISIBLE);
                    binding.venueCategories.setVisibility(View.VISIBLE);
                    PublicMethod.setMargins(binding.venueCategories, PublicMethod.dp(R.dimen.dp15, mContext),
                            0, PublicMethod.dp(R.dimen.dp15, mContext),
                            PublicMethod.getViewHeight(binding.btnRegisterVenue) + PublicMethod.dp(R.dimen.dp15, mContext));
                } else {
                    binding.stepper4.setVisibility(View.VISIBLE);
                    binding.imageStepper4.setVisibility(View.GONE);
                    binding.venueCategories.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void clickListener() {
        binding.inputVenueName.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (binding.inputVenueName.getText().toString().trim().length() == 0){
                    Toast.makeText(mContext, "Please write your venue name first.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    binding.inputVenueDescription.requestFocus();
                    return true;
                }
            }
            return true;
        });

        binding.layoutChangePhoto.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), binding.imageHeader);
            popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.camera:
                        dispatchTakePictureIntent();
                        break;

                    case R.id.storage:
                        Intent intentStorage = new Intent();
                        intentStorage.setType("image/*");
                        intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE );
                        break;
                }
                return false;
            });

            popupMenu.show();
        });

        binding.btnRegisterVenue.setOnClickListener(v -> {
            if (PublicMethod.isDeviceOnline(mContext)){
                final String venueName = binding.inputVenueName.getText().toString();
                final String venueDescription = binding.inputVenueDescription.getText().toString();
                final String venueCategories = binding.venueCategories.getText().toString();
                final String venueAddress = binding.inputVenueAddress.getText().toString();
                final String venueID = UUID.randomUUID().toString();

                if (venueAddress.equals(getResources().getString(R.string.tapMap))){
                    Toast.makeText(getContext(), "Please choose your venue location first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (venueName.isEmpty()){
                    Toast.makeText(getContext(), "Please fill your venue name", Toast.LENGTH_SHORT).show();
                    return;
                } else if (venueDescription.isEmpty()){
                    Toast.makeText(getContext(), "Please fill the venue description before continue", Toast.LENGTH_SHORT).show();
                    return;
                } else if (venueCategories.equals(getResources().getString(R.string.tapToChooseCategories))){
                    Toast.makeText(getContext(), "Please choose your venue categories before continue", Toast.LENGTH_SHORT).show();
                    return;
                }

                showDialogLoading();

                saveImage("", venueCategories, venueAddress, "", venueID, venueName, venueDescription, "",
                        "", "", "", "", "");
            } else {
                Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
            }
        });

        binding.venueCategories.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), binding.venueCategories);
            popupMenu.getMenuInflater().inflate(R.menu.menu_categories, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.food:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.foodBeverages);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.shopping:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.shoppingCenters);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.automotive:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.automotive);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.fashion:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.fashion);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.health:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.health);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.electronics:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.electronics);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.houseHolds:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.houseHold);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.souvenirs:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.souvenir);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.tourist:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.tourist);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;

                    case R.id.emergencies:
                        checkStepper6();
                        binding.venueCategories.setText(R.string.emergencies);
                        binding.btnRegisterVenue.setVisibility(View.VISIBLE);
                        break;
                }

                return false;
            });

            popupMenu.show();
        });

        binding.iconBack.setOnClickListener(v -> getActivity().onBackPressed());

        binding.skip.setOnClickListener(v -> {
            binding.stepper1.setVisibility(View.GONE);
            binding.imageStepper1.setVisibility(View.VISIBLE);
            binding.inputVenueAddress.setVisibility(View.VISIBLE);
            binding.textPickLocation.setVisibility(View.VISIBLE);
            setViewStepper(binding.viewStepper2, binding.layoutAddress);
        });

        binding.textPickLocation.setOnClickListener(v -> {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        });
    }

    private void checkStepper6() {
        binding.stepper5.setVisibility(View.GONE);
        binding.imageStepper5.setVisibility(View.VISIBLE);
    }

    private void setViewStepper(View viewStepper, ViewGroup layout){
        ViewGroup.LayoutParams viewParam = viewStepper.getLayoutParams();
        viewParam.height = PublicMethod.getViewHeight(layout) - PublicMethod.dp(R.dimen.dp40, mContext);
        viewStepper.setLayoutParams(viewParam);
    }

    private void preloadView() {
        binding.textPickLocation.setVisibility(View.GONE);
        binding.inputVenueAddress.setVisibility(View.GONE);
        binding.inputVenueName.setVisibility(View.GONE);
        binding.inputVenueDescription.setVisibility(View.GONE);
        binding.venueCategories.setVisibility(View.GONE);

        PublicMethod.enableDefaultAnimation(binding.layoutVenueName);
        PublicMethod.enableDefaultAnimation(binding.layoutAddress);
        PublicMethod.enableDefaultAnimation(binding.layoutDescription);
        PublicMethod.enableDefaultAnimation(binding.layoutCategories);

        setViewStepper(binding.viewStepper1, binding.layoutCoverPhoto);
        setViewStepper(binding.viewStepper2, binding.layoutAddress);
        setViewStepper(binding.viewStepper3, binding.layoutVenueName);
        setViewStepper(binding.viewStepper4, binding.layoutDescription);

        binding.title.setSelected(true);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);
        }*/

        ViewGroup.LayoutParams view = binding.viewToolbar.getLayoutParams();
        view.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(view);

        if (createSubvenue != null){
            binding.title.setText(mContext.getResources().getString(R.string.createSubVenue) + " " + venueName);
        } else {
            binding.title.setText(R.string.addVenue);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void openDialogVenueOrSubvenue() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.createVenueOrSubvenue);
        builder.setMessage(R.string.createWhat);
        builder.setCancelable(false);

        builder.setPositiveButton(R.string.venue, (dialog, which) -> dialog.cancel());

        builder.setNegativeButton(R.string.subVenue, (dialog, which) -> {
            FragmentPickVenue fragmentPickVenue = new FragmentPickVenue();
            Bundle bundle = new Bundle();
            bundle.putString("openFor", "Venue");
            bundle.putString("brandID", brandID);
            fragmentPickVenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickVenue).commit();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void setDefaultLayout() {
        binding.textPickLocation.setVisibility(View.GONE);
        binding.inputVenueAddress.setText(intentVenueAddress);
    }

    private void showDialogLoading() {
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void saveImage(final String venuePhone, final String venuCategories, final String venueAddress, final String venueUsername, final String venueID, final String venueName, final String venueDescription, final String venueFacebook, final String venueTwitter,
                           final String venueInstagram, final String venueWhatsapp, final String venueWebsite, final String venueEmail) {
        switch (imageHeaderFrom){
            case "camera":
                StorageReference storageReference = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(venueID);
                UploadTask uploadGambarBarang = storageReference.putFile(Uri.fromFile(new File(mCurrentPhotoPath)));

                uploadGambarBarang.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        if (createSubvenue != null){
                            saveSubvenue(venueID, venueName, venueDescription, venuePhone, venuCategories, venueUsername, venueFacebook,
                                    venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, downloadUri.toString());
                        } else {
                            saveVenue(venueID, venueName, venueDescription, venuePhone, venuCategories, venueAddress, venueUsername, venueFacebook,
                                    venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, downloadUri.toString());
                        }
                    } else {
                        Snackbar.make(getView(), R.string.failedToUpload, Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

            case "storage":
                StorageReference storageReferenceVenue = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(venueID);
                UploadTask uploadGambarBarangVenue = storageReferenceVenue.putFile(selectedImageUri);

                uploadGambarBarangVenue.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReferenceVenue.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        if (createSubvenue != null){
                            saveSubvenue(venueID, venueName, venueDescription, venuePhone, venuCategories, venueUsername, venueFacebook,
                                    venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, downloadUri.toString());
                        } else {
                            saveVenue(venueID, venueName, venueDescription, venuePhone, venuCategories, venueAddress, venueUsername, venueFacebook,
                                    venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, downloadUri.toString());
                        }
                    } else {
                        Snackbar.make(getView(), R.string.failedToUpload, Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

                default:
                    //memasukan gambar ke firebase storage
                    binding.imageHeader.setDrawingCacheEnabled(true);
                    binding.imageHeader.buildDrawingCache();
                    Bitmap bitmap3 = binding.imageHeader.getDrawingCache();
                    ByteArrayOutputStream baos3 = new ByteArrayOutputStream();
                    bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, baos3);
                    byte[] data3 = baos3.toByteArray();

                    StorageReference storageReference3 = FirebaseStorage.getInstance().getReference("Venue").child(venueID).child(venueID);
                    UploadTask uploadGambarBarang3 = storageReference3.putBytes(data3);

                    uploadGambarBarang3.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return storageReference3.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();

                            if (createSubvenue != null){
                                saveSubvenue(venueID, venueName, venueDescription, venuePhone, venuCategories, venueUsername, venueFacebook,
                                        venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, downloadUri.toString());
                            } else {
                                saveVenue(venueID, venueName, venueDescription, venuePhone, venuCategories, venueAddress, venueUsername, venueFacebook,
                                        venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, downloadUri.toString());
                            }
                        } else {
                            Snackbar.make(getView(), R.string.failedToUpload, Snackbar.LENGTH_SHORT).show();
                        }
                    });
                    break;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "com.kotalogue.android_business",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void saveVenue(String venueID, String venueName, String venueDescription, String venuePhone, String venueCategories, String venueAddress,
                           String venueUsername, String venueFacebook, String venueTwitter, String venueInstagram, String venueWhatsapp,
                           String venueWebsite, String venueEmail, String venueHeaderPhoto) {

        ModelVenue modelVenue = new ModelVenue(listOfTag, "", brandID, userID, venueID, "", venueCategories, venueUsername, venueName, venueAddress,
                venuePhone, venueHeaderPhoto, venueDescription, venueLatLon, venueFacebook, venueTwitter, venueInstagram, venueWhatsapp,
                venueWebsite, venueEmail, "0,0", "0,0", "0,0", "0,0", "0,0", "0,0", "0,0", false);

        //map collection venue
        Map<String, String> map = new HashMap<>();
        map.put("venueID", venueID);

        //map username
        Map<String, String> mapUsername = new HashMap<>();
        mapUsername.put("venueUsername", venueUsername);

        fs.collection("Venue").document(venueID).set(modelVenue).addOnCompleteListener(task -> {
            if (task.isComplete()){
                Toast.makeText(getContext(), "Succesfully add venue, redirecting to detail", Toast.LENGTH_SHORT).show();

                dialogLoading.dismiss();

                intentToDetailVenue(venueID);
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    dialogLoading.dismiss();

                    Snackbar.make(getView(), "Error: " + task.getException().getMessage(), Snackbar.LENGTH_LONG).setAction(R.string.retry, view -> {
                        saveVenue(venueID, venueName, venueDescription, venuePhone, venueCategories, venueAddress, venueUsername, venueFacebook,
                                venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, venueHeaderPhoto);
                    }).show();
                } else {
                    dialogLoading.dismiss();

                    Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
                }
            }
        });

        fs.collection("Brands").document(brandID).collection("CollectionVenue"+brandID).document(venueID).set(map);

        fs.collection("VenueUsername").document(venueID).set(mapUsername);
    }

    private void intentToDetailVenue(String venueID) {
        if (createSubvenue != null){
            FragmentVenueInformation fragmentVenueInformation = new FragmentVenueInformation();
            Bundle bundle = new Bundle();
            bundle.putString("venueIDParent", venueIDParent);
            bundle.putString("venueID", venueID);
            bundle.putString("userID", FirebaseAuth.getInstance().getCurrentUser().getUid());
            bundle.putString("openSubvenue", "openSubvenue");
            bundle.putString("brandID", brandID);
            fragmentVenueInformation.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentVenueInformation).commit();
        } else {
            FragmentVenueInformation fragmentVenueInformation = new FragmentVenueInformation();
            Bundle bundle = new Bundle();
            bundle.putString("venueID", venueID);
            bundle.putString("userID", FirebaseAuth.getInstance().getCurrentUser().getUid());
            bundle.putString("brandID", brandID);
            fragmentVenueInformation.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentVenueInformation).commit();
        }
    }

    private void saveSubvenue(String venueID, String venueName, String venueDescription, String venuePhone, String venueCategories,
                              String venueUsername, String venueFacebook, String venueTwitter, String venueInstagram, String venueWhatsapp,
                              String venueWebsite, String venueEmail, String venueHeaderPhoto){

        ModelVenue modelVenue = new ModelVenue(listOfTag, "", brandID, userID, venueID, venueIDParent, venueCategories, venueUsername, venueName, intentVenueAddress,
                venuePhone, venueHeaderPhoto, venueDescription, intentVenueLatLon, venueFacebook, venueTwitter, venueInstagram, venueWhatsapp,
                venueWebsite, venueEmail, "0,0", "0,0", "0,0", "0,0", "0,0", "0,0", "0,0", false);

        Map<String, String> map = new HashMap<>();
        map.put("venueID", venueID);

        //save username to collection venue username
        Map<String, String> mapUsername = new HashMap<>();
        mapUsername.put("venueUsername", venueUsername);

        //save whole data to Collection Venue
        fs.collection("Venue").document(venueID).set(modelVenue).addOnCompleteListener(task -> {
            if (task.isComplete()){
                Toast.makeText(getContext(), "Succesfully add venue, redirecting to detail", Toast.LENGTH_SHORT).show();

                dialogLoading.dismiss();

                intentToDetailVenue(venueID);
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    dialogLoading.dismiss();

                    Snackbar.make(getView(), "Error: " + task.getException().getMessage(), Snackbar.LENGTH_LONG).setAction(R.string.retry, view -> {
                        saveSubvenue(venueID, venueName, venueDescription, venuePhone, venueCategories, venueUsername, venueFacebook,
                                venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail, venueHeaderPhoto);
                    }).show();
                } else {
                    dialogLoading.dismiss();

                    Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
                }
            }
        });

        //put this venueID to the venue parent document
        fs.collection("Venue").document(venueIDParent).collection("CollectionSubVenue").document(venueID).set(map);

        //input venue ID to collection brands
        fs.collection("Brands").document(brandID).collection("CollectionVenue"+brandID).document(venueID).set(map);

        //save venue username
        fs.collection("VenueUsername").document(venueID).set(mapUsername);
    }

    private void getWidthHeight(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(
                    getContext().getContentResolver().openInputStream(uri),
                    null,
                    options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        double imageHeight = options.outHeight;
        double imageWidth = options.outWidth;
        double finalValue = imageWidth / imageHeight;

        if (finalValue >= 0.4 && finalValue <= 2){
            if (imageHeight < 400 || imageWidth < 400){
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
            } else {
                Glide.with(mContext.getApplicationContext()).load(uri).into(binding.imageHeader);
            }
        } else if (finalValue < 0.4){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooHeight), Toast.LENGTH_SHORT).show();
        } else if (finalValue > 2){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooWidth), Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case REQUEST_IMAGE_CAPTURE:
                    imageHeaderFrom = "camera";
                    Bundle extras = data.getExtras();
                    photoBitmap = (Bitmap) extras.get("data");
                    binding.imageHeader.setImageBitmap(photoBitmap);

                    //focus to address
                    binding.stepper1.setVisibility(View.GONE);
                    binding.imageStepper1.setVisibility(View.VISIBLE);
                    binding.inputVenueAddress.setVisibility(View.VISIBLE);
                    binding.textPickLocation.setVisibility(View.VISIBLE);
                    setViewStepper(binding.viewStepper2, binding.layoutAddress);
                    break;

                case SELECT_PICTURE:
                    imageHeaderFrom = "storage";
                    selectedImageUri = data.getData();
                    if (null != selectedImageUri) {
                        getWidthHeight(selectedImageUri);

                        binding.stepper1.setVisibility(View.GONE);
                        binding.imageStepper1.setVisibility(View.VISIBLE);
                        binding.inputVenueAddress.setVisibility(View.VISIBLE);
                        binding.textPickLocation.setVisibility(View.VISIBLE);
                        setViewStepper(binding.viewStepper2, binding.layoutAddress);
                    }
                    break;

                case PLACE_PICKER_REQUEST:
                    Place place = PlacePicker.getPlace(getContext(), data);
                    venueLatLon = place.getLatLng().toString().substring(10).replace(")", "");
                    binding.inputVenueAddress.setText(place.getAddress().toString());
                    setViewStepper(binding.viewStepper2, binding.layoutAddress);
                    binding.inputVenueName.requestFocus();
                    break;
            }
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needToBackPressHandled;
    }

    @Override
    public void onBackPress() {
        openDialogExit();
    }

    private void openDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view  = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_change_venue, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView yes = view.findViewById(R.id.yes);
        TextView stay = view.findViewById(R.id.stayHere);
        TextView changeVenue = view.findViewById(R.id.changeVenue);
        View divider2 = view.findViewById(R.id.divider2);

        //check this is create event or sub event
        if (specificVenue != null) {
            divider2.setVisibility(View.GONE);
            changeVenue.setVisibility(View.GONE);
        } else if (createSubvenue != null){
            divider2.setVisibility(View.VISIBLE);
            changeVenue.setVisibility(View.VISIBLE);
        } else {
            divider2.setVisibility(View.GONE);
            changeVenue.setVisibility(View.GONE);
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        yes.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            needToBackPressHandled = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        stay.setOnClickListener(view1 -> alertDialog.dismiss());

        changeVenue.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            needToBackPressHandled = false;

            FragmentPickVenue fragmentPickVenue = new FragmentPickVenue();
            Bundle bundle = new Bundle();
            bundle.putString("openFor", "Venue");
            bundle.putString("brandID", brandID);
            fragmentPickVenue.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickVenue).commit();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });
    }
}
