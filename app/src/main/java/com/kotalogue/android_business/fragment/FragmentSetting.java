package com.kotalogue.android_business.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kotalogue.android_business.classHelper.Constants;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentSettingBinding;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSetting extends Fragment implements BackPressObserver {
    private FragmentSettingBinding binding;

    private SharedPreferences languangePref;
    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languangePref = getActivity().getSharedPreferences(Constants.USER_LOCALIZATION, Context.MODE_PRIVATE);

        // Inflate the layout for this fragment
        binding = FragmentSettingBinding.inflate(LayoutInflater.from(mContext), null, false);

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        userSetting();
        languangeListener();
        clickListener();
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(v -> getActivity().onBackPressed());
    }

    private void userSetting() {
        switch (languangePref.getString(Constants.LANGUANGE, "")){
            case "in":
                binding.indonesia.setChecked(true);
                break;

            case "en":
                binding.english.setChecked(true);
                break;

            default:
                binding.english.setChecked(true);
                break;
        }
    }

    private void languangeListener() {
        binding.indonesia.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                languangePref.edit().putString(Constants.LANGUANGE, "in").apply();

                changeLocalization(languangePref.getString(Constants.LANGUANGE, ""));

                Toast.makeText(mContext, mContext.getString(R.string.berhasilMengubahBahasa), Toast.LENGTH_SHORT).show();
            }
        });

        binding.english.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                languangePref.edit().putString(Constants.LANGUANGE, "en").apply();

                changeLocalization(languangePref.getString(Constants.LANGUANGE, ""));

                Toast.makeText(mContext, mContext.getString(R.string.successfullyChangeLanguange), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changeLocalization(String string) {
        Locale locale = new Locale(string);
        Locale.setDefault(locale);
        Configuration config = getActivity().getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return false;
    }

    @Override
    public void onBackPress() {
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}
