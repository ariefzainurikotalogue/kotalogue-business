package com.kotalogue.android_business.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelEvent;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentPickEventBinding;
import com.kotalogue.android_business.databinding.LayoutItemPickVenueBinding;

import java.util.ArrayList;
import java.util.InputMismatchException;

public class FragmentPickEvent extends Fragment implements AdapterPickEvent.AdapterPickEventInterface, BackPressObserver {
    FragmentPickEventBinding binding;
    private AdapterPickEvent adapterPickEvent;
    private ArrayList<ModelEvent> modelEvents = new ArrayList<>();
    private String brandID;
    private ArrayList<ModelEvent> filterList;
    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBundle();

        binding = FragmentPickEventBinding.inflate(LayoutInflater.from(mContext), null, false);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(binding.recyclerEvent, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        recyclerInit();

        loadListOfEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();

        binding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    filterList = new ArrayList<>();
                    for (ModelEvent modelEvent : modelEvents){
                        String venueName = modelEvent.getEventName().toLowerCase();
                        if (venueName.contains(charSequence)){
                            filterList.add(modelEvent);
                        }
                    }

                    adapterPickEvent.setFilter(filterList);
                } catch (InputMismatchException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> getActivity().onBackPressed());
    }

    private void loadListOfEvent() {
        FirebaseFirestore.getInstance().collection("Event").whereEqualTo("hasPass", false).get().addOnSuccessListener(queryDocumentSnapshots -> {
           modelEvents.clear();

           for (DocumentSnapshot document : queryDocumentSnapshots){
               ModelEvent modelEvent = document.toObject(ModelEvent.class);
               modelEvents.add(modelEvent);
           }

           adapterPickEvent.updateData(modelEvents);
        });
    }

    private void getBundle() {
        Bundle bundle = this.getArguments();
        if (bundle != null){
            brandID = bundle.getString("brandID");
        }
    }

    private void recyclerInit() {
        adapterPickEvent = new AdapterPickEvent(modelEvents, getContext(), this);

        binding.recyclerEvent.setHasFixedSize(true);
        binding.recyclerEvent.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerEvent.setAdapter(adapterPickEvent);
    }

    @Override
    public void itemClick(ArrayList<ModelEvent> modelEvents, int position) {
        FragmentAddEvent fragmentAddEvent = new FragmentAddEvent();
        Bundle bundle = new Bundle();
        bundle.putString("createSubevent", "Create Subevent");
        bundle.putString("eventID", modelEvents.get(position).getEventID());
        bundle.putString("eventLatLon", modelEvents.get(position).getEventLatLon());
        bundle.putString("eventAddress", modelEvents.get(position).getEventAddress());
        bundle.putString("eventName", modelEvents.get(position).getEventName());
        bundle.putString("brandID", brandID);
        bundle.putString("venueID", modelEvents.get(position).getVenueID());
        fragmentAddEvent.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentAddEvent).commit();
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return false;
    }

    @Override
    public void onBackPress() {
        getActivity().onBackPressed();
        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
    }
}

class AdapterPickEvent extends RecyclerView.Adapter<AdapterPickEvent.ViewHolder>{
    private ArrayList<ModelEvent> modelEvents;
    private Context context;
    private AdapterPickEventInterface adapterPickEventInterface;

    public AdapterPickEvent(ArrayList<ModelEvent> modelEvents, Context context, AdapterPickEventInterface adapterPickEventInterface) {
        this.modelEvents = modelEvents;
        this.context = context;
        this.adapterPickEventInterface = adapterPickEventInterface;
    }

    @NonNull
    @Override
    public AdapterPickEvent.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemPickVenueBinding binding = LayoutItemPickVenueBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPickEvent.ViewHolder holder, int position) {
        PublicMethod.enableDefaultAnimation(holder.binding.layoutContentMain);

        holder.binding.venueName.setSelected(true);

        holder.binding.venueName.setText(modelEvents.get(position).getEventName());
        holder.binding.venueAddress.setText(modelEvents.get(position).getEventAddress());

        holder.binding.layoutVenue.setOnClickListener(view -> adapterPickEventInterface.itemClick(modelEvents, position));
    }

    @Override
    public int getItemCount() {
        if (modelEvents != null){
            return modelEvents.size();
        } else {
            return 0;
        }
    }

    public void updateData(ArrayList<ModelEvent> modelEvents) {
        this.modelEvents = modelEvents;
        notifyDataSetChanged();
    }

    public void setFilter(ArrayList<ModelEvent> filterList) {
        modelEvents = new ArrayList<>();
        modelEvents.addAll(filterList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutItemPickVenueBinding binding;
        public ViewHolder(LayoutItemPickVenueBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface AdapterPickEventInterface{
        void itemClick(ArrayList<ModelEvent> modelEvents, int position);
    }
}
