package com.kotalogue.android_business.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.databinding.FragmentAddEventBinding;
import com.kotalogue.android_business.fragmentDetailEventFolder.FragmentEventInformation;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelEvent;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class FragmentAddEvent extends Fragment implements BackPressObserver, TimePickerFragment.TimePickerInterface {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int SELECT_PICTURE = 2;
    private FragmentAddEventBinding binding;
    private String venueAddress, venueLatLon, venueID, brandID;
    private Bitmap photoBitmap;
    private FirebaseFirestore fs = FirebaseFirestore.getInstance();
    private AlertDialog alertDialog;
    private String userID;

    //intent to create subevent
    private String intentEventID, intentEventLatLon, intentEventAddress, createSubevent, intentBrandID, intentVenueID;
    private Context mContext;
    private String imageHeaderFrom = "a";
    private Uri selectedImageUri;
    private String mCurrentPhotoPath;
    private AlertDialog dialogLoading;
    private boolean needToBackPressHandled = true;
    private RelativeLayout layoutLoading;
    private String eventName;
    private String venueName;
    private String specificVenue;
    private ArrayList<String> listOfTag = new ArrayList<>();
    private boolean isFirstTimeDescription = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            venueAddress = bundle.getString("venueAddress");
            venueLatLon = bundle.getString("venueLatLon");
            venueID = bundle.getString("venueID");
            brandID = bundle.getString("brandID");
            eventName = bundle.getString("eventName");
            venueName = bundle.getString("venueName");
            specificVenue = bundle.getString("specificVenue");

            //get intent to create sub event
            intentEventID = bundle.getString("eventID");
            intentEventLatLon = bundle.getString("eventLatLon");
            intentEventAddress = bundle.getString("eventAddress");
            createSubevent = bundle.getString("createSubevent");
            intentBrandID = bundle.getString("brandID");
            intentVenueID = bundle.getString("venueID");
        }

        // Inflate the layout for this fragment
        binding = FragmentAddEventBinding.inflate(LayoutInflater.from(mContext), null, false);

        preLoadView();

        dialogInit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return binding.getRoot();
    }

    private void setViewStepper(View viewStepper, ViewGroup layout){
        ViewGroup.LayoutParams viewParam = viewStepper.getLayoutParams();
        viewParam.height = PublicMethod.getViewHeight(layout) - PublicMethod.dp(R.dimen.dp40, mContext);
        viewStepper.setLayoutParams(viewParam);
    }

    private void preLoadView() {
        binding.eventStartDate.setSelected(true);
        binding.eventFinishDate.setSelected(true);
        binding.title.setSelected(true);

        PublicMethod.enableDefaultAnimation(binding.layoutEventName);
        PublicMethod.enableDefaultAnimation(binding.layoutDeskripsi);
        PublicMethod.enableDefaultAnimation(binding.layoutDate);
        PublicMethod.enableDefaultAnimation(binding.layoutTime);
        PublicMethod.enableDefaultAnimation(binding.layoutPrice);
        PublicMethod.enableDefaultAnimation(binding.layoutEventCategories);

        binding.buttonSave.setVisibility(View.GONE);

        binding.inputEventName.setVisibility(View.GONE);
        binding.inputEventDescription.setVisibility(View.GONE);
        binding.layoutStartDate.setVisibility(View.GONE);
        binding.layoutFinishDate.setVisibility(View.GONE);
        binding.layoutStartTime.setVisibility(View.GONE);
        binding.layoutFinishTime.setVisibility(View.GONE);
        binding.eventPrice.setVisibility(View.GONE);
        binding.eventCategories.setVisibility(View.GONE);

        ViewGroup.LayoutParams view = binding.viewToolbar.getLayoutParams();
        view.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(view);

        setViewStepper(binding.viewStepper1, binding.layoutCoverPhoto);
        setViewStepper(binding.viewStepper2, binding.layoutEventName);
        setViewStepper(binding.viewStepper3, binding.layoutDeskripsi);
        setViewStepper(binding.viewStepper4, binding.layoutDate);
        setViewStepper(binding.viewStepper5, binding.layoutTime);
        setViewStepper(binding.viewStepper6, binding.layoutPrice);

        //check create sub event or event
        if (createSubevent != null){
            binding.title.setText(mContext.getResources().getString(R.string.createSubEvent) + " " + eventName);
        } else {
            binding.title.setText(mContext.getResources().getString(R.string.addEvent) + " " + venueName);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        new Handler().postDelayed(() -> {
            if (venueName != null){
                Snackbar.make(getView(), "You pick " + venueName + " to your venues event", Snackbar.LENGTH_SHORT).show();
            }
        }, 500);

        clickListener();

        listener();
    }

    private void listener() {
        binding.inputEventDescription.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (!isFirstTimeDescription) {
                ViewGroup.LayoutParams stepperParam = binding.viewStepper3.getLayoutParams();
                stepperParam.height = PublicMethod.getViewHeight(binding.inputEventDescription) + PublicMethod.dp(R.dimen.dp37, mContext);
                binding.viewStepper3.setLayoutParams(stepperParam);
            }
        });

        binding.eventPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int numberChar = s.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper6.setVisibility(View.GONE);
                    binding.imageStepper6.setVisibility(View.VISIBLE);
                    binding.eventCategories.setVisibility(View.VISIBLE);
                    PublicMethod.setMargins(binding.eventCategories, PublicMethod.dp(R.dimen.dp15, mContext),
                            0, PublicMethod.dp(R.dimen.dp15, mContext),
                                PublicMethod.getViewHeight(binding.buttonSave) + PublicMethod.dp(R.dimen.dp15, mContext));
                } else {
                    binding.stepper6.setVisibility(View.VISIBLE);
                    binding.imageStepper6.setVisibility(View.GONE);
                    binding.eventCategories.setVisibility(View.GONE);
                    PublicMethod.setMargins(binding.eventCategories, PublicMethod.dp(R.dimen.dp15, mContext),
                            0, PublicMethod.dp(R.dimen.dp15, mContext), 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.inputEventDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int numberChar = s.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper3.setVisibility(View.GONE);
                    binding.imageStepper3.setVisibility(View.VISIBLE);
                    binding.layoutStartDate.setVisibility(View.VISIBLE);
                    binding.layoutFinishDate.setVisibility(View.VISIBLE);
                } else {
                    binding.stepper3.setVisibility(View.VISIBLE);
                    binding.imageStepper3.setVisibility(View.GONE);
                    binding.layoutStartDate.setVisibility(View.GONE);
                    binding.layoutFinishDate.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.inputEventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int numberChar = s.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper2.setVisibility(View.GONE);
                    binding.imageStepper2.setVisibility(View.VISIBLE);
                    binding.inputEventDescription.setVisibility(View.VISIBLE);

                    isFirstTimeDescription = false;
                    ViewGroup.LayoutParams stepperParam = binding.viewStepper3.getLayoutParams();
                    stepperParam.height = PublicMethod.getViewHeight(binding.inputEventDescription);
                    binding.viewStepper3.setLayoutParams(stepperParam);
                } else {
                    binding.stepper2.setVisibility(View.VISIBLE);
                    binding.imageStepper2.setVisibility(View.GONE);
                    binding.inputEventDescription.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void clickListener() {
        binding.eventPrice.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (binding.eventPrice.getText().toString().trim().length() == 0){
                    Toast.makeText(mContext, "Please write your event price.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    PublicMethod.hideKeyboard(getActivity());
                    PublicMethod.focusToView(binding.eventCategories);
                    return true;
                }
            }
            return true;
        });

        binding.iconBack.setOnClickListener(v -> getActivity().onBackPressed());

        binding.eventCategories.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(mContext, binding.eventCategories);
            popupMenu.getMenuInflater().inflate(R.menu.menu_categories, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.food:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.foodBeverages);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.shopping:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.shoppingCenters);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.automotive:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.automotive);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.fashion:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.fashion);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.health:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.health);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.electronics:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.electronics);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.houseHolds:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.houseHold);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.souvenirs:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.souvenir);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.tourist:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.tourist);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;

                    case R.id.emergencies:
                        checkStepper7();
                        binding.eventCategories.setText(R.string.emergencies);
                        binding.buttonSave.setVisibility(View.VISIBLE);
                        break;
                }
                return false;
            });

            popupMenu.show();
        });

        binding.layoutChangePhoto.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(mContext, binding.imageHeader);
            popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.camera:
                        dispatchTakePictureIntent();
                        break;

                    case R.id.storage:
                        Intent intentStorage = new Intent();
                        intentStorage.setType("image/*");
                        intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE );
                        break;
                }
                return false;
            });
            popupMenu.show();
        });

        binding.buttonSave.setOnClickListener(v -> {
            if (PublicMethod.isDeviceOnline(mContext)){
                final String eventName = binding.inputEventName.getText().toString();
                final String eventDescription = binding.inputEventDescription.getText().toString();
                final String eventID = UUID.randomUUID().toString();
                final String eventCategories = binding.eventCategories.getText().toString();

                if (eventName.isEmpty()){
                    Toast.makeText(mContext, "Please fill event name first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (eventDescription.isEmpty()){
                    Toast.makeText(mContext, "Please fill event description first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (binding.eventStartDate.getText().toString().equals(getResources().getString(R.string.dateStandart))) {
                    Toast.makeText(mContext, "Please fill event start date first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (binding.eventFinishDate.getText().toString().equals(getResources().getString(R.string.dateStandart))) {
                    Toast.makeText(mContext, "Please fill event finish date first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (binding.eventStartTime.getText().toString().equals(getResources().getString(R.string.eventStartAt))) {
                    Toast.makeText(mContext, "Please fill event start time first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (binding.eventFinishTime.getText().toString().equals(getResources().getString(R.string.eventFinishAt))){
                    Toast.makeText(mContext, "Please fill event finish time first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (binding.eventPrice.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please fill event price first", Toast.LENGTH_SHORT).show();
                    return;
                } else if (eventCategories.equals(getResources().getString(R.string.tapToChooseCategories))){
                    Toast.makeText(mContext, "Choose your event category first", Toast.LENGTH_SHORT).show();
                    return;
                }

                String eventTimeStart = String.valueOf(PublicMethod.dateStringToLong(binding.eventStartTime.getText().toString(), "kk:mm"));
                String eventTimeFinish = String.valueOf(PublicMethod.dateStringToLong(binding.eventFinishTime.getText().toString(), "kk:mm"));
                String eventStartFinishTime = eventTimeStart + "," + eventTimeFinish;
                long eventStartDate = PublicMethod.dateStringToLong(binding.eventStartDate.getText().toString(), "MMMM dd, yyyy");
                long eventFinishDate = PublicMethod.dateStringToLong(binding.eventFinishDate.getText().toString(), "MMMM dd, yyyy");
                int eventPrice = Integer.valueOf(binding.eventPrice.getText().toString());

                //show the alertdialog to loading
                showDialogLoading();

                saveImageHeader(eventCategories, eventID, eventName, eventDescription, eventStartDate,
                        eventFinishDate, eventPrice, eventStartFinishTime, "", "", "", "", "", "");
            } else {
                Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
            }
        });

        binding.eventFinishTime.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.eventFinishTime, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });

        binding.eventStartTime.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.eventStartTime, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });

        binding.eventFinishDate.setOnClickListener(v -> openDatePicker("FinishDate"));

        binding.eventStartDate.setOnClickListener(v -> openDatePicker("StartDate"));

        binding.inputEventName.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (binding.inputEventName.getText().toString().trim().length() == 0){
                    Toast.makeText(mContext, "Please write your event name first.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    binding.inputEventDescription.requestFocus();
                    return true;
                }
            }
            return true;
        });

        binding.skip.setOnClickListener(v -> {
            binding.stepper1.setVisibility(View.GONE);
            binding.imageStepper1.setVisibility(View.VISIBLE);
            binding.inputEventName.setVisibility(View.VISIBLE);
            binding.inputEventName.requestFocus();
            setViewStepper(binding.viewStepper2, binding.layoutEventName);
        });
    }

    private void checkStepper7() {
        binding.stepper7.setVisibility(View.GONE);
        binding.imageStepper7.setVisibility(View.VISIBLE);
    }

    private void dialogInit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.setCancelable(false);
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading() {
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void openDatePicker(final String openFrom) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.dialog_date_picker, null);
        builder.setView(view);

        CalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setDate(Calendar.getInstance().getTimeInMillis());
        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            switch (openFrom){
                case "StartDate":
                    String newMonth = String.valueOf(month+1);
                    String newDay = String.valueOf(dayOfMonth);
                    long longDate = PublicMethod.dateStringToLong(newMonth + "/" + newDay + "/" + String.valueOf(year), "M/d/yyyy");
                    binding.eventStartDate.setText(PublicMethod.dateLongToString(longDate, "MMMM dd, yyyy", mContext));
                    alertDialog.dismiss();
                    break;

                case "FinishDate":
                    String finishDay = String.valueOf(dayOfMonth);
                    String finishMonth = String.valueOf(month+1);
                    long finishLongDate = PublicMethod.dateStringToLong(finishMonth + "/" + finishDay + "/" + String.valueOf(year), "M/d/yyyy");
                    binding.eventFinishDate.setText(PublicMethod.dateLongToString(finishLongDate, "MMMM dd, yyyy", mContext));
                    alertDialog.dismiss();

                    //open time layout
                    binding.stepper4.setVisibility(View.GONE);
                    binding.imageStepper4.setVisibility(View.VISIBLE);
                    binding.layoutStartTime.setVisibility(View.VISIBLE);
                    binding.layoutFinishTime.setVisibility(View.VISIBLE);
                    setViewStepper(binding.viewStepper5, binding.layoutTime);

                    break;
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    private void saveImageHeader(final String eventCategories, final String eventID, final String eventName, final String eventDesc, final long eventStartDate, final long eventEndDate, final int eventPrice, final String eventTime,
                                 final String eventFacebook, final String eventTwitter, final String eventInstagram, final String eventWhatsapp, final String eventWebsite, final String eventEmail){
        switch (imageHeaderFrom){
            case "storage":
                StorageReference storageReference = FirebaseStorage.getInstance().getReference("Event").child(eventID).child(eventID);
                UploadTask uploadGambarBarang = storageReference.putFile(selectedImageUri);

                uploadGambarBarang.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        if (createSubevent != null){
                            saveSubevent(eventCategories, eventID, eventName, downloadUri.toString(), eventDesc, eventStartDate, eventEndDate, eventPrice, eventTime,
                                    eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail);
                        } else {
                            saveEvent(eventCategories, eventID, eventName, downloadUri.toString(), eventDesc, eventStartDate, eventEndDate, eventPrice, eventTime,
                                    eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail);
                        }
                    } else {
                        Toast.makeText(mContext, "Failed to upload data", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case "camera":
                StorageReference storageReference2 = FirebaseStorage.getInstance().getReference("Event").child(eventID).child(eventID);
                UploadTask uploadGambarBarang2 = storageReference2.putFile(Uri.fromFile(new File(mCurrentPhotoPath)));

                uploadGambarBarang2.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference2.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        if (createSubevent != null){
                            Log.d("Save as Subevent:", "true");
                            saveSubevent(eventCategories, eventID, eventName, downloadUri.toString(), eventDesc, eventStartDate, eventEndDate, eventPrice, eventTime,
                                    eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail);
                        } else {
                            Log.d("Save as Event:", "true");
                            saveEvent(eventCategories, eventID, eventName, downloadUri.toString(), eventDesc, eventStartDate, eventEndDate, eventPrice, eventTime,
                                    eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail);
                        }
                    } else {
                        Toast.makeText(mContext, "Failed to upload data", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

                default:
                    //memasukan gambar ke firebase storage
                    binding.imageHeader.setDrawingCacheEnabled(true);
                    binding.imageHeader.buildDrawingCache();
                    Bitmap bitmap3 = binding.imageHeader.getDrawingCache();
                    ByteArrayOutputStream baos3 = new ByteArrayOutputStream();
                    bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, baos3);
                    byte[] data3 = baos3.toByteArray();

                    StorageReference storageReference3 = FirebaseStorage.getInstance().getReference("Event").child(eventID).child(eventID);
                    UploadTask uploadGambarBarang3 = storageReference3.putBytes(data3);

                    uploadGambarBarang3.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return storageReference3.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();

                            if (createSubevent != null){
                                Log.d("Save as Subevent:", "true");
                                saveSubevent(eventCategories, eventID, eventName, downloadUri.toString(), eventDesc, eventStartDate, eventEndDate, eventPrice, eventTime,
                                        eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail);
                            } else {
                                Log.d("Save as Event:", "true");
                                saveEvent(eventCategories, eventID, eventName, downloadUri.toString(), eventDesc, eventStartDate, eventEndDate, eventPrice, eventTime,
                                        eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail);
                            }
                        } else {
                            Toast.makeText(mContext, "Failed to upload data", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "com.kotalogue.android_business",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void saveEvent(String eventCategories, String eventID, String eventName, String eventPhoto, String eventDesc, long eventStartDate, long eventEndDate, int eventPrice, String eventTime,
                           String eventFacebook, String eventTwitter, String eventInstagram, String eventWhatsapp, String eventWebsite, String eventEmail){

        ModelEvent modelEvent = new ModelEvent(listOfTag, brandID, userID, eventID, "", venueID, eventName, eventCategories, eventPhoto, eventDesc, venueAddress, venueLatLon, eventTime, eventFacebook,
                eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail, 0, 0, 0, eventPrice, false, false, false, eventStartDate, eventEndDate);

        Map<String, String> mapEventID = new HashMap<>();
        mapEventID.put("eventID", eventID);

        saveEventWholeData(eventID, eventPhoto, modelEvent, mapEventID);
    }

    private void saveEventWholeData(String eventID, String eventPhoto, ModelEvent modelEvent, Map<String, String> mapEventID) {
        //put the id event to venue collection
        fs.collection("Venue").document(venueID).collection("CollectionEvent").document(eventID).set(mapEventID);

        //put the id event to brands collection
        fs.collection("Brands").document(brandID).collection("CollectionEvent"+brandID).document(eventID).set(mapEventID);
        
        fs.collection("Event").document(eventID).set(modelEvent).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                dialogLoading.dismiss();
                Toast.makeText(mContext, R.string.successRegisterEvent, Toast.LENGTH_SHORT).show();
                intentToDetailEvent(brandID, eventID, eventPhoto);
            }
        }).addOnFailureListener(e -> {
            if (PublicMethod.isDeviceOnline(mContext)){
                dialogLoading.dismiss();
                Toast.makeText(mContext, "Failed register your event, error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            } else {
                dialogLoading.dismiss();
                Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void intentToDetailEvent(String brandID, String eventID, String eventPhoto) {
        FragmentEventInformation fragmentEventInformation = new FragmentEventInformation();
        Bundle bundle = new Bundle();
        bundle.putString("eventID", eventID);
        bundle.putString("brandID", brandID);
        bundle.putString("eventHeaderPhoto", eventPhoto);
        fragmentEventInformation.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentEventInformation).commit();
    }

    private void saveSubevent(String eventCategories, String eventID, String eventName, String eventPhoto, String eventDesc, long eventStartDate, long eventEndDate, int eventPrice, String eventTime,
                              String eventFacebook, String eventTwitter, String eventInstagram, String eventWhatsapp, String eventWebsite, String eventEmail){

        Map<String, String> mapEventID = new HashMap<>();
        mapEventID.put("eventID", eventID);

        ModelEvent modelEvent = new ModelEvent(listOfTag, brandID, userID, eventID, intentEventID, intentVenueID, eventName, eventCategories, eventPhoto, eventDesc, intentEventAddress, intentEventLatLon, eventTime, eventFacebook,
                eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail, 0, 0, 0, eventPrice, false, false, false, eventStartDate, eventEndDate);

        saveSubEventWholeData(eventID, eventPhoto, modelEvent, mapEventID);
    }

    private void saveSubEventWholeData(String eventID, String eventPhoto, ModelEvent modelEvent, Map<String, String> mapEventID) {
        //put the id event to super event sub collection
        fs.collection("Event").document(intentEventID).collection("CollectionSubEvent").document(eventID).set(mapEventID);

        //put the id event to venue collection
        fs.collection("Venue").document(intentVenueID).collection("CollectionEvent").document(eventID).set(mapEventID);

        //put the id event to brands collection
        fs.collection("Brands").document(intentBrandID).collection("CollectionEvent"+intentBrandID).document(eventID).set(mapEventID);

        //put the whole data of event to event collection
        fs.collection("Event").document(eventID).set(modelEvent).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                dialogLoading.dismiss();
                Toast.makeText(mContext, R.string.successRegisterEvent, Toast.LENGTH_SHORT).show();
                intentToDetailEvent(brandID, eventID, eventPhoto);
            }
        }).addOnFailureListener(e -> {
            if (PublicMethod.isDeviceOnline(mContext)){
                dialogLoading.dismiss();
                Toast.makeText(mContext, "Failed register your event, error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            } else {
                dialogLoading.dismiss();
                Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getWidthHeight(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(
                    mContext.getContentResolver().openInputStream(uri),
                    null,
                    options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double imageHeight = options.outHeight;
        double imageWidth = options.outWidth;
        double finalValue = imageWidth / imageHeight;

        if (finalValue >= 0.4 && finalValue <= 2){
            if (imageHeight < 400 || imageWidth < 400){
                Toast.makeText(mContext, mContext.getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
            } else {
                Glide.with(mContext.getApplicationContext()).load(uri).into(binding.imageHeader);
            }
        } else if (finalValue < 0.4){
            Toast.makeText(mContext, mContext.getResources().getString(R.string.imageTooHeight), Toast.LENGTH_SHORT).show();
        } else if (finalValue > 2){
            Toast.makeText(mContext, mContext.getResources().getString(R.string.imageTooWidth), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case REQUEST_IMAGE_CAPTURE:
                    imageHeaderFrom = "camera";
                    Bundle extras = data.getExtras();
                    photoBitmap = (Bitmap) extras.get("data");
                    binding.imageHeader.setImageBitmap(photoBitmap);

                    //focus on event name
                    binding.stepper1.setVisibility(View.GONE);
                    binding.imageStepper1.setVisibility(View.VISIBLE);
                    binding.inputEventName.setVisibility(View.VISIBLE);
                    setViewStepper(binding.viewStepper2, binding.layoutEventName);
                    new Handler().postDelayed(() -> binding.inputEventName.requestFocus(), 200);
                    break;

                case SELECT_PICTURE:
                    imageHeaderFrom = "storage";
                    selectedImageUri = data.getData();
                    if (null != selectedImageUri) {
                        getWidthHeight(selectedImageUri);
                        //focus on event name
                        binding.stepper1.setVisibility(View.GONE);
                        binding.imageStepper1.setVisibility(View.VISIBLE);
                        binding.inputEventName.setVisibility(View.VISIBLE);
                        setViewStepper(binding.viewStepper2, binding.layoutEventName);
                        new Handler().postDelayed(() -> binding.inputEventName.requestFocus(), 200);
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needToBackPressHandled;
    }

    @Override
    public void onBackPress() { openDialogExit(); }

    private void openDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view  = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_change_venue, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView yes = view.findViewById(R.id.yes);
        TextView stay = view.findViewById(R.id.stayHere);
        TextView changeVenue = view.findViewById(R.id.changeVenue);
        View divider2 = view.findViewById(R.id.divider2);

        //check this is create event or sub event
        if (specificVenue != null) {
            divider2.setVisibility(View.GONE);
            changeVenue.setVisibility(View.GONE);
        } else if (createSubevent != null){
            changeVenue.setText(R.string.changeEvent);
            divider2.setVisibility(View.VISIBLE);
            changeVenue.setVisibility(View.VISIBLE);
        } else {
            divider2.setVisibility(View.VISIBLE);
            changeVenue.setVisibility(View.VISIBLE);
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        yes.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            needToBackPressHandled = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        stay.setOnClickListener(view1 -> alertDialog.dismiss());

        changeVenue.setOnClickListener(view1 -> {
            if (createSubevent != null){
                alertDialog.dismiss();

                needToBackPressHandled = false;

                FragmentPickEvent fragmentPickEvent = new FragmentPickEvent();
                Bundle bundle = new Bundle();
                bundle.putString("brandID", brandID);
                fragmentPickEvent.setArguments(bundle);

                getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickEvent).commit();
                new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
            } else {
                alertDialog.dismiss();

                needToBackPressHandled = false;

                FragmentPickVenue fragmentPickVenue = new FragmentPickVenue();
                Bundle bundle = new Bundle();
                bundle.putString("openFor", "Event");
                bundle.putString("brandID", brandID);
                fragmentPickVenue.setArguments(bundle);

                getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentPickVenue).commit();
                new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
            }
        });
    }

    @Override
    public void onTimeSet() {
        binding.stepper5.setVisibility(View.GONE);
        binding.imageStepper5.setVisibility(View.VISIBLE);
        binding.eventPrice.setVisibility(View.VISIBLE);
        setViewStepper(binding.viewStepper6, binding.layoutPrice);
        binding.eventPrice.requestFocus();
        binding.eventPrice.setFocusable(true);
        binding.eventPrice.setFocusableInTouchMode(true);
    }
}
