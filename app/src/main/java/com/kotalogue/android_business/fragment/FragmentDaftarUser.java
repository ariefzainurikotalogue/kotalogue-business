package com.kotalogue.android_business.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kotalogue.android_business.activity.StandartActivity;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.model.ModelUser;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.LayoutDaftarUserBinding;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDaftarUser extends Fragment {
    private RecyclerView recyclerView;
    private AppBarLayout appBarLayout;
    private View viewToolbar;
    private AdapterFragmentDaftarUser adapterFragmentDaftarUser;
    private ArrayList<ModelUser> modelUsers = new ArrayList<>();
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_daftar_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        appBarLayout = view.findViewById(R.id.appBarLayout);
        recyclerView = view.findViewById(R.id.rvDaftarUser);
        viewToolbar = view.findViewById(R.id.viewToolbar);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            PublicMethod.setMargins(recyclerView, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        viewToolbar.setLayoutParams(layoutParams);

        recyclerInit();
        loadUser();
    }

    private void loadUser() {
        FirebaseFirestore.getInstance().collection("User").whereEqualTo("userType", "Business").addSnapshotListener((documentSnapshots, e) -> {
            if (documentSnapshots != null){
                modelUsers.clear();

                for (DocumentSnapshot document : documentSnapshots){
                    ModelUser modelUser = document.toObject(ModelUser.class);
                    modelUsers.add(modelUser);
                }

                adapterFragmentDaftarUser.updateData(modelUsers);
            }
        });
    }

    private void recyclerInit() {
        adapterFragmentDaftarUser = new AdapterFragmentDaftarUser(modelUsers);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapterFragmentDaftarUser);
    }
}

class AdapterFragmentDaftarUser extends RecyclerView.Adapter<AdapterFragmentDaftarUser.ViewHolder>{
    private ArrayList<ModelUser> modelUsers;

    public AdapterFragmentDaftarUser(ArrayList<ModelUser> modelUsers) {
        this.modelUsers = modelUsers;
    }

    @NonNull
    @Override
    public AdapterFragmentDaftarUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutDaftarUserBinding binding = LayoutDaftarUserBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFragmentDaftarUser.ViewHolder holder, int position) {
        holder.binding.user.setText(modelUsers.get(position).getEmail());

        holder.binding.user.setOnClickListener(view -> {
            Intent intent = new Intent(holder.itemView.getContext(), StandartActivity.class);
            intent.putExtra("open", "OpenFragmentHome");
            intent.putExtra("userID", modelUsers.get(position).getUserID());
            intent.putExtra("email", modelUsers.get(position).getEmail());
            holder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return modelUsers.size();
    }

    public void updateData(ArrayList<ModelUser> modelUsers) {
        this.modelUsers = modelUsers;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LayoutDaftarUserBinding binding;
        public ViewHolder(@NonNull LayoutDaftarUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}