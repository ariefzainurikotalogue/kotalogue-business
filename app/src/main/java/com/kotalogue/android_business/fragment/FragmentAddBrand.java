package com.kotalogue.android_business.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.globalAdapter.AdapterTag;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.globalInterface.FragmentAddBrandInterface;
import com.kotalogue.android_business.model.ModelBrand;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentAddBrandBinding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

public class FragmentAddBrand extends Fragment implements BackPressObserver {
    //request code
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int SELECT_PICTURE = 2;

    //view
    private FragmentAddBrandBinding binding;
    private Bitmap photoBitmap;
    private ArrayList<String> listOfTag = new ArrayList<>();
    private String imageHeaderFrom = "a";
    private Uri selectedImageUri;
    private String mCurrentPhotoPath;
    private Context mContext;
    private AlertDialog dialogLoading;
    private boolean needToBackPressHandled = true;
    private RelativeLayout layoutLoading;
    private boolean isFirstTimeDescription = true;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = FragmentAddBrandBinding.inflate(LayoutInflater.from(mContext), null, false);

        //binding.setClick(fragmentAddBrandInterface);

        PublicMethod.enableDefaultAnimation(binding.layoutCoverPhoto);
        PublicMethod.enableDefaultAnimation(binding.layoutBrandName);
        PublicMethod.enableDefaultAnimation(binding.layoutBrandCategories);
        PublicMethod.enableDefaultAnimation(binding.layoutBrandDescription);

        binding.btnSend.setVisibility(View.GONE);

        //uncheck below code if you set translucent navigation bar to ture
        /*//check if device has navbar enabled or no
        if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navBarParam = binding.viewNavbar.getLayoutParams();
            navBarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navBarParam);
        }*/

        //set the margin of toolbar using empty view
        ViewGroup.LayoutParams toolbarParam = binding.viewToolbar.getLayoutParams();
        toolbarParam.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(toolbarParam);

        //set custom height
        setViewStepper(binding.viewStepper1, binding.layoutCoverPhoto);
        setViewStepper(binding.viewStepper2, binding.layoutBrandName);
        setViewStepper(binding.viewStepper3, binding.layoutBrandDescription);

        dialogInit();
    }

    private void setViewStepper(View viewStepper, ViewGroup layout){
        ViewGroup.LayoutParams viewParam = viewStepper.getLayoutParams();
        viewParam.height = PublicMethod.getViewHeight(layout) - PublicMethod.dp(R.dimen.dp40, mContext);
        viewStepper.setLayoutParams(viewParam);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        dialogLoading = builder.create();
        dialogLoading.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialogLoading.setCancelable(false);
    }

    private void showDialogLoading(){
        dialogLoading.show();
        dialogLoading.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
        listener();
    }

    private void listener() {
        binding.inputDescription.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (!isFirstTimeDescription) {
                ViewGroup.LayoutParams stepperParam = binding.viewStepper3.getLayoutParams();
                stepperParam.height = PublicMethod.getViewHeight(binding.inputDescription) + PublicMethod.dp(R.dimen.dp37, mContext);
                binding.viewStepper3.setLayoutParams(stepperParam);
            }
        });

        binding.inputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0){
                    binding.stepper2.setVisibility(View.GONE);
                    binding.imageStepper2.setVisibility(View.VISIBLE);
                    binding.inputDescription.setVisibility(View.VISIBLE);

                    isFirstTimeDescription = false;
                    ViewGroup.LayoutParams stepperParam = binding.viewStepper3.getLayoutParams();
                    stepperParam.height = PublicMethod.getViewHeight(binding.inputDescription);
                    binding.viewStepper3.setLayoutParams(stepperParam);
                } else {
                    binding.stepper2.setVisibility(View.VISIBLE);
                    binding.imageStepper2.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.inputDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int numberChar = charSequence.toString().trim().length();

                if (numberChar > 0) {
                    binding.stepper3.setVisibility(View.GONE);
                    binding.imageStepper3.setVisibility(View.VISIBLE);
                    binding.brandCategories.setVisibility(View.VISIBLE);
                    PublicMethod.setMargins(binding.brandCategories, PublicMethod.dp(R.dimen.dp15, mContext),
                            0, PublicMethod.dp(R.dimen.dp15, mContext), PublicMethod.getViewHeight(binding.btnSend)
                                    + PublicMethod.dp(R.dimen.dp15, mContext));
                } else {
                    binding.stepper3.setVisibility(View.VISIBLE);
                    binding.imageStepper3.setVisibility(View.GONE);
                    binding.brandCategories.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(v -> getActivity().onBackPressed());

        binding.inputName.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (binding.inputName.getText().toString().trim().length() == 0){
                    Toast.makeText(mContext, "Please write your brand name first.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    binding.inputDescription.requestFocus();
                    return true;
                }
            }
            return true;
        });

        binding.skip.setOnClickListener(view -> {
            binding.stepper1.setVisibility(View.GONE);
            binding.imageStepper1.setVisibility(View.VISIBLE);
            binding.inputName.setVisibility(View.VISIBLE);
            setViewStepper(binding.viewStepper2, binding.layoutBrandName);
        });

        binding.layoutChangePhoto.setOnClickListener(view -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), binding.imageHeader);
            popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.camera:
                        dispatchTakePictureIntent();
                        break;

                    case R.id.storage:
                        Intent intentStorage = new Intent();
                        intentStorage.setType("image/*");
                        intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE );
                        break;
                }
                return false;
            });

            popupMenu.show();
        });

        binding.btnSend.setOnClickListener(view -> doSaveBrand());

        binding.brandCategories.setOnClickListener(view -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), binding.brandCategories);
            popupMenu.getMenuInflater().inflate(R.menu.menu_categories, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.food:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.foodBeverages);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.shopping:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.shoppingCenters);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.automotive:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.automotive);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.fashion:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.fashion);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.health:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.health);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.electronics:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.electronics);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.houseHolds:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.houseHold);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.souvenirs:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.souvenir);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.tourist:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.tourist);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;

                    case R.id.emergencies:
                        showBrandDescription();
                        binding.brandCategories.setText(R.string.emergencies);
                        binding.btnSend.setVisibility(View.VISIBLE);
                        break;
                }
                return false;
            });

            popupMenu.show();
        });
    }

    private void showBrandDescription() {
        binding.stepper4.setVisibility(View.GONE);
        binding.imageStepper4.setVisibility(View.VISIBLE);
    }

    private FragmentAddBrandInterface fragmentAddBrandInterface = new FragmentAddBrandInterface() {
        @Override
        public void back() { getActivity().onBackPressed(); }

        @Override
        public void saveBrand() { doSaveBrand(); }

        @Override
        public void openCloseSocialMedia() {
        }

        @Override
        public void addTag() {
        }

        @Override
        public void addImage() {
            PopupMenu popupMenu = new PopupMenu(getContext(), binding.imageHeader);
            popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.camera:
                        dispatchTakePictureIntent();
                        break;

                    case R.id.storage:
                        Intent intentStorage = new Intent();
                        intentStorage.setType("image/*");
                        intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE );
                        break;
                }
                return false;
            });

            popupMenu.show();
        }

        @Override
        public void chooseCategories() {

        }
    };

    private void doSaveBrand() {
        final String brandName = binding.inputName.getText().toString().trim();
        final String brandDescription = binding.inputDescription.getText().toString().trim();
        final String brandID = UUID.randomUUID().toString().trim();
        final String brandCategories = binding.brandCategories.getText().toString().trim();

        if (brandName.isEmpty()){
            Toast.makeText(getContext(), "Please input your brand name first", Toast.LENGTH_SHORT).show();
            return;
        } else if (brandCategories.equals(getResources().getString(R.string.chooseCategories))){
            Toast.makeText(getContext(), "Please choose you brand categories first", Toast.LENGTH_SHORT).show();
            return;
        } else if (brandDescription.isEmpty()){
            Toast.makeText(getContext(), "Please input your brand description first", Toast.LENGTH_SHORT).show();
            return;
        }

        showDialogLoading();

        saveImage(brandCategories, "", brandID, brandName, brandDescription, "", "", "", "", "", "");
    }

    //method to save the image to the cloud storage
    private void saveImage(final String brandCategories, final String brandPhone, final String brandID, final String brandName, final String brandDescription, final String brandFacebook, final String brandTwitter,
                           final String brandInstagram, final String brandWhatsapp, final String brandWebsite, final String brandEmail) {

        switch (imageHeaderFrom){
            case "camera":
                StorageReference storageReference = FirebaseStorage.getInstance().getReference("Brand").child(brandID);
                UploadTask uploadGambarBarang = storageReference.putFile(Uri.fromFile(new File(mCurrentPhotoPath)));

                uploadGambarBarang.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        saveBrand(brandCategories, brandPhone, brandID, brandName, brandDescription, brandFacebook, brandTwitter, brandInstagram, brandWhatsapp, brandWebsite, brandEmail, downloadUri.toString());
                    } else {
                        Toast.makeText(getContext(), "Failed to save data, make sure you're connect to internet", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case "storage":
                StorageReference storageReferenceStorage = FirebaseStorage.getInstance().getReference("Brand").child(brandID);
                UploadTask uploadTask = storageReferenceStorage.putFile(selectedImageUri);

                uploadTask.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReferenceStorage.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        saveBrand(brandCategories, brandPhone, brandID, brandName, brandDescription, brandFacebook, brandTwitter, brandInstagram, brandWhatsapp, brandWebsite, brandEmail, downloadUri.toString());
                    } else {
                        Toast.makeText(getContext(), "Failed to save data, make sure you're connect to internet", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

                default:
                    //memasukan gambar ke firebase storage
                    binding.imageHeader.setDrawingCacheEnabled(true);
                    binding.imageHeader.buildDrawingCache();
                    Bitmap bitmap2 = ((BitmapDrawable) binding.imageHeader.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
                    byte[] data2 = baos2.toByteArray();

                    StorageReference storageReference2 = FirebaseStorage.getInstance().getReference("Brand").child(brandID);
                    UploadTask uploadGambarBarang2 = storageReference2.putBytes(data2);

                    uploadGambarBarang2.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return storageReference2.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();
                            saveBrand(brandCategories, brandPhone, brandID, brandName, brandDescription, brandFacebook, brandTwitter, brandInstagram, brandWhatsapp, brandWebsite, brandEmail, downloadUri.toString());
                        } else {
                            Toast.makeText(getContext(), "Failed to save data, make sure you're connect to internet", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "com.kotalogue.android_business",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void testUpdate(){

    }

    //method to save the brand to the firestore database, and algolia
    private void saveBrand(String brandCategories, String brandPhone, String brandID, String brandName, String brandDescription, String brandFacebook,
                           String brandTwitter, String brandInstagram, String brandWhatsapp, String brandWebsite, String brandEmail, String url) {

        ModelBrand modelBrand = new ModelBrand(listOfTag, brandID, "", brandCategories, brandName, url, brandPhone, brandDescription, brandFacebook, brandTwitter,
                brandInstagram, brandWhatsapp, brandWebsite, brandEmail, FirebaseAuth.getInstance().getCurrentUser().getUid(), false, false);

        Map<String, String> map = new HashMap<>();
        map.put("brandID", brandID);

        //send data to algolia
        FirebaseFirestore.getInstance().collection("Brands").document(brandID).set(modelBrand).addOnCompleteListener(task -> {
            if (task.isComplete()){
                dialogLoading.dismiss();

                Toast.makeText(getContext(), "Succesfully register your brand, redirecting to previous page", Toast.LENGTH_SHORT).show();

                getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, new FragmentHome()).commit();
            } else {
                if (PublicMethod.isDeviceOnline(mContext)){
                    dialogLoading.dismiss();

                    Snackbar.make(getView(), "Error: " + task.getException().getMessage(), Snackbar.LENGTH_LONG).setAction(R.string.retry, view -> {
                        saveBrand(brandCategories, brandPhone, brandID, brandName, brandDescription, brandFacebook, brandTwitter, brandInstagram, brandWhatsapp, brandWebsite, brandEmail, url);
                    }).show();
                } else {
                    dialogLoading.dismiss();

                    Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
                }
            }
        });

        FirebaseFirestore.getInstance().collection("User").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection("CollectionBrand").document(brandID).set(map);
    }

    private String getPathFromUri(Uri contentUri){
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    private void getDropboxIMGSize(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(
                    getContext().getContentResolver().openInputStream(uri),
                    null,
                    options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double imageHeight = options.outHeight;
        double imageWidth = options.outWidth;
        double finalValue = imageWidth / imageHeight;

        if (finalValue >= 0.4 && finalValue <= 2){
            if (imageHeight < 400 || imageWidth < 400){
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
            } else {
                binding.imageHeader.setImageURI(uri);
            }
        } else if (finalValue < 0.5){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooHeight),
                    Toast.LENGTH_SHORT).show();
        } else if (finalValue > 1.8){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooWidth),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void showKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case REQUEST_IMAGE_CAPTURE:
                    imageHeaderFrom = "camera";
                    Bundle extras = data.getExtras();
                    photoBitmap = (Bitmap) extras.get("data");
                    binding.imageHeader.setImageBitmap(photoBitmap);
                    binding.stepper1.setVisibility(View.GONE);
                    binding.imageStepper1.setVisibility(View.VISIBLE);
                    binding.inputName.setVisibility(View.VISIBLE);
                    setViewStepper(binding.viewStepper2, binding.layoutBrandName);
                    binding.inputName.setFocusable(true);
                    binding.inputName.setFocusableInTouchMode(true);
                    binding.inputName.requestFocus();
                    showKeyboard();
                    break;

                case SELECT_PICTURE:
                    imageHeaderFrom = "storage";
                    selectedImageUri = data.getData();
                    if (null != selectedImageUri) {
                        getDropboxIMGSize(selectedImageUri);
                        binding.stepper1.setVisibility(View.GONE);
                        binding.imageStepper1.setVisibility(View.VISIBLE);
                        binding.inputName.setVisibility(View.VISIBLE);
                        setViewStepper(binding.viewStepper2, binding.layoutBrandName);
                        binding.inputName.setFocusable(true);
                        binding.inputName.setFocusableInTouchMode(true);
                        binding.inputName.requestFocus();
                        showKeyboard();
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needToBackPressHandled;
    }

    @Override
    public void onBackPress() {
        openDialogExit();
    }

    private void openDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view  = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_change_venue, null);
        builder.setView(view);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView yes = view.findViewById(R.id.yes);
        TextView stay = view.findViewById(R.id.stayHere);
        TextView changeVenue = view.findViewById(R.id.changeVenue);
        View divider2 = view.findViewById(R.id.divider2);

        //hide un used view
        divider2.setVisibility(View.GONE);
        changeVenue.setVisibility(View.GONE);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        yes.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            needToBackPressHandled = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        });

        stay.setOnClickListener(view1 -> alertDialog.dismiss());
    }
}
