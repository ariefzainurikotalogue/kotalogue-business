package com.kotalogue.android_business.fragmentDetailEventFolder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kotalogue.android_business.classHelper.PublicMethod;
import com.kotalogue.android_business.classHelper.StaticVariable;
import com.kotalogue.android_business.fragment.FragmentAddEvent;
import com.kotalogue.android_business.fragment.TimePickerFragment;
import com.kotalogue.android_business.globalAdapter.AdapterTag;
import com.kotalogue.android_business.globalInterface.BackPressObserver;
import com.kotalogue.android_business.model.ModelEvent;
import com.kotalogue.android_business.R;
import com.kotalogue.android_business.databinding.FragmentEventInformationBinding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static com.kotalogue.android_business.classHelper.PublicMethod.checkSocMedia;

@SuppressLint("ValidFragment")
public class FragmentEventInformation extends Fragment implements AdapterTag.InterfaceTag, BackPressObserver, TimePickerFragment.TimePickerInterface {
    private FragmentEventInformationBinding binding;

    private String eventID;

    //recycler view
    private AdapterTag adapterTag;
    private ArrayList<String> listOfTag = new ArrayList<>();
    private Context mContext;

    //lazy init
    private String eventLatLon, eventAddress, venueID, eventHeaderPhoto, brandID, eventParentID;
    private int eventLike, eventDislike, eventRSVP;
    private boolean eventManagedBy;
    private boolean isFeatured;

    //request code
    private final int REQUEST_IMAGE_CAPTURE = 1;
    private final int SELECT_PICTURE = 2;

    private Bitmap photoBitmap;
    private boolean isUpdateImageCanceled = true;
    private boolean isImageUpdate = false;
    private ModelEvent modelEventSementara;
    private AlertDialog alertDialog;
    private String oldStatus;
    private String imageHeaderFrom = "a";
    private Uri selectedImageUri;
    private String eventName;
    private String mCurrentPhotoPath;

    private boolean isMaster;
    private boolean tagChanged = false;
    private boolean needCheckBackPressed = true;
    private boolean isSocialMediaOpen = false;
    private RelativeLayout layoutLoading;
    private AlertDialog datePickerDialog;
    private String destination = "";

    public FragmentEventInformation() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null){
            eventID = bundle.getString("eventID");
        }

        isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        binding = FragmentEventInformationBinding.inflate(LayoutInflater.from(mContext), null, false);

        PublicMethod.enableDefaultAnimation(binding.layoutContentMain);
        PublicMethod.enableDefaultAnimation(binding.layoutTextSocialMedia);
        PublicMethod.enableDefaultAnimation(binding.nestedScrollView);
        PublicMethod.enableDefaultAnimation(binding.layoutLoading);
        PublicMethod.enableDefaultAnimation(binding.layoutIconAdd);

        //custom margin
        PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));

        binding.recyclerTag.setNestedScrollingEnabled(false);

        binding.eventStartDate.setSelected(true);
        binding.eventFinishDate.setSelected(true);
        binding.eventInformation.setSelected(true);
        binding.title.setSelected(true);

        /*if (PublicMethod.hasNavBar(mContext, getActivity())){
            ViewGroup.LayoutParams navbarParam = binding.viewNavbar.getLayoutParams();
            navbarParam.height = PublicMethod.getNavigationBarHeight(mContext);
            binding.viewNavbar.setLayoutParams(navbarParam);

            PublicMethod.setMargins(binding.layoutMenu, 0, 0, 0, PublicMethod.getNavigationBarHeight(mContext));
        }*/

        ViewGroup.LayoutParams layoutParams = binding.viewToolbar.getLayoutParams();
        layoutParams.height = PublicMethod.getStatusBarHeight(mContext);
        binding.viewToolbar.setLayoutParams(layoutParams);

        dialogInit();

        recyclerInit();

        switchListener();

        checkMaster();

        loadDetailEvent();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        clickListener();
        nestedScrollListener();
    }

    private void nestedScrollListener() {
        binding.nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (view, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY == (view.getChildAt(0).getMeasuredHeight() - view.getMeasuredHeight())){
                Log.d(TAG, "BOTTOM SCROLL");
            }

            if (scrollY > oldScrollY){
                Log.d(TAG, "SCROLL DOWN");
                binding.layoutBottomInformation.setVisibility(View.GONE);
                binding.layoutBottomGallery.setVisibility(View.GONE);
                binding.layoutBottomSubEvent.setVisibility(View.GONE);

                //custom margin
                PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                        PublicMethod.dp(R.dimen.standart_20dp, mContext));
            }

            if (scrollY < oldScrollY){
                Log.d(TAG, "SCROLL UP");
                binding.layoutBottomInformation.setVisibility(View.VISIBLE);
                binding.layoutBottomGallery.setVisibility(View.VISIBLE);
                binding.layoutBottomSubEvent.setVisibility(View.VISIBLE);

                //custom margin
                PublicMethod.setMargins(binding.iconAdd, 0, 0, PublicMethod.dp(R.dimen.standart_20dp, mContext),
                        PublicMethod.getViewHeight(binding.layoutMenu) + PublicMethod.dp(R.dimen.standart_20dp, mContext));
            }

            if (scrollY == 0){
                Log.d(TAG, "TOP SCROLL");
            }
        });
    }

    private void checkMaster() {
        boolean isMaster = mContext.getSharedPreferences(StaticVariable.MASTER_PREFERENCE, Context.MODE_PRIVATE).getBoolean(StaticVariable.IS_MASTER, false);

        if (isMaster){
            binding.btnSaveChange.setVisibility(View.GONE);
        }
    }

    private void switchListener() {
        binding.switchEventStatus.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                binding.txtEventStatus.setText(R.string.eventIsPublished);
            } else {
                binding.txtEventStatus.setText(R.string.eventIsDisable);
            }
        });
    }

    private void clickListener() {
        binding.iconBack.setOnClickListener(view -> getActivity().onBackPressed());

        binding.toogleSocialMedia.setOnClickListener(v -> {
            if (isSocialMediaOpen){
                binding.toogleSocialMedia.setText(R.string.show);
                binding.layoutSocialMedia.setVisibility(View.GONE);
                isSocialMediaOpen = false;
            } else {
                binding.toogleSocialMedia.setText(R.string.hide);
                binding.layoutSocialMedia.setVisibility(View.VISIBLE);
                isSocialMediaOpen = true;
            }
        });

        binding.layoutBottomInformation.setOnClickListener(view -> {});

        binding.layoutBottomSubEvent.setOnClickListener(v -> {
            destination = "subEvent";

            String[] eventTime = modelEventSementara.getEventTime().split(",");

            if (isImageUpdate){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.inputEventName.getText().toString().equals(modelEventSementara.getEventName())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventCategories.getText().toString().equals(modelEventSementara.getEventCategories())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.txtEventStatus.getText().toString().equals(oldStatus)) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.inputEventDescription.getText().toString().equals(modelEventSementara.getEventLongDesc())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventStartDate.getText().toString().equals(PublicMethod.dateLongToString(modelEventSementara.getEventStartDate(), "MMMM dd, yyyy", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventFinishDate.getText().toString().equals(PublicMethod.dateLongToString(modelEventSementara.getEventEndDate(), "MMMM dd, yyyy", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventStartTime.getText().toString().equals(PublicMethod.dateLongToString(Long.valueOf(eventTime[0]), "kk:mm", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventFinishTime.getText().toString().equals(PublicMethod.dateLongToString(Long.valueOf(eventTime[1]), "kk:mm", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.inputEventPrice.getText().toString().equals(String.valueOf(modelEventSementara.getEventPrice()))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelEventSementara.getEventFacebook())){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelEventSementara.getEventTwitter())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelEventSementara.getEventInstagram())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelEventSementara.getEventWhatsapp())){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelEventSementara.getEventWebsite())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateEmail.getText().toString().equals(modelEventSementara.getEventEmail())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (tagChanged){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            }

            bottomSubEvent();
        });

        binding.layoutBottomGallery.setOnClickListener(v -> {
            destination = "gallery";

            String[] eventTime = modelEventSementara.getEventTime().split(",");

            if (isImageUpdate){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.inputEventName.getText().toString().equals(modelEventSementara.getEventName())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventCategories.getText().toString().equals(modelEventSementara.getEventCategories())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.txtEventStatus.getText().toString().equals(oldStatus)) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.inputEventDescription.getText().toString().equals(modelEventSementara.getEventLongDesc())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventStartDate.getText().toString().equals(PublicMethod.dateLongToString(modelEventSementara.getEventStartDate(), "MMMM dd, yyyy", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventFinishDate.getText().toString().equals(PublicMethod.dateLongToString(modelEventSementara.getEventEndDate(), "MMMM dd, yyyy", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventStartTime.getText().toString().equals(PublicMethod.dateLongToString(Long.valueOf(eventTime[0]), "kk:mm", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.eventFinishTime.getText().toString().equals(PublicMethod.dateLongToString(Long.valueOf(eventTime[1]), "kk:mm", mContext))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.inputEventPrice.getText().toString().equals(String.valueOf(modelEventSementara.getEventPrice()))) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateFacebook.getText().toString().equals(modelEventSementara.getEventFacebook())){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateTwitter.getText().toString().equals(modelEventSementara.getEventTwitter())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateInstagram.getText().toString().equals(modelEventSementara.getEventInstagram())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWhatsApp.getText().toString().equals(modelEventSementara.getEventWhatsapp())){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateWebsite.getText().toString().equals(modelEventSementara.getEventWebsite())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (!binding.updateEmail.getText().toString().equals(modelEventSementara.getEventEmail())) {
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            } else if (tagChanged){
                needCheckBackPressed = false;
                openUnsavedDialog();
                return;
            }

            bottomGallery();
        });

        binding.inputEventTag.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addTag();
            }
            return true;
        });

        binding.iconEditHeaderPhoto.setOnClickListener(v -> openPopUpPhoto());

        binding.btnSaveChange.setOnClickListener(v -> {
            if (isImageUpdate){
                updateImage();
            } else {
                showDialogLoading();
                saveChange(eventHeaderPhoto);
            }
        });

        binding.eventStartTime.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("startTime", binding.eventStartTime, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });

        binding.eventFinishTime.setOnClickListener(v -> {
            DialogFragment timePickerFragment = new TimePickerFragment("finishTime", binding.eventFinishTime, this);
            timePickerFragment.show(getFragmentManager(), "timePicker");
        });

        binding.eventStartDate.setOnClickListener(v -> openDatePicker("StartDate"));

        binding.eventFinishDate.setOnClickListener(v -> openDatePicker("FinishDate"));

        binding.eventCategories.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(mContext, binding.eventCategories);
            popupMenu.getMenuInflater().inflate(R.menu.menu_categories, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.food:
                        binding.eventCategories.setText(R.string.foodBeverages);
                        break;

                    case R.id.shopping:
                        binding.eventCategories.setText(R.string.shoppingCenters);
                        break;

                    case R.id.automotive:
                        binding.eventCategories.setText(R.string.automotive);
                        break;

                    case R.id.fashion:
                        binding.eventCategories.setText(R.string.fashion);
                        break;

                    case R.id.health:
                        binding.eventCategories.setText(R.string.health);
                        break;

                    case R.id.electronics:
                        binding.eventCategories.setText(R.string.electronics);
                        break;

                    case R.id.houseHolds:
                        binding.eventCategories.setText(R.string.houseHold);
                        break;

                    case R.id.souvenirs:
                        binding.eventCategories.setText(R.string.souvenir);
                        break;

                    case R.id.tourist:
                        binding.eventCategories.setText(R.string.tourist);
                        break;

                    case R.id.emergencies:
                        binding.eventCategories.setText(R.string.emergencies);
                        break;
                }
                return false;
            });

            popupMenu.show();
        });

        binding.iconAdd.setOnClickListener(view -> {
            if (eventName != null){
                FragmentAddEvent fragmentAddEvent = new FragmentAddEvent();
                Bundle bundle = new Bundle();
                bundle.putString("createSubevent", "Create Subevent");
                bundle.putString("specificVenue", "specificVenue");
                bundle.putString("eventID", eventID);
                bundle.putString("eventLatLon", eventLatLon);
                bundle.putString("eventAddress", eventAddress);
                bundle.putString("eventName", eventName);
                bundle.putString("brandID", brandID);
                bundle.putString("venueID", venueID);
                fragmentAddEvent.setArguments(bundle);

                getFragmentManager().beginTransaction().replace(R.id.containerStandartActivity, fragmentAddEvent).addToBackStack("").commit();
            } else {
                Toast.makeText(mContext, R.string.pleaseWaitWhileLoading, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openDatePicker(final String openFrom) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = getLayoutInflater().inflate(R.layout.dialog_date_picker, null);
        builder.setView(view);

        CalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setDate(Calendar.getInstance().getTimeInMillis());
        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            switch (openFrom){
                case "StartDate":
                    String newMonth = String.valueOf(month+1);
                    String newDay = String.valueOf(dayOfMonth);
                    long longDate = PublicMethod.dateStringToLong(newMonth + "/" + newDay + "/" + String.valueOf(year), "M/d/yyyy");
                    binding.eventStartDate.setText(PublicMethod.dateLongToString(longDate, "MMMM dd, yyyy", mContext));
                    datePickerDialog.dismiss();
                    break;

                case "FinishDate":
                    String finishDay = String.valueOf(dayOfMonth);
                    String finishMonth = String.valueOf(month+1);
                    long finishLongDate = PublicMethod.dateStringToLong(finishMonth + "/" + finishDay + "/" + String.valueOf(year), "M/d/yyyy");
                    binding.eventFinishDate.setText(PublicMethod.dateLongToString(finishLongDate, "MMMM dd, yyyy", mContext));
                    datePickerDialog.dismiss();
                    break;
            }
        });

        datePickerDialog = builder.create();
        datePickerDialog.show();
    }

    private void addTag() {
        if (binding.inputEventTag.getText().toString().trim().equals("")){
            Toast.makeText(mContext, R.string.masukanTag, Toast.LENGTH_SHORT).show();
            return;
        }

        tagChanged = true;

        listOfTag = adapterTag.addData(listOfTag.size() - 1, binding.inputEventTag.getText().toString().trim());

        binding.inputEventTag.setText(null);
    }

    private void dialogInit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading, null);
        builder.setView(view);
        builder.setCancelable(false);

        layoutLoading = view.findViewById(R.id.layoutLoading);

        alertDialog = builder.create();
        alertDialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        alertDialog.setCancelable(false);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mContext,
                        "com.kotalogue.android_business",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void updateImage() {
        //memasukan gambar ke firebase storage
        showDialogLoading();

        switch (imageHeaderFrom){
            case "camera":
                StorageReference storageReference = FirebaseStorage.getInstance().getReference("Event").child(eventID).child(eventID);
                UploadTask uploadGambarBarang = storageReference.putFile(Uri.fromFile(new File(mCurrentPhotoPath)));
                uploadGambarBarang.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        binding.textPhotoCover.setText(R.string.changePhotoCover);
                        binding.iconEdit.setImageResource(R.drawable.icon_edit);

                        saveChange(downloadUri.toString());
                    } else {
                        Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

            case "storage":
                StorageReference storageReferenceEvent = FirebaseStorage.getInstance().getReference("Event").child(eventID).child(eventID);
                UploadTask uploadGambarBarangEvent = storageReferenceEvent.putFile(selectedImageUri);
                uploadGambarBarangEvent.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageReferenceEvent.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        binding.textPhotoCover.setText(R.string.changePhotoCover);
                        binding.iconEdit.setImageResource(R.drawable.icon_edit);

                        saveChange(downloadUri.toString());

                    } else {
                        Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
                break;

                default:
                    binding.imageHeader.setDrawingCacheEnabled(true);
                    binding.imageHeader.buildDrawingCache();
                    Bitmap bitmap2 = binding.imageHeader.getDrawingCache();
                    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
                    byte[] data2 = baos2.toByteArray();

                    StorageReference storageReference2 = FirebaseStorage.getInstance().getReference("Event").child(eventID).child(eventID);
                    UploadTask uploadGambarBarang2 = storageReference2.putBytes(data2);
                    uploadGambarBarang2.continueWithTask(task -> {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return storageReference2.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();

                            binding.textPhotoCover.setText(R.string.changePhotoCover);
                            binding.iconEdit.setImageResource(R.drawable.icon_edit);

                            saveChange(downloadUri.toString());

                        } else {
                            Snackbar.make(getView(), "failed to update. Error: " + task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
                    break;
        }
    }

    private void saveChange(String eventHeaderPhoto) {
        String eventTimeStart = String.valueOf(PublicMethod.dateStringToLong(binding.eventStartTime.getText().toString(), "kk:mm"));
        String eventTimeFinish = String.valueOf(PublicMethod.dateStringToLong(binding.eventFinishTime.getText().toString(), "kk:mm"));
        String eventStartFinishTime = eventTimeStart + "," + eventTimeFinish;
        long eventStartDate = PublicMethod.dateStringToLong(binding.eventStartDate.getText().toString(), "MMMM dd, yyyy");
        long eventFinishDate = PublicMethod.dateStringToLong(binding.eventFinishDate.getText().toString(), "MMMM dd, yyyy");

        boolean isHasPass;

        if (binding.switchEventStatus.isChecked()){
            isHasPass = false;
        } else {
            isHasPass = true;
        }

        ModelEvent modelEvent = new ModelEvent(listOfTag, brandID, FirebaseAuth.getInstance().getCurrentUser().getUid(), eventID, eventParentID, venueID,
                binding.inputEventName.getText().toString(), binding.eventCategories.getText().toString(), eventHeaderPhoto, binding.inputEventDescription.getText().toString(),
                eventAddress, eventLatLon, eventStartFinishTime, binding.updateFacebook.getText().toString(), binding.updateTwitter.getText().toString(),
                binding.updateInstagram.getText().toString(), binding.updateWhatsApp.getText().toString(), binding.updateWebsite.getText().toString(),
                binding.updateEmail.getText().toString(), eventLike, eventDislike, eventRSVP, Integer.valueOf(binding.inputEventPrice.getText().toString()),
                eventManagedBy, isHasPass, isFeatured, eventStartDate, eventFinishDate);

        FirebaseFirestore.getInstance().collection("Event").document(eventID).set(modelEvent)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        alertDialog.dismiss();

                        //change the flag of tag change so the system will not check tag again if user
                        //directly back pressed after save the data
                        tagChanged = false;

                        //change the flag to ture so it will check again from the changing data
                        needCheckBackPressed = true;

                        Snackbar.make(getView(), R.string.succesUpdateEvent, Snackbar.LENGTH_LONG).show();
                    } else {
                        alertDialog.dismiss();

                        if (PublicMethod.isDeviceOnline(mContext)){
                            Toast.makeText(mContext, R.string.failedToUpdate + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, R.string.noConnection, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void openPopUpPhoto() {
        PopupMenu popupMenu = new PopupMenu(mContext, binding.iconEditHeaderPhoto);
        popupMenu.getMenuInflater().inflate(R.menu.camera_storage_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.camera:
                    dispatchTakePictureIntent();
                    break;

                case R.id.storage:
                    Intent intentStorage = new Intent();
                    intentStorage.setType("image/*");
                    intentStorage.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intentStorage, "Select Picture"), SELECT_PICTURE);
                    break;
            }
            return false;
        });

        popupMenu.show();
    }

    private void recyclerInit(){
        adapterTag = new AdapterTag(listOfTag, this);

        binding.recyclerTag.setHasFixedSize(true);
        binding.recyclerTag.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL));
        binding.recyclerTag.setAdapter(adapterTag);
    }

    private void loadDetailEvent(){
        FirebaseFirestore.getInstance().collection("Event").document(eventID).addSnapshotListener((documentSnapshot, e) -> {

            listOfTag = new ArrayList<>();

            if (documentSnapshot != null && documentSnapshot.exists()){

                modelEventSementara = documentSnapshot.toObject(ModelEvent.class);

                binding.setEvent(modelEventSementara);

                new Handler().postDelayed(() -> {
                    hideLoading();
                    binding.layoutContentMain.setVisibility(View.VISIBLE);
                }, 500);

                if (modelEventSementara.isHasPass()){
                    oldStatus = mContext.getResources().getString(R.string.eventIsDisable);
                    binding.switchEventStatus.setChecked(false);
                    binding.txtEventStatus.setText(R.string.eventIsDisable);
                    binding.txtEventStatus.setVisibility(View.VISIBLE);
                } else {
                    oldStatus = mContext.getResources().getString(R.string.eventIsPublished);
                    binding.switchEventStatus.setChecked(true);
                    binding.txtEventStatus.setText(R.string.eventIsPublished);
                    binding.txtEventStatus.setVisibility(View.VISIBLE);
                }

                eventName = modelEventSementara.getEventName();
                brandID = modelEventSementara.getBrandID();
                eventParentID = modelEventSementara.getEventParentID();
                eventLike = modelEventSementara.getEventLike();
                eventDislike = modelEventSementara.getEventDislike();
                eventRSVP = modelEventSementara.getEventRSVP();
                eventManagedBy = modelEventSementara.isEventManagedBy();
                isFeatured = modelEventSementara.isEventFeatured();
                eventLatLon = modelEventSementara.getEventLatLon();
                eventAddress = modelEventSementara.getEventAddress();
                venueID = modelEventSementara.getVenueID();
                eventHeaderPhoto = modelEventSementara.getEventHeaderPhoto();

                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + modelEventSementara.getEventLatLon() + "&zoom=16&size=400x200";

                if (modelEventSementara.getTag() != null){
                    adapterTag.updateData(modelEventSementara.getTag());

                    if (modelEventSementara.getTag().size() > 0){
                        listOfTag = modelEventSementara.getTag();
                    }
                }

                setTime(modelEventSementara.getEventPrice(), modelEventSementara.getEventTime(), modelEventSementara.getEventStartDate(), modelEventSementara.getEventEndDate());

                checkSocMedia(modelEventSementara.getEventFacebook(), modelEventSementara.getEventTwitter(), modelEventSementara.getEventInstagram(), modelEventSementara.getEventWhatsapp(),
                        modelEventSementara.getEventWebsite(), modelEventSementara.getEventEmail(), binding.updateFacebook, binding.updateTwitter, binding.updateInstagram,
                        binding.updateWhatsApp, binding.updateWebsite, binding.updateEmail);

                Glide.with(mContext.getApplicationContext()).load(mapURL).placeholder(R.drawable.thumbnail).into(binding.venueStaticMap);
                Glide.with(mContext.getApplicationContext()).load(modelEventSementara.getEventHeaderPhoto()).placeholder(R.drawable.thumbnail).into(binding.imageHeader);
            }
        });
    }

    private void hideLoading() {
        binding.iconCircle.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void setTime(int eventPrice, String eventTime, long eventStartDate, long eventEndDate) {
        String[] arrEventTime = eventTime.split(",");
        binding.eventStartTime.setText(PublicMethod.dateLongToString(Long.valueOf(arrEventTime[0]), "kk:mm", mContext));
        binding.eventFinishTime.setText(PublicMethod.dateLongToString(Long.valueOf(arrEventTime[1]), "kk:mm", mContext));
        binding.eventStartDate.setText(PublicMethod.dateLongToString(eventStartDate, "MMMM dd, yyyy", mContext));
        binding.eventFinishDate.setText(PublicMethod.dateLongToString(eventEndDate, "MMMM dd, yyyy", mContext));
        binding.inputEventPrice.setText(String.valueOf(eventPrice));
    }

    private void getDropboxIMGSize(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(
                    getContext().getContentResolver().openInputStream(uri),
                    null,
                    options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double imageHeight = options.outHeight;
        double imageWidth = options.outWidth;
        double finalValue = imageWidth / imageHeight;

        if (imageHeight < 300 || imageWidth < 300){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
        } else {
            binding.imageHeader.setImageURI(uri);
        }

        /*if (finalValue >= 0.5 && finalValue <= 1.8){
            if (imageHeight < 300 || imageWidth < 300){
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooSmall), Toast.LENGTH_SHORT).show();
            } else {
                binding.imageHeader.setImageURI(uri);
            }
        } else if (finalValue < 0.5){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooHeight),
                    Toast.LENGTH_SHORT).show();
        } else if (finalValue > 1.8){
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.imageTooWidth),
                    Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){

                case REQUEST_IMAGE_CAPTURE:
                    imageHeaderFrom = "camera";
                    Bundle extras = data.getExtras();
                    photoBitmap = (Bitmap) extras.get("data");
                    binding.imageHeader.setImageBitmap(photoBitmap);

                    //change the flag that user is change the image
                    isImageUpdate = true;

                    //change the flag to tru so we can update the image after user change the photo
                    isUpdateImageCanceled = true;

                    //overide onclick update image
                    binding.iconEdit.setImageResource(R.drawable.icon_cancel);
                    binding.textPhotoCover.setText(R.string.undo);

                    binding.iconEditHeaderPhoto.setOnClickListener((View v) -> {
                        if (isUpdateImageCanceled){
                            isImageUpdate = false;
                            binding.iconEdit.setImageResource(R.drawable.icon_edit);
                            binding.textPhotoCover.setText(R.string.changePhotoCover);
                            Glide.with(mContext.getApplicationContext()).load(eventHeaderPhoto).into(binding.imageHeader);
                            isUpdateImageCanceled = false;
                        } else {
                            openPopUpPhoto();
                        }
                    });
                    break;

                case SELECT_PICTURE:
                    imageHeaderFrom = "storage";
                    selectedImageUri = data.getData();
                    if (null != selectedImageUri) {

                        getDropboxIMGSize(selectedImageUri);

                        //change the flag that user is change the image
                        isImageUpdate = true;

                        //change the flag to true so we can update the image after user change the photo
                        isUpdateImageCanceled = true;

                        //overide onclick update image
                        binding.iconEdit.setImageResource(R.drawable.icon_cancel);
                        binding.textPhotoCover.setText(R.string.undo);

                        binding.iconEditHeaderPhoto.setOnClickListener(v -> {
                            if (isUpdateImageCanceled){
                                isImageUpdate = false;
                                binding.textPhotoCover.setText(R.string.changePhotoCover);
                                binding.iconEdit.setImageResource(R.drawable.icon_edit);
                                Glide.with(mContext.getApplicationContext()).load(eventHeaderPhoto).into(binding.imageHeader);
                                //Picasso.get().load(eventHeaderPhoto).into(binding.imageHeader);
                                isUpdateImageCanceled = false;
                            } else {
                                openPopUpPhoto();
                            }
                        });
                    }
                    break;
            }
        }
    }

    private void openUnsavedDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_unsaved_change, null);
        builder.setView(view);
        builder.setCancelable(false);

        LinearLayout root = view.findViewById(R.id.rootLayoutDialog);
        TextView save = view.findViewById(R.id.save);
        TextView dismiss = view.findViewById(R.id.dismiss);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(root) - PublicMethod.dp(R.dimen.dp60, mContext), ViewGroup.LayoutParams.WRAP_CONTENT);

        //handle click
        save.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            if (isImageUpdate){
                updateImage();
            } else {
                showDialogLoading();

                saveChange(eventHeaderPhoto);
            }
        });

        dismiss.setOnClickListener(view1 -> {
            alertDialog.dismiss();

            switch (destination){
                case "subEvent":
                    bottomSubEvent();
                    break;

                case "gallery":
                    bottomGallery();
                    break;

                    default:
                        getActivity().onBackPressed();
                        new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
                        break;
            }
        });
    }

    private void bottomGallery() {
        FragmentEventGallery fragmentEventGallery = new FragmentEventGallery();
        Bundle bundle = new Bundle();
        bundle.putString("eventID", eventID);
        bundle.putString("eventLatLon", eventLatLon);
        bundle.putString("eventAddress", eventAddress);
        bundle.putString("eventName", eventName);
        bundle.putString("brandID", brandID);
        bundle.putString("venueID", venueID);
        fragmentEventGallery.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentEventGallery).addToBackStack("").commit();
    }

    private void bottomSubEvent() {
        FragmentEventSubEvent fragmentEventSubEvent = new FragmentEventSubEvent();
        Bundle bundle = new Bundle();
        bundle.putString("eventID", eventID);
        bundle.putString("eventLatLon", eventLatLon);
        bundle.putString("eventAddress", eventAddress);
        bundle.putString("eventName", eventName);
        bundle.putString("brandID", brandID);
        bundle.putString("venueID", venueID);
        fragmentEventSubEvent.setArguments(bundle);

        getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.containerStandartActivity, fragmentEventSubEvent).addToBackStack("").commit();
    }

    private void showDialogLoading() {
        alertDialog.show();
        alertDialog.getWindow().setLayout(PublicMethod.getViewWidth(layoutLoading), PublicMethod.getViewHeight(layoutLoading));
    }

    private void checkBackPressed(){
        String[] eventTime = modelEventSementara.getEventTime().split(",");

        if (isImageUpdate){
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.inputEventName.getText().toString().equals(modelEventSementara.getEventName())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.eventCategories.getText().toString().equals(modelEventSementara.getEventCategories())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.txtEventStatus.getText().toString().equals(oldStatus)) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.inputEventDescription.getText().toString().equals(modelEventSementara.getEventLongDesc())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.eventStartDate.getText().toString().equals(PublicMethod.dateLongToString(modelEventSementara.getEventStartDate(), "MMMM dd, yyyy", mContext))) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.eventFinishDate.getText().toString().equals(PublicMethod.dateLongToString(modelEventSementara.getEventEndDate(), "MMMM dd, yyyy", mContext))) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.eventStartTime.getText().toString().equals(PublicMethod.dateLongToString(Long.valueOf(eventTime[0]), "kk:mm", mContext))) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.eventFinishTime.getText().toString().equals(PublicMethod.dateLongToString(Long.valueOf(eventTime[1]), "kk:mm", mContext))) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.inputEventPrice.getText().toString().equals(String.valueOf(modelEventSementara.getEventPrice()))) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateFacebook.getText().toString().equals(modelEventSementara.getEventFacebook())){
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateTwitter.getText().toString().equals(modelEventSementara.getEventTwitter())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateInstagram.getText().toString().equals(modelEventSementara.getEventInstagram())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateWhatsApp.getText().toString().equals(modelEventSementara.getEventWhatsapp())){
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateWebsite.getText().toString().equals(modelEventSementara.getEventWebsite())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (!binding.updateEmail.getText().toString().equals(modelEventSementara.getEventEmail())) {
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else if (tagChanged){
            needCheckBackPressed = false;
            openUnsavedDialog();
            return;
        } else {
            needCheckBackPressed = false;
            getActivity().onBackPressed();
            new Handler().postDelayed(() -> binding.rootLayout.removeAllViews(), 500);
        }
    }

    @Override
    public void tagClick(String tag) {
        tagChanged = true;

        int index = listOfTag.indexOf(tag);

        if (listOfTag.size() == 1){
            listOfTag.remove(index);
            adapterTag.updateData(listOfTag);
        } else {
            listOfTag = adapterTag.removeTag(index);
        }
    }

    @Override
    public boolean isReadyToInterceptBackPress() {
        return needCheckBackPressed;
    }

    @Override
    public void onBackPress() {
        checkBackPressed();
    }

    @Override
    public void onTimeSet() {
        //do nothing
    }
}
