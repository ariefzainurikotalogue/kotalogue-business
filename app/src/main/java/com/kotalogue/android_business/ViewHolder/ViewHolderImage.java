package com.kotalogue.android_business.ViewHolder;

import android.support.v7.widget.RecyclerView;

import com.kotalogue.android_business.databinding.LayoutGalleryBinding;

public class ViewHolderImage extends RecyclerView.ViewHolder{
    LayoutGalleryBinding layoutGalleryBinding;
    public ViewHolderImage(LayoutGalleryBinding layoutGalleryBinding) {
        super(layoutGalleryBinding.getRoot());
        this.layoutGalleryBinding = layoutGalleryBinding;
    }
}
