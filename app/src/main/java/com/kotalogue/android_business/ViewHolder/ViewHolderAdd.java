package com.kotalogue.android_business.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import com.kotalogue.android_business.databinding.LayoutAddImageBinding;

public class ViewHolderAdd extends RecyclerView.ViewHolder {
    LayoutAddImageBinding layoutAddImageBinding;
    public ViewHolderAdd(@NonNull LayoutAddImageBinding layoutAddImageBinding) {
        super(layoutAddImageBinding.getRoot());
    }
}
