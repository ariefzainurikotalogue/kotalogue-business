package com.kotalogue.android_business.model;

import android.net.Uri;

/**
 * Created by ariefzainuri on 15/04/18.
 */

public class ModelGallery {
    private String photoID, photo, status;
    private boolean savedToServer;
    private Uri photoUri;

    public ModelGallery() {}

    public ModelGallery(String photoID, String photo, String status, boolean savedToServer, Uri photoUri) {
        this.photoID = photoID;
        this.photo = photo;
        this.status = status;
        this.savedToServer = savedToServer;
        this.photoUri = photoUri;
    }

    public String getPhotoID() {
        return photoID;
    }

    public String getPhoto() {
        return photo;
    }

    public String getStatus() {
        return status;
    }

    public boolean isSavedToServer() {
        return savedToServer;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSavedToServer(boolean savedToServer) {
        this.savedToServer = savedToServer;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }
}
