package com.kotalogue.android_business.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ariefzainuri on 15/04/18.
 */

public class ModelEvent implements Serializable{
    private ArrayList<String> tag;
    private String brandID, userID, eventID, eventParentID, venueID, eventName, eventCategories , eventHeaderPhoto, eventLongDesc, eventAddress, eventLatLon, eventTime, eventFacebook, eventTwitter, eventInstagram, eventWhatsapp, eventWebsite, eventEmail;
    private int eventLike, eventDislike, eventRSVP, eventPrice;
    private boolean eventManagedBy, hasPass, eventFeatured;
    private long eventStartDate, eventEndDate;
    //private Activity activity;

    public ModelEvent() { }

    public ModelEvent(ArrayList<String> tag, String brandID, String userID, String eventID, String eventParentID, String venueID, String eventName, String eventCategories, String eventHeaderPhoto, String eventLongDesc, String eventAddress, String eventLatLon, String eventTime, String eventFacebook, String eventTwitter, String eventInstagram, String eventWhatsapp, String eventWebsite, String eventEmail, int eventLike, int eventDislike, int eventRSVP, int eventPrice, boolean eventManagedBy, boolean hasPass, boolean eventFeatured, long eventStartDate, long eventEndDate) {
        this.tag = tag;
        this.brandID = brandID;
        this.userID = userID;
        this.eventID = eventID;
        this.eventParentID = eventParentID;
        this.venueID = venueID;
        this.eventName = eventName;
        this.eventCategories = eventCategories;
        this.eventHeaderPhoto = eventHeaderPhoto;
        this.eventLongDesc = eventLongDesc;
        this.eventAddress = eventAddress;
        this.eventLatLon = eventLatLon;
        this.eventTime = eventTime;
        this.eventFacebook = eventFacebook;
        this.eventTwitter = eventTwitter;
        this.eventInstagram = eventInstagram;
        this.eventWhatsapp = eventWhatsapp;
        this.eventWebsite = eventWebsite;
        this.eventEmail = eventEmail;
        this.eventLike = eventLike;
        this.eventDislike = eventDislike;
        this.eventRSVP = eventRSVP;
        this.eventPrice = eventPrice;
        this.eventManagedBy = eventManagedBy;
        this.hasPass = hasPass;
        this.eventFeatured = eventFeatured;
        this.eventStartDate = eventStartDate;
        this.eventEndDate = eventEndDate;
    }

    public ArrayList<String> getTag() {
        return tag;
    }

    public String getBrandID() {
        return brandID;
    }

    public String getUserID() {
        return userID;
    }

    public String getEventID() {
        return eventID;
    }

    public String getEventParentID() {
        return eventParentID;
    }

    public String getVenueID() {
        return venueID;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventCategories() {
        return eventCategories;
    }

    public String getEventHeaderPhoto() {
        return eventHeaderPhoto;
    }

    public String getEventLongDesc() {
        return eventLongDesc;
    }

    public String getEventAddress() {
        return eventAddress;
    }

    public String getEventLatLon() {
        return eventLatLon;
    }

    public String getEventTime() {
        return eventTime;
    }

    public String getEventFacebook() {
        return eventFacebook;
    }

    public String getEventTwitter() {
        return eventTwitter;
    }

    public String getEventInstagram() {
        return eventInstagram;
    }

    public String getEventWhatsapp() {
        return eventWhatsapp;
    }

    public String getEventWebsite() {
        return eventWebsite;
    }

    public String getEventEmail() {
        return eventEmail;
    }

    public int getEventLike() {
        return eventLike;
    }

    public int getEventDislike() {
        return eventDislike;
    }

    public int getEventRSVP() {
        return eventRSVP;
    }

    public int getEventPrice() {
        return eventPrice;
    }

    public boolean isEventManagedBy() {
        return eventManagedBy;
    }

    public boolean isHasPass() {
        return hasPass;
    }

    public boolean isEventFeatured() {
        return eventFeatured;
    }

    public long getEventStartDate() {
        return eventStartDate;
    }

    public long getEventEndDate() {
        return eventEndDate;
    }

    //custom getter

    /*public String getLikeString(){
        return String.valueOf(eventLike);
    }

    public String getDislikeString(){
        return String.valueOf(eventDislike);
    }

    public String getDay(){
        return String.valueOf(getDifferenceDays(Calendar.getInstance().getTimeInMillis(), eventStartDate));
    }

    public String getStatusEvent(){
        String status = "";
        if (eventStartDate > Calendar.getInstance().getTimeInMillis()){
            String strDifference = String.valueOf(getDifferenceDays(Calendar.getInstance().getTimeInMillis(), eventStartDate)).replace("-", "");
            int intDifference = Integer.valueOf(strDifference) + 1;
            return String.valueOf(intDifference) + " day to go";
        } else if (Calendar.getInstance().getTimeInMillis() < eventEndDate && Calendar.getInstance().getTimeInMillis() > eventStartDate){
            status = activity.getResources().getString(R.string.itsHappeningNow);
        }
        else if (eventStartDate == Calendar.getInstance().getTimeInMillis() && eventEndDate == Calendar.getInstance().getTimeInMillis()){
            status = activity.getResources().getString(R.string.today);
        }
        else if (Calendar.getInstance().getTimeInMillis() > eventEndDate){
            status = activity.getResources().getString(R.string.hasPass);
        }

        return status;
    }

    public String getPrice(){
        String strPrice;

        if (eventPrice == 0){
            strPrice = activity.getResources().getString(R.string.free);
        } else {
            strPrice = String.valueOf(eventPrice);
        }

        return strPrice;
    }

    public String getStringPrice(){
        return String.valueOf(eventPrice);
    }

    public String getTime(){
        String[] arrEventTime = String.valueOf(eventTime).split(",");
        return longTimeConverter(Long.valueOf(arrEventTime[0])) + " - " + longTimeConverter(Long.valueOf(arrEventTime[1]));
    }

    public String getStartTime(){
        String[] arrEventTime = eventTime.split(",");
        return longTimeConverter(Long.valueOf(arrEventTime[0]));
    }

    public String getFinishTime(){
        String[] arrEventTime = eventTime.split(",");
        return longTimeConverter(Long.valueOf(arrEventTime[1]));
    }

    public String getFullDate(){
        return convertToMonthDay(eventStartDate) + " " + activity.getResources().getString(R.string.until) + " " + convertToFullDate(eventEndDate);
    }

    public String getStartDate(){
        return longDateConverter(eventStartDate);
    }

    public String getEndDate(){
        return longDateConverter(eventEndDate);
    }

    private String convertToMonthDay(long longDate){
        Date date = new Date(longDate);
        Format format = new SimpleDateFormat("MMMM dd");
        return format.format(date);
    }

    private String convertToFullDate(long longDate){
        Date date = new Date(longDate);
        Format format = new SimpleDateFormat("MMMM dd, yyyy");
        return format.format(date);
    }

    private String longDateConverter(long longDate){
        Date date = new Date(longDate);
        Format format = new SimpleDateFormat("MM/dd/yyyy");
        return format.format(date);
    }*/
}
