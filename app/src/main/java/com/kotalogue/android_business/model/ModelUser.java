package com.kotalogue.android_business.model;

/**
 * Created by ariefzainuri on 17/04/18.
 */

public class ModelUser {
    private String userID, userType, email;

    public ModelUser() {
    }

    public ModelUser(String userID, String userType, String email) {
        this.userID = userID;
        this.userType = userType;
        this.email = email;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserType() {
        return userType;
    }

    public String getEmail() {
        return email;
    }
}
