package com.kotalogue.android_business.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ariefzainuri on 13/04/18.
 */

public class ModelBrand implements Serializable{
    private ArrayList<String> tag;
    private String brandID, brandUsername, brandCategories, brandName, brandHeaderPhoto, brandPhoneNumber, brandLongDesc, brandFacebook, brandTwitter, brandInstagram, brandWhatsapp, brandWebsite, brandEmail, userID;
    private boolean brandIsManagedByBusiness, publicBrand;

    public ModelBrand() {}

    public ModelBrand(ArrayList<String> tag, String brandID, String brandUsername, String brandCategories, String brandName, String brandHeaderPhoto, String brandPhoneNumber, String brandLongDesc, String brandFacebook, String brandTwitter, String brandInstagram, String brandWhatsapp, String brandWebsite, String brandEmail, String userID, boolean brandIsManagedByBusiness, boolean publicBrand) {
        this.tag = tag;
        this.brandID = brandID;
        this.brandUsername = brandUsername;
        this.brandCategories = brandCategories;
        this.brandName = brandName;
        this.brandHeaderPhoto = brandHeaderPhoto;
        this.brandPhoneNumber = brandPhoneNumber;
        this.brandLongDesc = brandLongDesc;
        this.brandFacebook = brandFacebook;
        this.brandTwitter = brandTwitter;
        this.brandInstagram = brandInstagram;
        this.brandWhatsapp = brandWhatsapp;
        this.brandWebsite = brandWebsite;
        this.brandEmail = brandEmail;
        this.userID = userID;
        this.brandIsManagedByBusiness = brandIsManagedByBusiness;
        this.publicBrand = publicBrand;
    }

    public ArrayList<String> getTag() {
        return tag;
    }

    public String getBrandID() {
        return brandID;
    }

    public String getBrandUsername() {
        return brandUsername;
    }

    public String getBrandCategories() {
        return brandCategories;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getBrandHeaderPhoto() {
        return brandHeaderPhoto;
    }

    public String getBrandPhoneNumber() {
        return brandPhoneNumber;
    }

    public String getBrandLongDesc() {
        return brandLongDesc;
    }

    public String getBrandFacebook() {
        return brandFacebook;
    }

    public String getBrandTwitter() {
        return brandTwitter;
    }

    public String getBrandInstagram() {
        return brandInstagram;
    }

    public String getBrandWhatsapp() {
        return brandWhatsapp;
    }

    public String getBrandWebsite() {
        return brandWebsite;
    }

    public String getBrandEmail() {
        return brandEmail;
    }

    public String getUserID() {
        return userID;
    }

    public boolean isBrandIsManagedByBusiness() {
        return brandIsManagedByBusiness;
    }

    public boolean isPublicBrand() {
        return publicBrand;
    }
}
