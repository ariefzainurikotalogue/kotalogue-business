package com.kotalogue.android_business.model;

public class ModelGroup {
    private String groupID, groupName, groupDescription;

    public ModelGroup() {
    }

    public ModelGroup(String groupID, String groupName, String groupDescription) {
        this.groupID = groupID;
        this.groupName = groupName;
        this.groupDescription = groupDescription;
    }

    public String getGroupID() {
        return groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }
}
