package com.kotalogue.android_business.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ariefzainuri on 14/04/18.
 */

public class ModelVenue implements Serializable{
    private ArrayList<String> tag;
    private String overWriteTime, brandID, userID, venueID, venueParentID, venueCategories, venueUniqueName, venueName, venueAddress, venuePhoneNumber,
             venueHeaderPhoto, venueLongDesc, venueLatLon, venueFacebook, venueTwitter, venueInstagram, venueWhatsapp, venueWebsite, venueEmail,
             sunday, monday, tuesday, wednesday, thursday, friday, saturday;
    private boolean venueIsManagedByBusiness;

    public ModelVenue() {}

    public ModelVenue(ArrayList<String> tag, String overWriteTime, String brandID, String userID, String venueID, String venueParentID, String venueCategories, String venueUniqueName, String venueName, String venueAddress, String venuePhoneNumber, String venueHeaderPhoto, String venueLongDesc, String venueLatLon, String venueFacebook, String venueTwitter, String venueInstagram, String venueWhatsapp, String venueWebsite, String venueEmail, String sunday, String monday, String tuesday, String wednesday, String thursday, String friday, String saturday, boolean venueIsManagedByBusiness) {
        this.tag = tag;
        this.overWriteTime = overWriteTime;
        this.brandID = brandID;
        this.userID = userID;
        this.venueID = venueID;
        this.venueParentID = venueParentID;
        this.venueCategories = venueCategories;
        this.venueUniqueName = venueUniqueName;
        this.venueName = venueName;
        this.venueAddress = venueAddress;
        this.venuePhoneNumber = venuePhoneNumber;
        this.venueHeaderPhoto = venueHeaderPhoto;
        this.venueLongDesc = venueLongDesc;
        this.venueLatLon = venueLatLon;
        this.venueFacebook = venueFacebook;
        this.venueTwitter = venueTwitter;
        this.venueInstagram = venueInstagram;
        this.venueWhatsapp = venueWhatsapp;
        this.venueWebsite = venueWebsite;
        this.venueEmail = venueEmail;
        this.sunday = sunday;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.venueIsManagedByBusiness = venueIsManagedByBusiness;
    }

    public ArrayList<String> getTag() {
        return tag;
    }

    public String getOverWriteTime() {
        return overWriteTime;
    }

    public String getBrandID() {
        return brandID;
    }

    public String getUserID() {
        return userID;
    }

    public String getVenueID() {
        return venueID;
    }

    public String getVenueParentID() {
        return venueParentID;
    }

    public String getVenueCategories() {
        return venueCategories;
    }

    public String getVenueUniqueName() {
        return venueUniqueName;
    }

    public String getVenueName() {
        return venueName;
    }

    public String getVenueAddress() {
        return venueAddress;
    }

    public String getVenuePhoneNumber() {
        return venuePhoneNumber;
    }

    public String getVenueHeaderPhoto() {
        return venueHeaderPhoto;
    }

    public String getVenueLongDesc() {
        return venueLongDesc;
    }

    public String getVenueLatLon() {
        return venueLatLon;
    }

    public String getVenueFacebook() {
        return venueFacebook;
    }

    public String getVenueTwitter() {
        return venueTwitter;
    }

    public String getVenueInstagram() {
        return venueInstagram;
    }

    public String getVenueWhatsapp() {
        return venueWhatsapp;
    }

    public String getVenueWebsite() {
        return venueWebsite;
    }

    public String getVenueEmail() {
        return venueEmail;
    }

    public String getSunday() {
        return sunday;
    }

    public String getMonday() {
        return monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public String getFriday() {
        return friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public boolean isVenueIsManagedByBusiness() {
        return venueIsManagedByBusiness;
    }
}
